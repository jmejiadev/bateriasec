<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Racks
 *
 * @ORM\Table(name="racks")
 * @ORM\Entity(repositoryClass="App\Repository\RacksRepository")
 */
class Racks
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bodegas", inversedBy="racks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bodegas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Seccion", mappedBy="racks")
     */
    private $secciones;

    /**
     * @ORM\Column(type="integer")
     */
    private $racNumero;


    /**
     * @ORM\Column(type="integer")
     */
    private $racNumFilas;

    /**
     * @ORM\Column(type="integer")
     */
    private $racNumColumnas;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $racDescripcion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $racEstado;

    public function __toString()
    {
        return (string)$this->racNumero;
    }

    public function __construct()
    {
        $this->secciones = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRacNumero(): ?int
    {
        return $this->racNumero;
    }

    public function setRacNumero(int $racNumero): self
    {
        $this->racNumero = $racNumero;

        return $this;
    }

    public function getRacNumFilas(): ?int
    {
        return $this->racNumFilas;
    }

    public function setRacNumFilas(int $racNumFilas): self
    {
        $this->racNumFilas = $racNumFilas;

        return $this;
    }

    public function getRacNumColumnas(): ?int
    {
        return $this->racNumColumnas;
    }

    public function setRacNumColumnas(int $racNumColumnas): self
    {
        $this->racNumColumnas = $racNumColumnas;

        return $this;
    }

    public function getRacDescripcion(): ?string
    {
        return $this->racDescripcion;
    }

    public function setRacDescripcion(?string $racDescripcion): self
    {
        $this->racDescripcion = $racDescripcion;

        return $this;
    }

    public function getRacEstado(): ?bool
    {
        return $this->racEstado;
    }

    public function setRacEstado(bool $racEstado): self
    {
        $this->racEstado = $racEstado;

        return $this;
    }

    public function getBodegas(): ?Bodegas
    {
        return $this->bodegas;
    }

    public function setBodegas(?Bodegas $bodegas): self
    {
        $this->bodegas = $bodegas;

        return $this;
    }

    /**
     * @return Collection<int, Seccion>
     */
    public function getSecciones(): Collection
    {
        return $this->secciones;
    }

    public function addSeccione(Seccion $seccione): self
    {
        if (!$this->secciones->contains($seccione)) {
            $this->secciones[] = $seccione;
            $seccione->setRacks($this);
        }

        return $this;
    }

    public function removeSeccione(Seccion $seccione): self
    {
        if ($this->secciones->removeElement($seccione)) {
            // set the owning side to null (unless already changed)
            if ($seccione->getRacks() === $this) {
                $seccione->setRacks(null);
            }
        }

        return $this;
    }

}
