<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Bodegas
 *
 * @ORM\Table(name="bodegas")
 * @ORM\Entity(repositoryClass="App\Repository\BodegasRepository")
 */
class Bodegas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Racks", mappedBy="bodegas")
     */
    private $racks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Recepcion", mappedBy="bodegas")
     */
    private $recepcion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bodNombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bodDescripcion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $bodEstado;


    public function __toString()
    {
        return $this->bodNombre;
    }

    public function __construct()
    {
        $this->racks = new ArrayCollection();
        $this->recepcion = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBodNombre(): ?string
    {
        return $this->bodNombre;
    }

    public function setBodNombre(string $bodNombre): self
    {
        $this->bodNombre = $bodNombre;

        return $this;
    }

    public function getBodDescripcion(): ?string
    {
        return $this->bodDescripcion;
    }

    public function setBodDescripcion(?string $bodDescripcion): self
    {
        $this->bodDescripcion = $bodDescripcion;

        return $this;
    }

    public function getBodEstado(): ?bool
    {
        return $this->bodEstado;
    }

    public function setBodEstado(bool $bodEstado): self
    {
        $this->bodEstado = $bodEstado;

        return $this;
    }

    /**
     * @return Collection<int, Racks>
     */
    public function getRacks(): Collection
    {
        return $this->racks;
    }

    public function addRack(Racks $rack): self
    {
        if (!$this->racks->contains($rack)) {
            $this->racks[] = $rack;
            $rack->setBodegas($this);
        }

        return $this;
    }

    public function removeRack(Racks $rack): self
    {
        if ($this->racks->removeElement($rack)) {
            // set the owning side to null (unless already changed)
            if ($rack->getBodegas() === $this) {
                $rack->setBodegas(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Recepcion>
     */
    public function getRecepcion(): Collection
    {
        return $this->recepcion;
    }

    public function addRecepcion(Recepcion $recepcion): self
    {
        if (!$this->recepcion->contains($recepcion)) {
            $this->recepcion[] = $recepcion;
            $recepcion->setBodegas($this);
        }

        return $this;
    }

    public function removeRecepcion(Recepcion $recepcion): self
    {
        if ($this->recepcion->removeElement($recepcion)) {
            // set the owning side to null (unless already changed)
            if ($recepcion->getBodegas() === $this) {
                $recepcion->setBodegas(null);
            }
        }

        return $this;
    }

}
