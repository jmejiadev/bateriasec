<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Distribuidores
 *
 * @ORM\Table(name="distribuidores")
 * @ORM\Entity(repositoryClass="App\Repository\DistribuidoresRepository")
 */
class Distribuidores
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Users", mappedBy="distribuidores")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $disDocumento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $disNombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $disNombre2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $disDireccion;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $disTelefono;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $disLocalidad;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disEstado;


    public function __toString()
    {
        return $this->disDocumento . " - " . $this->disNombre;
    }

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDisDocumento(): ?string
    {
        return $this->disDocumento;
    }

    public function setDisDocumento(string $disDocumento): self
    {
        $this->disDocumento = $disDocumento;

        return $this;
    }

    public function getDisNombre(): ?string
    {
        return $this->disNombre;
    }

    public function setDisNombre(string $disNombre): self
    {
        $this->disNombre = $disNombre;

        return $this;
    }

    public function getDisEstado(): ?bool
    {
        return $this->disEstado;
    }

    public function setDisEstado(bool $disEstado): self
    {
        $this->disEstado = $disEstado;

        return $this;
    }

    /**
     * @return Collection<int, Users>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(Users $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setDistribuidores($this);
        }

        return $this;
    }

    public function removeUser(Users $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getDistribuidores() === $this) {
                $user->setDistribuidores(null);
            }
        }

        return $this;
    }

    public function getDisNombre2(): ?string
    {
        return $this->disNombre2;
    }

    public function setDisNombre2(?string $disNombre2): self
    {
        $this->disNombre2 = $disNombre2;

        return $this;
    }

    public function getDisDireccion(): ?string
    {
        return $this->disDireccion;
    }

    public function setDisDireccion(?string $disDireccion): self
    {
        $this->disDireccion = $disDireccion;

        return $this;
    }

    public function getDisTelefono(): ?string
    {
        return $this->disTelefono;
    }

    public function setDisTelefono(?string $disTelefono): self
    {
        $this->disTelefono = $disTelefono;

        return $this;
    }

    public function getDisLocalidad(): ?string
    {
        return $this->disLocalidad;
    }

    public function setDisLocalidad(?string $disLocalidad): self
    {
        $this->disLocalidad = $disLocalidad;

        return $this;
    }
}
