<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Modelos
 *
 * @ORM\Table(name="modelos")
 * @ORM\Entity(repositoryClass="App\Repository\ModelosRepository")
 */
class Modelos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $modCaja;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $modCodigo;

    /**
     * @ORM\Column(type="integer")
     */
    private $modCantidad;


    /**
     * @ORM\Column(type="boolean")
     */
    private $modEstado;


    public function __toString()
    {
        return $this->modCodigo;
    }

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModCaja(): ?string
    {
        return $this->modCaja;
    }

    public function setModCaja(string $modCaja): self
    {
        $this->modCaja = $modCaja;

        return $this;
    }

    public function getModCodigo(): ?string
    {
        return $this->modCodigo;
    }

    public function setModCodigo(string $modCodigo): self
    {
        $this->modCodigo = $modCodigo;

        return $this;
    }

    public function getModCantidad(): ?int
    {
        return $this->modCantidad;
    }

    public function setModCantidad(int $modCantidad): self
    {
        $this->modCantidad = $modCantidad;

        return $this;
    }

    public function getModEstado(): ?bool
    {
        return $this->modEstado;
    }

    public function setModEstado(bool $modEstado): self
    {
        $this->modEstado = $modEstado;

        return $this;
    }

}
