<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Despachos
 *
 * @ORM\Table(name="despachos")
 * @ORM\Entity(repositoryClass="App\Repository\DespachosRepository")
 */
class Despachos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DetalleFac", inversedBy="despachos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $detalleFac;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Distribucion", inversedBy="despachos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $distribucion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="despachos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $codGarantia;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $description2;

    /**
     * @ORM\Column(type="boolean")
     */
    private $actGarantia;

    public function __toString()
    {
        return (string)$this->description;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodGarantia(): ?string
    {
        return $this->codGarantia;
    }

    public function setCodGarantia(string $codGarantia): self
    {
        $this->codGarantia = $codGarantia;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription2(): ?string
    {
        return $this->description2;
    }

    public function setDescription2(string $description2): self
    {
        $this->description2 = $description2;

        return $this;
    }

    public function getDetalleFac(): ?DetalleFac
    {
        return $this->detalleFac;
    }

    public function setDetalleFac(?DetalleFac $detalleFac): self
    {
        $this->detalleFac = $detalleFac;

        return $this;
    }

    public function getDistribucion(): ?Distribucion
    {
        return $this->distribucion;
    }

    public function setDistribucion(?Distribucion $distribucion): self
    {
        $this->distribucion = $distribucion;

        return $this;
    }

    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getActGarantia(): ?bool
    {
        return $this->actGarantia;
    }

    public function setActGarantia(bool $actGarantia): self
    {
        $this->actGarantia = $actGarantia;

        return $this;
    }

}
