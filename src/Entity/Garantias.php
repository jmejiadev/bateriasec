<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Garantias
 *
 * @ORM\Table(name="garantias")
 * @ORM\Entity(repositoryClass="App\Repository\GarantiasRepository")
 */
class Garantias
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $garCodigo;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $gruCodigo;

    /**
     * @ORM\Column(type="datetime")
     */
    private $garFecha;

    /**
     * @ORM\Column(type="datetime")
     */
    private $garMarTime;

    /**
     * @ORM\Column(type="integer")
     */
    private $perId;

    /**
     * @ORM\Column(type="integer")
     */
    private $lotId;

    /**
     * @ORM\Column(type="integer")
     */
    private $barId;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $garCodSimple;

    /**
     * @ORM\Column(type="boolean")
     */
    private $garCodQr;

    public function __toString()
    {
        return $this->garCodigo;
    }

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGarCodigo(): ?string
    {
        return $this->garCodigo;
    }

    public function setGarCodigo(string $garCodigo): self
    {
        $this->garCodigo = $garCodigo;

        return $this;
    }

    public function getGruCodigo(): ?string
    {
        return $this->gruCodigo;
    }

    public function setGruCodigo(string $gruCodigo): self
    {
        $this->gruCodigo = $gruCodigo;

        return $this;
    }

    public function getGarFecha(): ?\DateTimeInterface
    {
        return $this->garFecha;
    }

    public function setGarFecha(\DateTimeInterface $garFecha): self
    {
        $this->garFecha = $garFecha;

        return $this;
    }

    public function getGarMarTime(): ?\DateTimeInterface
    {
        return $this->garMarTime;
    }

    public function setGarMarTime(\DateTimeInterface $garMarTime): self
    {
        $this->garMarTime = $garMarTime;

        return $this;
    }

    public function getPerId(): ?int
    {
        return $this->perId;
    }

    public function setPerId(int $perId): self
    {
        $this->perId = $perId;

        return $this;
    }

    public function getLotId(): ?int
    {
        return $this->lotId;
    }

    public function setLotId(int $lotId): self
    {
        $this->lotId = $lotId;

        return $this;
    }

    public function getBarId(): ?int
    {
        return $this->barId;
    }

    public function setBarId(int $barId): self
    {
        $this->barId = $barId;

        return $this;
    }

    public function getGarCodSimple(): ?string
    {
        return $this->garCodSimple;
    }

    public function setGarCodSimple(?string $garCodSimple): self
    {
        $this->garCodSimple = $garCodSimple;

        return $this;
    }

    public function getGarCodQr(): ?bool
    {
        return $this->garCodQr;
    }

    public function setGarCodQr(bool $garCodQr): self
    {
        $this->garCodQr = $garCodQr;

        return $this;
    }

}
