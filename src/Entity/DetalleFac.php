<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * DetalleFac
 *
 * @ORM\Table(name="detalle_fac")
 * @ORM\Entity(repositoryClass="App\Repository\DetalleFacRepository")
 */
class DetalleFac
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Facturas", inversedBy="detalleFac")
     * @ORM\JoinColumn(nullable=false)
     */
    private $facturas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Despachos", mappedBy="detalleFac")
     */
    private $despachos;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $itemId;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $description2;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $unitPrice;

    /**
     * @ORM\Column(type="string", length=500, nullable=false)
     */
    private $regEstado;

    public function __construct()
    {
        $this->despachos = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->description;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItemId(): ?string
    {
        return $this->itemId;
    }

    public function setItemId(string $itemId): self
    {
        $this->itemId = $itemId;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription2(): ?string
    {
        return $this->description2;
    }

    public function setDescription2(string $description2): self
    {
        $this->description2 = $description2;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getUnitPrice(): ?string
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(string $unitPrice): self
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    public function getRegEstado(): ?string
    {
        return $this->regEstado;
    }

    public function setRegEstado(string $regEstado): self
    {
        $this->regEstado = $regEstado;

        return $this;
    }

    public function getFacturas(): ?Facturas
    {
        return $this->facturas;
    }

    public function setFacturas(?Facturas $facturas): self
    {
        $this->facturas = $facturas;

        return $this;
    }

    /**
     * @return Collection<int, Despachos>
     */
    public function getDespachos(): Collection
    {
        return $this->despachos;
    }

    public function addDespacho(Despachos $despacho): self
    {
        if (!$this->despachos->contains($despacho)) {
            $this->despachos[] = $despacho;
            $despacho->setDetalleFac($this);
        }

        return $this;
    }

    public function removeDespacho(Despachos $despacho): self
    {
        if ($this->despachos->removeElement($despacho)) {
            // set the owning side to null (unless already changed)
            if ($despacho->getDetalleFac() === $this) {
                $despacho->setDetalleFac(null);
            }
        }

        return $this;
    }

}
