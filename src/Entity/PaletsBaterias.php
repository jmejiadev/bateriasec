<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaletsBaterias
 *
 * @ORM\Table(name="paletsbaterias")
 * @ORM\Entity(repositoryClass="App\Repository\PaletsBateriasRepository")
 */
class PaletsBaterias
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Palets", inversedBy="paletsbaterias")
     * @ORM\JoinColumn(nullable=false)
     */
    private $palets;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $codigo_gar;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $id_lote;

    public function __toString()
    {
        return $this->codigo_gar;
    }

    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigoGar(): ?string
    {
        return $this->codigo_gar;
    }

    public function setCodigoGar(string $codigo_gar): self
    {
        $this->codigo_gar = $codigo_gar;

        return $this;
    }

    public function getPalets(): ?Palets
    {
        return $this->palets;
    }

    public function setPalets(?Palets $palets): self
    {
        $this->palets = $palets;

        return $this;
    }

    public function getIdLote(): ?string
    {
        return $this->id_lote;
    }

    public function setIdLote(string $id_lote): self
    {
        $this->id_lote = $id_lote;

        return $this;
    }

}
