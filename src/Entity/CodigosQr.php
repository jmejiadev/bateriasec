<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CodigosQr
 *
 * @ORM\Table(name="codigosqr")
 * @ORM\Entity(repositoryClass="App\Repository\CodigosQrRepository")
 */
class CodigosQr
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $codigo_gar;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $cambio_codigo_gar;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $codigo_grupo;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $codigo_grupo_garantia;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $modelo;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nom_prod_neural;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $tipo_bater;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $num_lote;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha_gar;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecCreacion;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $codEstado;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pathQr;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="boolean")
     */
    private $staEstado;

    /**
     * @ORM\Column(type="boolean")
     */
    private $staImpresion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $actGarantia = false;


    public function __toString()
    {
        return $this->pathQr;
    }

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigoGar(): ?string
    {
        return $this->codigo_gar;
    }

    public function setCodigoGar(string $codigo_gar): self
    {
        $this->codigo_gar = $codigo_gar;

        return $this;
    }

    public function getCodigoGrupo(): ?string
    {
        return $this->codigo_grupo;
    }

    public function setCodigoGrupo(string $codigo_grupo): self
    {
        $this->codigo_grupo = $codigo_grupo;

        return $this;
    }

    public function getCodigoGrupoGarantia(): ?string
    {
        return $this->codigo_grupo_garantia;
    }

    public function setCodigoGrupoGarantia(string $codigo_grupo_garantia): self
    {
        $this->codigo_grupo_garantia = $codigo_grupo_garantia;

        return $this;
    }

    public function getModelo(): ?string
    {
        return $this->modelo;
    }

    public function setModelo(string $modelo): self
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function getNomProdNeural(): ?string
    {
        return $this->nom_prod_neural;
    }

    public function setNomProdNeural(string $nom_prod_neural): self
    {
        $this->nom_prod_neural = $nom_prod_neural;

        return $this;
    }

    public function getTipoBater(): ?string
    {
        return $this->tipo_bater;
    }

    public function setTipoBater(string $tipo_bater): self
    {
        $this->tipo_bater = $tipo_bater;

        return $this;
    }

    public function getNumLote(): ?string
    {
        return $this->num_lote;
    }

    public function setNumLote(string $num_lote): self
    {
        $this->num_lote = $num_lote;

        return $this;
    }

    public function getFechaGar(): ?\DateTimeInterface
    {
        return $this->fecha_gar;
    }

    public function setFechaGar(\DateTimeInterface $fecha_gar): self
    {
        $this->fecha_gar = $fecha_gar;

        return $this;
    }

    public function getFecCreacion(): ?\DateTimeInterface
    {
        return $this->fecCreacion;
    }

    public function setFecCreacion(\DateTimeInterface $fecCreacion): self
    {
        $this->fecCreacion = $fecCreacion;

        return $this;
    }

    public function getCodEstado(): ?string
    {
        return $this->codEstado;
    }

    public function setCodEstado(string $codEstado): self
    {
        $this->codEstado = $codEstado;

        return $this;
    }

    public function getPathQr(): ?string
    {
        return $this->pathQr;
    }

    public function setPathQr(string $pathQr): self
    {
        $this->pathQr = $pathQr;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getStaEstado(): ?bool
    {
        return $this->staEstado;
    }

    public function setStaEstado(bool $staEstado): self
    {
        $this->staEstado = $staEstado;

        return $this;
    }

    public function getStaImpresion(): ?bool
    {
        return $this->staImpresion;
    }

    public function setStaImpresion(bool $staImpresion): self
    {
        $this->staImpresion = $staImpresion;

        return $this;
    }

    public function getCambioCodigoGar(): ?string
    {
        return $this->cambio_codigo_gar;
    }

    public function setCambioCodigoGar(?string $cambio_codigo_gar): self
    {
        $this->cambio_codigo_gar = $cambio_codigo_gar;

        return $this;
    }

    public function getActGarantia(): ?bool
    {
        return $this->actGarantia;
    }

    public function setActGarantia(bool $actGarantia): self
    {
        $this->actGarantia = $actGarantia;

        return $this;
    }
}
