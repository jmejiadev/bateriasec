<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Seccion
 *
 * @ORM\Table(name="seccion")
 * @ORM\Entity(repositoryClass="App\Repository\SeccionRepository")
 */
class Seccion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Racks", inversedBy="secciones")
     * @ORM\JoinColumn(nullable=false)
     */
    private $racks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Distribucion", mappedBy="secciones")
     */
    private $distribucion;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $secCodigo;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $secNombre;

    /**
     * @ORM\Column(type="integer")
     */
    private $secCanMaxima;

    /**
     * @ORM\Column(type="boolean")
     */
    private $secEstado;

    public function __toString()
    {
        return $this->secCodigo;
    }

    public function __construct()
    {
        $this->distribucion = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSecCodigo(): ?string
    {
        return $this->secCodigo;
    }

    public function setSecCodigo(string $secCodigo): self
    {
        $this->secCodigo = $secCodigo;

        return $this;
    }

    public function getSecNombre(): ?string
    {
        return $this->secNombre;
    }

    public function setSecNombre(string $secNombre): self
    {
        $this->secNombre = $secNombre;

        return $this;
    }

    public function getSecEstado(): ?bool
    {
        return $this->secEstado;
    }

    public function setSecEstado(bool $secEstado): self
    {
        $this->secEstado = $secEstado;

        return $this;
    }

    public function getRacks(): ?Racks
    {
        return $this->racks;
    }

    public function setRacks(?Racks $racks): self
    {
        $this->racks = $racks;

        return $this;
    }

    /**
     * @return Collection<int, Distribucion>
     */
    public function getDistribucion(): Collection
    {
        return $this->distribucion;
    }

    public function addDistribucion(Distribucion $distribucion): self
    {
        if (!$this->distribucion->contains($distribucion)) {
            $this->distribucion[] = $distribucion;
            $distribucion->setSecciones($this);
        }

        return $this;
    }

    public function removeDistribucion(Distribucion $distribucion): self
    {
        if ($this->distribucion->removeElement($distribucion)) {
            // set the owning side to null (unless already changed)
            if ($distribucion->getSecciones() === $this) {
                $distribucion->setSecciones(null);
            }
        }

        return $this;
    }

    public function getSecCanMaxima(): ?int
    {
        return $this->secCanMaxima;
    }

    public function setSecCanMaxima(int $secCanMaxima): self
    {
        $this->secCanMaxima = $secCanMaxima;

        return $this;
    }

}
