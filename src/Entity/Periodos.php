<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Periodos
 *
 * @ORM\Table(name="periodos")
 * @ORM\Entity(repositoryClass="App\Repository\PeriodosRepository")
 */
class Periodos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $perMes;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $perAnio;

    /**
     * @ORM\Column(type="datetime")
     */
    private $perFecInicio;

    /**
     * @ORM\Column(type="datetime")
     */
    private $perFecFin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $perEstado;


    public function __toString()
    {
        return $this->perAnio;
    }

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerMes(): ?string
    {
        return $this->perMes;
    }

    public function setPerMes(string $perMes): self
    {
        $this->perMes = $perMes;

        return $this;
    }

    public function getPerAnio(): ?string
    {
        return $this->perAnio;
    }

    public function setPerAnio(string $perAnio): self
    {
        $this->perAnio = $perAnio;

        return $this;
    }

    public function getPerFecInicio(): ?\DateTimeInterface
    {
        return $this->perFecInicio;
    }

    public function setPerFecInicio(\DateTimeInterface $perFecInicio): self
    {
        $this->perFecInicio = $perFecInicio;

        return $this;
    }

    public function getPerFecFin(): ?\DateTimeInterface
    {
        return $this->perFecFin;
    }

    public function setPerFecFin(\DateTimeInterface $perFecFin): self
    {
        $this->perFecFin = $perFecFin;

        return $this;
    }

    public function getPerEstado(): ?bool
    {
        return $this->perEstado;
    }

    public function setPerEstado(bool $perEstado): self
    {
        $this->perEstado = $perEstado;

        return $this;
    }
}
