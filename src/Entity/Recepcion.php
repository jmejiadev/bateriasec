<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Recepcion
 *
 * @ORM\Table(name="recepcion")
 * @ORM\Entity(repositoryClass="App\Repository\RecepcionRepository")
 */
class Recepcion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bodegas", inversedBy="recepcion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bodegas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Palets", inversedBy="recepcion")
     * @ORM\JoinColumn(nullable=true)
     */
    private $palets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Distribucion", mappedBy="recepcion")
     */
    private $distribucion;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $codigo_gar;

    /**
     * @ORM\Column(type="datetime")
     */
    private $recFecha;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $id_bateria;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $num_lote;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $codCrm;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $codNeural;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nombProducto;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nombProdNeural;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $modelo;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $codigoLP;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $codigo_inicio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $tipo_bater;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $estado_bater;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $id_garantia;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $fecha_gar;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $marca_time_gar;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $path;


    public function __toString()
    {
        return $this->codigo_gar;
    }

    public function __construct()
    {
        $this->distribucion = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigoGar(): ?string
    {
        return $this->codigo_gar;
    }

    public function setCodigoGar(string $codigo_gar): self
    {
        $this->codigo_gar = $codigo_gar;

        return $this;
    }

    public function getRecFecha(): ?\DateTimeInterface
    {
        return $this->recFecha;
    }

    public function setRecFecha(\DateTimeInterface $recFecha): self
    {
        $this->recFecha = $recFecha;

        return $this;
    }

    public function getIdBateria(): ?string
    {
        return $this->id_bateria;
    }

    public function setIdBateria(?string $id_bateria): self
    {
        $this->id_bateria = $id_bateria;

        return $this;
    }

    public function getNumLote(): ?string
    {
        return $this->num_lote;
    }

    public function setNumLote(?string $num_lote): self
    {
        $this->num_lote = $num_lote;

        return $this;
    }

    public function getCodCrm(): ?string
    {
        return $this->codCrm;
    }

    public function setCodCrm(?string $codCrm): self
    {
        $this->codCrm = $codCrm;

        return $this;
    }

    public function getCodNeural(): ?string
    {
        return $this->codNeural;
    }

    public function setCodNeural(?string $codNeural): self
    {
        $this->codNeural = $codNeural;

        return $this;
    }

    public function getNombProducto(): ?string
    {
        return $this->nombProducto;
    }

    public function setNombProducto(?string $nombProducto): self
    {
        $this->nombProducto = $nombProducto;

        return $this;
    }

    public function getNombProdNeural(): ?string
    {
        return $this->nombProdNeural;
    }

    public function setNombProdNeural(?string $nombProdNeural): self
    {
        $this->nombProdNeural = $nombProdNeural;

        return $this;
    }

    public function getModelo(): ?string
    {
        return $this->modelo;
    }

    public function setModelo(?string $modelo): self
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function getCodigoLP(): ?string
    {
        return $this->codigoLP;
    }

    public function setCodigoLP(?string $codigoLP): self
    {
        $this->codigoLP = $codigoLP;

        return $this;
    }

    public function getCodigoInicio(): ?string
    {
        return $this->codigo_inicio;
    }

    public function setCodigoInicio(?string $codigo_inicio): self
    {
        $this->codigo_inicio = $codigo_inicio;

        return $this;
    }

    public function getTipoBater(): ?string
    {
        return $this->tipo_bater;
    }

    public function setTipoBater(?string $tipo_bater): self
    {
        $this->tipo_bater = $tipo_bater;

        return $this;
    }

    public function getEstadoBater(): ?string
    {
        return $this->estado_bater;
    }

    public function setEstadoBater(?string $estado_bater): self
    {
        $this->estado_bater = $estado_bater;

        return $this;
    }

    public function getIdGarantia(): ?string
    {
        return $this->id_garantia;
    }

    public function setIdGarantia(?string $id_garantia): self
    {
        $this->id_garantia = $id_garantia;

        return $this;
    }

    public function getFechaGar(): ?string
    {
        return $this->fecha_gar;
    }

    public function setFechaGar(?string $fecha_gar): self
    {
        $this->fecha_gar = $fecha_gar;

        return $this;
    }

    public function getMarcaTimeGar(): ?string
    {
        return $this->marca_time_gar;
    }

    public function setMarcaTimeGar(?string $marca_time_gar): self
    {
        $this->marca_time_gar = $marca_time_gar;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getBodegas(): ?Bodegas
    {
        return $this->bodegas;
    }

    public function setBodegas(?Bodegas $bodegas): self
    {
        $this->bodegas = $bodegas;

        return $this;
    }

    /**
     * @return Collection<int, Distribucion>
     */
    public function getDistribucion(): Collection
    {
        return $this->distribucion;
    }

    public function addDistribucion(Distribucion $distribucion): self
    {
        if (!$this->distribucion->contains($distribucion)) {
            $this->distribucion[] = $distribucion;
            $distribucion->setRecepcion($this);
        }

        return $this;
    }

    public function removeDistribucion(Distribucion $distribucion): self
    {
        if ($this->distribucion->removeElement($distribucion)) {
            // set the owning side to null (unless already changed)
            if ($distribucion->getRecepcion() === $this) {
                $distribucion->setRecepcion(null);
            }
        }

        return $this;
    }

    public function getPalets(): ?Palets
    {
        return $this->palets;
    }

    public function setPalets(?Palets $palets): self
    {
        $this->palets = $palets;

        return $this;
    }

}
