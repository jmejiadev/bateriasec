<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Baterias
 *
 * @ORM\Table(name="baterias")
 * @ORM\Entity(repositoryClass="App\Repository\BateriasRepository")
 */
class Baterias
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $batCodCrm;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $batCodNeural;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $batNomProducto;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $batNomProdNeural;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $batModelo;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $batCodLp;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $batCodInicio;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $batTipo;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $batCodGrupo;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $batCodDynamics;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $batNumPlacas;


    /**
     * @ORM\Column(type="boolean")
     */
    private $batEstado;


    public function __toString()
    {
        return $this->batNomProducto;
    }

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBatCodCrm(): ?string
    {
        return $this->batCodCrm;
    }

    public function setBatCodCrm(?string $batCodCrm): self
    {
        $this->batCodCrm = $batCodCrm;

        return $this;
    }

    public function getBatCodNeural(): ?string
    {
        return $this->batCodNeural;
    }

    public function setBatCodNeural(?string $batCodNeural): self
    {
        $this->batCodNeural = $batCodNeural;

        return $this;
    }

    public function getBatNomProducto(): ?string
    {
        return $this->batNomProducto;
    }

    public function setBatNomProducto(?string $batNomProducto): self
    {
        $this->batNomProducto = $batNomProducto;

        return $this;
    }

    public function getBatNomProdNeural(): ?string
    {
        return $this->batNomProdNeural;
    }

    public function setBatNomProdNeural(?string $batNomProdNeural): self
    {
        $this->batNomProdNeural = $batNomProdNeural;

        return $this;
    }

    public function getBatModelo(): ?string
    {
        return $this->batModelo;
    }

    public function setBatModelo(?string $batModelo): self
    {
        $this->batModelo = $batModelo;

        return $this;
    }

    public function getBatCodLp(): ?string
    {
        return $this->batCodLp;
    }

    public function setBatCodLp(?string $batCodLp): self
    {
        $this->batCodLp = $batCodLp;

        return $this;
    }

    public function getBatCodInicio(): ?string
    {
        return $this->batCodInicio;
    }

    public function setBatCodInicio(string $batCodInicio): self
    {
        $this->batCodInicio = $batCodInicio;

        return $this;
    }

    public function getBatTipo(): ?string
    {
        return $this->batTipo;
    }

    public function setBatTipo(?string $batTipo): self
    {
        $this->batTipo = $batTipo;

        return $this;
    }

    public function getBatCodGrupo(): ?string
    {
        return $this->batCodGrupo;
    }

    public function setBatCodGrupo(?string $batCodGrupo): self
    {
        $this->batCodGrupo = $batCodGrupo;

        return $this;
    }

    public function getBatNumPlacas(): ?int
    {
        return $this->batNumPlacas;
    }

    public function setBatNumPlacas(int $batNumPlacas): self
    {
        $this->batNumPlacas = $batNumPlacas;

        return $this;
    }

    public function getBatEstado(): ?bool
    {
        return $this->batEstado;
    }

    public function setBatEstado(bool $batEstado): self
    {
        $this->batEstado = $batEstado;

        return $this;
    }

    public function getBatCodDynamics(): ?string
    {
        return $this->batCodDynamics;
    }

    public function setBatCodDynamics(?string $batCodDynamics): self
    {
        $this->batCodDynamics = $batCodDynamics;

        return $this;
    }
}
