<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Grupos
 *
 * @ORM\Table(name="grupos")
 * @ORM\Entity(repositoryClass="App\Repository\GruposRepository")
 */
class Grupos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $gruCodigo;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $gruDescripcion;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $gruTipBateria1;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $gruTipBateria2;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $gruTipBateria3;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $gruTipBateria4;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $gruTipBateria5;

    /**
     * @ORM\Column(type="integer")
     */
    private $gruSecuencia;

    /**
     * @ORM\Column(type="boolean")
     */
    private $gruEstado;


    public function __toString()
    {
        return $this->gruCodigo;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGruCodigo(): ?string
    {
        return $this->gruCodigo;
    }

    public function setGruCodigo(?string $gruCodigo): self
    {
        $this->gruCodigo = $gruCodigo;

        return $this;
    }

    public function getGruDescripcion(): ?string
    {
        return $this->gruDescripcion;
    }

    public function setGruDescripcion(?string $gruDescripcion): self
    {
        $this->gruDescripcion = $gruDescripcion;

        return $this;
    }

    public function getGruTipBateria1(): ?string
    {
        return $this->gruTipBateria1;
    }

    public function setGruTipBateria1(?string $gruTipBateria1): self
    {
        $this->gruTipBateria1 = $gruTipBateria1;

        return $this;
    }

    public function getGruTipBateria2(): ?string
    {
        return $this->gruTipBateria2;
    }

    public function setGruTipBateria2(?string $gruTipBateria2): self
    {
        $this->gruTipBateria2 = $gruTipBateria2;

        return $this;
    }

    public function getGruTipBateria3(): ?string
    {
        return $this->gruTipBateria3;
    }

    public function setGruTipBateria3(?string $gruTipBateria3): self
    {
        $this->gruTipBateria3 = $gruTipBateria3;

        return $this;
    }

    public function getGruTipBateria4(): ?string
    {
        return $this->gruTipBateria4;
    }

    public function setGruTipBateria4(?string $gruTipBateria4): self
    {
        $this->gruTipBateria4 = $gruTipBateria4;

        return $this;
    }

    public function getGruTipBateria5(): ?string
    {
        return $this->gruTipBateria5;
    }

    public function setGruTipBateria5(?string $gruTipBateria5): self
    {
        $this->gruTipBateria5 = $gruTipBateria5;

        return $this;
    }

    public function getGruSecuencia(): ?int
    {
        return $this->gruSecuencia;
    }

    public function setGruSecuencia(int $gruSecuencia): self
    {
        $this->gruSecuencia = $gruSecuencia;

        return $this;
    }

    public function getGruEstado(): ?bool
    {
        return $this->gruEstado;
    }

    public function setGruEstado(bool $gruEstado): self
    {
        $this->gruEstado = $gruEstado;

        return $this;
    }

}
