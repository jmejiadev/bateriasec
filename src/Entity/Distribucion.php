<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Distribucion
 *
 * @ORM\Table(name="distribucion")
 * @ORM\Entity(repositoryClass="App\Repository\DistribucionRepository")
 */
class Distribucion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Palets", inversedBy="distribucion")
     * @ORM\JoinColumn(nullable=true)
     */
    private $palets;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Recepcion", inversedBy="distribucion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $recepcion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Seccion", inversedBy="distribucion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $secciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Despachos", mappedBy="distribucion")
     */
    private $despachos;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $disNumLote;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codigo_gar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $modelo;

    /**
     * @ORM\Column(type="datetime")
     */
    private $disFecDistribucion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $disFecDespacho;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $disEstado;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $disRucFactura;

    /**
     * @ORM\Column(type="integer", length=1, nullable=true)
     */
    private $disStatus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $disSecOrigen;

    public function __toString()
    {
        return $this->codigo_gar;
    }

    public function __construct()
    {
        $this->despachos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDisNumLote(): ?string
    {
        return $this->disNumLote;
    }

    public function setDisNumLote(string $disNumLote): self
    {
        $this->disNumLote = $disNumLote;

        return $this;
    }

    public function getCodigoGar(): ?string
    {
        return $this->codigo_gar;
    }

    public function setCodigoGar(string $codigo_gar): self
    {
        $this->codigo_gar = $codigo_gar;

        return $this;
    }

    public function getDisFecDistribucion(): ?\DateTimeInterface
    {
        return $this->disFecDistribucion;
    }

    public function setDisFecDistribucion(\DateTimeInterface $disFecDistribucion): self
    {
        $this->disFecDistribucion = $disFecDistribucion;

        return $this;
    }

    public function getDisFecDespacho(): ?\DateTimeInterface
    {
        return $this->disFecDespacho;
    }

    public function setDisFecDespacho(?\DateTimeInterface $disFecDespacho): self
    {
        $this->disFecDespacho = $disFecDespacho;

        return $this;
    }

    public function getDisEstado(): ?string
    {
        return $this->disEstado;
    }

    public function setDisEstado(string $disEstado): self
    {
        $this->disEstado = $disEstado;

        return $this;
    }

    public function getRecepcion(): ?Recepcion
    {
        return $this->recepcion;
    }

    public function setRecepcion(?Recepcion $recepcion): self
    {
        $this->recepcion = $recepcion;

        return $this;
    }

    public function getSecciones(): ?Seccion
    {
        return $this->secciones;
    }

    public function setSecciones(?Seccion $secciones): self
    {
        $this->secciones = $secciones;

        return $this;
    }

    public function getPalets(): ?Palets
    {
        return $this->palets;
    }

    public function setPalets(?Palets $palets): self
    {
        $this->palets = $palets;

        return $this;
    }

    public function getModelo(): ?string
    {
        return $this->modelo;
    }

    public function setModelo(string $modelo): self
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * @return Collection<int, Despachos>
     */
    public function getDespachos(): Collection
    {
        return $this->despachos;
    }

    public function addDespacho(Despachos $despacho): self
    {
        if (!$this->despachos->contains($despacho)) {
            $this->despachos[] = $despacho;
            $despacho->setDistribucion($this);
        }

        return $this;
    }

    public function removeDespacho(Despachos $despacho): self
    {
        if ($this->despachos->removeElement($despacho)) {
            // set the owning side to null (unless already changed)
            if ($despacho->getDistribucion() === $this) {
                $despacho->setDistribucion(null);
            }
        }

        return $this;
    }

    public function getDisRucFactura(): ?string
    {
        return $this->disRucFactura;
    }

    public function setDisRucFactura(?string $disRucFactura): self
    {
        $this->disRucFactura = $disRucFactura;

        return $this;
    }

    public function getDisStatus(): ?int
    {
        return $this->disStatus;
    }

    public function setDisStatus(?int $disStatus): self
    {
        $this->disStatus = $disStatus;

        return $this;
    }

    public function getDisSecOrigen(): ?int
    {
        return $this->disSecOrigen;
    }

    public function setDisSecOrigen(?int $disSecOrigen): self
    {
        $this->disSecOrigen = $disSecOrigen;

        return $this;
    }


}
