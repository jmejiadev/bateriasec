<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Status
 *
 * @ORM\Table(name="status")
 * @ORM\Entity(repositoryClass="App\Repository\StatusRepository")
 */
class Status
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $staNombre;

    /**
     * @ORM\Column(type="boolean")
     */
    private $staEstado;


    public function __toString()
    {
        return $this->staNombre;
    }

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStaNombre(): ?string
    {
        return $this->staNombre;
    }

    public function setStaNombre(string $staNombre): self
    {
        $this->staNombre = $staNombre;

        return $this;
    }

    public function getStaEstado(): ?bool
    {
        return $this->staEstado;
    }

    public function setStaEstado(bool $staEstado): self
    {
        $this->staEstado = $staEstado;

        return $this;
    }

}
