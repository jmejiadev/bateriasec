<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Palets
 *
 * @ORM\Table(name="palets")
 * @ORM\Entity(repositoryClass="App\Repository\PaletsRepository")
 */
class Palets
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PaletsBaterias", mappedBy="palets")
     */
    private $paletsbaterias;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Recepcion", mappedBy="palets")
     */
    private $recepcion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Distribucion", mappedBy="palets")
     */
    private $distribucion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $palDescripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $palPathQr;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $palFecCreacion;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $palEstado;


    public function __toString()
    {
        return $this->palDescripcion;
    }

    public function __construct()
    {
        $this->paletsbaterias = new ArrayCollection();
        $this->recepcion = new ArrayCollection();
        $this->distribucion = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPalDescripcion(): ?string
    {
        return $this->palDescripcion;
    }

    public function setPalDescripcion(string $palDescripcion): self
    {
        $this->palDescripcion = $palDescripcion;

        return $this;
    }

    public function getPalPathQr(): ?string
    {
        return $this->palPathQr;
    }

    public function setPalPathQr(string $palPathQr): self
    {
        $this->palPathQr = $palPathQr;

        return $this;
    }

    public function getPalFecCreacion(): ?\DateTimeInterface
    {
        return $this->palFecCreacion;
    }

    public function setPalFecCreacion(?\DateTimeInterface $palFecCreacion): self
    {
        $this->palFecCreacion = $palFecCreacion;

        return $this;
    }

    public function getPalEstado(): ?string
    {
        return $this->palEstado;
    }

    public function setPalEstado(string $palEstado): self
    {
        $this->palEstado = $palEstado;

        return $this;
    }

    /**
     * @return Collection<int, PaletsBaterias>
     */
    public function getPaletsbaterias(): Collection
    {
        return $this->paletsbaterias;
    }

    public function addPaletsbateria(PaletsBaterias $paletsbateria): self
    {
        if (!$this->paletsbaterias->contains($paletsbateria)) {
            $this->paletsbaterias[] = $paletsbateria;
            $paletsbateria->setPalets($this);
        }

        return $this;
    }

    public function removePaletsbateria(PaletsBaterias $paletsbateria): self
    {
        if ($this->paletsbaterias->removeElement($paletsbateria)) {
            // set the owning side to null (unless already changed)
            if ($paletsbateria->getPalets() === $this) {
                $paletsbateria->setPalets(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Recepcion>
     */
    public function getRecepcion(): Collection
    {
        return $this->recepcion;
    }

    public function addRecepcion(Recepcion $recepcion): self
    {
        if (!$this->recepcion->contains($recepcion)) {
            $this->recepcion[] = $recepcion;
            $recepcion->setPalets($this);
        }

        return $this;
    }

    public function removeRecepcion(Recepcion $recepcion): self
    {
        if ($this->recepcion->removeElement($recepcion)) {
            // set the owning side to null (unless already changed)
            if ($recepcion->getPalets() === $this) {
                $recepcion->setPalets(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Distribucion>
     */
    public function getDistribucion(): Collection
    {
        return $this->distribucion;
    }

    public function addDistribucion(Distribucion $distribucion): self
    {
        if (!$this->distribucion->contains($distribucion)) {
            $this->distribucion[] = $distribucion;
            $distribucion->setPalets($this);
        }

        return $this;
    }

    public function removeDistribucion(Distribucion $distribucion): self
    {
        if ($this->distribucion->removeElement($distribucion)) {
            // set the owning side to null (unless already changed)
            if ($distribucion->getPalets() === $this) {
                $distribucion->setPalets(null);
            }
        }

        return $this;
    }
}
