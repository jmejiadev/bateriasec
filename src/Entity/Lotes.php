<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Lotes
 *
 * @ORM\Table(name="lotes")
 * @ORM\Entity(repositoryClass="App\Repository\LotesRepository")
 */
class Lotes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $lotCodigo;

    /**
     * @ORM\Column(type="integer")
     */
    private $lotCantidad;

    /**
     * @ORM\Column(type="integer")
     */
    private $lotNumPlacas;

    /**
     * @ORM\Column(type="integer")
     */
    private $lotSaldo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lotFecCreacion;

    /**
     * @ORM\Column(type="integer")
     */
    private $lotIdCrm;

    /**
     * @ORM\Column(type="boolean")
     */
    private $lotEstado;


    public function __toString()
    {
        return $this->lotCodigo;
    }

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLotCodigo(): ?string
    {
        return $this->lotCodigo;
    }

    public function setLotCodigo(string $lotCodigo): self
    {
        $this->lotCodigo = $lotCodigo;

        return $this;
    }

    public function getLotCantidad(): ?int
    {
        return $this->lotCantidad;
    }

    public function setLotCantidad(int $lotCantidad): self
    {
        $this->lotCantidad = $lotCantidad;

        return $this;
    }

    public function getLotNumPlacas(): ?int
    {
        return $this->lotNumPlacas;
    }

    public function setLotNumPlacas(int $lotNumPlacas): self
    {
        $this->lotNumPlacas = $lotNumPlacas;

        return $this;
    }

    public function getLotSaldo(): ?int
    {
        return $this->lotSaldo;
    }

    public function setLotSaldo(int $lotSaldo): self
    {
        $this->lotSaldo = $lotSaldo;

        return $this;
    }

    public function getLotFecCreacion(): ?\DateTimeInterface
    {
        return $this->lotFecCreacion;
    }

    public function setLotFecCreacion(?\DateTimeInterface $lotFecCreacion): self
    {
        $this->lotFecCreacion = $lotFecCreacion;

        return $this;
    }

    public function getLotIdCrm(): ?int
    {
        return $this->lotIdCrm;
    }

    public function setLotIdCrm(int $lotIdCrm): self
    {
        $this->lotIdCrm = $lotIdCrm;

        return $this;
    }

    public function getLotEstado(): ?bool
    {
        return $this->lotEstado;
    }

    public function setLotEstado(bool $lotEstado): self
    {
        $this->lotEstado = $lotEstado;

        return $this;
    }
}
