<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Facturas
 *
 * @ORM\Table(name="facturas")
 * @ORM\Entity(repositoryClass="App\Repository\FacturasRepository")
 */
class Facturas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetalleFac", mappedBy="facturas")
     */
    private $detalleFac;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $idFac;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $numFac;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $facFecha;

    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    private $customerNumber;

    /**
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $customerName;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $totalAmountIncludingTax;

    /**
     * @ORM\Column(type="string", length=40, nullable=false)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $facFecDespacho;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $facEstado;


    public function __toString()
    {
        return $this->numFac;
    }

    public function __construct()
    {
        $this->despachos = new ArrayCollection();
        $this->detalleFac = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdFac(): ?string
    {
        return $this->idFac;
    }

    public function setIdFac(string $idFac): self
    {
        $this->idFac = $idFac;

        return $this;
    }

    public function getNumFac(): ?string
    {
        return $this->numFac;
    }

    public function setNumFac(string $numFac): self
    {
        $this->numFac = $numFac;

        return $this;
    }

    public function getFacFecha(): ?\DateTimeInterface
    {
        return $this->facFecha;
    }

    public function setFacFecha(\DateTimeInterface $facFecha): self
    {
        $this->facFecha = $facFecha;

        return $this;
    }

    public function getCustomerNumber(): ?string
    {
        return $this->customerNumber;
    }

    public function setCustomerNumber(string $customerNumber): self
    {
        $this->customerNumber = $customerNumber;

        return $this;
    }

    public function getCustomerName(): ?string
    {
        return $this->customerName;
    }

    public function setCustomerName(string $customerName): self
    {
        $this->customerName = $customerName;

        return $this;
    }

    public function getTotalAmountIncludingTax(): ?string
    {
        return $this->totalAmountIncludingTax;
    }

    public function setTotalAmountIncludingTax(string $totalAmountIncludingTax): self
    {
        $this->totalAmountIncludingTax = $totalAmountIncludingTax;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFacFecDespacho(): ?\DateTimeInterface
    {
        return $this->facFecDespacho;
    }

    public function setFacFecDespacho(?\DateTimeInterface $facFecDespacho): self
    {
        $this->facFecDespacho = $facFecDespacho;

        return $this;
    }

    public function getFacEstado(): ?string
    {
        return $this->facEstado;
    }

    public function setFacEstado(string $facEstado): self
    {
        $this->facEstado = $facEstado;

        return $this;
    }

    /**
     * @return Collection<int, DetalleFac>
     */
    public function getDetalleFac(): Collection
    {
        return $this->detalleFac;
    }

    public function addDetalleFac(DetalleFac $detalleFac): self
    {
        if (!$this->detalleFac->contains($detalleFac)) {
            $this->detalleFac[] = $detalleFac;
            $detalleFac->setFacturas($this);
        }

        return $this;
    }

    public function removeDetalleFac(DetalleFac $detalleFac): self
    {
        if ($this->detalleFac->removeElement($detalleFac)) {
            // set the owning side to null (unless already changed)
            if ($detalleFac->getFacturas() === $this) {
                $detalleFac->setFacturas(null);
            }
        }

        return $this;
    }

}
