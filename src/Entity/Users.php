<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Distribuidores", inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     */
    private $distribuidores;

    /**
     * @Assert\NotBlank(groups={"registration"})
     * @Assert\Length(min=6, max=15, groups={"registration"})
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $usuNombres;

    /**
     * @var string
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $usuApellidos;


    public function __construct()
    {
        parent::__construct();
    }
    public function __toString()
    {
        return $this->usuNombres. " " .$this->usuApellidos;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuNombres(): ?string
    {
        return $this->usuNombres;
    }

    public function setUsuNombres(?string $usuNombres): self
    {
        $this->usuNombres = $usuNombres;

        return $this;
    }

    public function getUsuApellidos(): ?string
    {
        return $this->usuApellidos;
    }

    public function setUsuApellidos(?string $usuApellidos): self
    {
        $this->usuApellidos = $usuApellidos;

        return $this;
    }
    public function getDistribuidores(): ?Distribuidores
    {
        return $this->distribuidores;
    }

    public function setDistribuidores(?Distribuidores $distribuidores): self
    {
        $this->distribuidores = $distribuidores;

        return $this;
    }



}
