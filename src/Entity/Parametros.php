<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Parametros
 *
 * @ORM\Table(name="parametros")
 * @ORM\Entity(repositoryClass="App\Repository\ParametrosRepository")
 */
class Parametros
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $par_codigo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $par_descripcion;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $par_valor;

    /**
     * @ORM\Column(type="boolean")
     */
    private $par_estado;

    public function __toString()
    {
        return $this->par_codigo;
    }

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParCodigo(): ?string
    {
        return $this->par_codigo;
    }

    public function setParCodigo(string $par_codigo): self
    {
        $this->par_codigo = $par_codigo;

        return $this;
    }

    public function getParDescripcion(): ?string
    {
        return $this->par_descripcion;
    }

    public function setParDescripcion(?string $par_descripcion): self
    {
        $this->par_descripcion = $par_descripcion;

        return $this;
    }

    public function getParValor(): ?string
    {
        return $this->par_valor;
    }

    public function setParValor(string $par_valor): self
    {
        $this->par_valor = $par_valor;

        return $this;
    }

    public function getParEstado(): ?bool
    {
        return $this->par_estado;
    }

    public function setParEstado(bool $par_estado): self
    {
        $this->par_estado = $par_estado;

        return $this;
    }
}
