<?php

namespace App\Form;

use App\Entity\Garantias;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GarantiasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('garCodigo')
            ->add('gruCodigo')
            ->add('garFecha')
            ->add('garMarTime')
            ->add('perId')
            ->add('lotId')
            ->add('barId')
            ->add('garCodSimple')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Garantias::class,
        ]);
    }
}
