<?php

namespace App\Form;

use App\Entity\Facturas;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FacturasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('idFac')
            ->add('numFac')
            ->add('facFecha')
            ->add('customerNumber')
            ->add('customerName')
            ->add('totalAmountIncludingTax')
            ->add('phoneNumber')
            ->add('email')
            ->add('facFecDespacho')
            ->add('facEstado')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Facturas::class,
        ]);
    }
}
