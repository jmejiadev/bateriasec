<?php

namespace App\Form;

use App\Entity\Racks;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RacksType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('racNumero')
            ->add('racNumFilas')
            ->add('racNumColumnas')
            ->add('racDescripcion')
            ->add('racEstado')
            ->add('bodegas')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Racks::class,
        ]);
    }
}
