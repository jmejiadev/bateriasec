<?php

namespace App\Form;

use App\Entity\Baterias;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BateriasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('batCodCrm')
            ->add('batCodNeural')
            ->add('batNomProducto')
            ->add('batNomProdNeural')
            ->add('batModelo')
            ->add('batCodLp')
            ->add('batCodInicio')
            ->add('batTipo')
            ->add('batCodGrupo')
            ->add('batNumPlacas')
            ->add('batEstado')
            ->add('batCodDynamics')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Baterias::class,
        ]);
    }
}
