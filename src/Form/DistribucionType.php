<?php

namespace App\Form;

use App\Entity\Distribucion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DistribucionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('disNumLote')
            ->add('codigo_gar')
            ->add('disFecDistribucion')
            ->add('disFecDespacho')
            ->add('disEstado')
            ->add('recepcion')
            ->add('secciones')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Distribucion::class,
        ]);
    }
}
