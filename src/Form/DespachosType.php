<?php

namespace App\Form;

use App\Entity\Despachos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DespachosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('codGarantia')
            ->add('number')
            ->add('description')
            ->add('description2')
            ->add('detalleFac')
            ->add('distribucion')
            ->add('users')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Despachos::class,
        ]);
    }
}
