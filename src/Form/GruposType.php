<?php

namespace App\Form;

use App\Entity\Grupos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GruposType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('gruCodigo')
            ->add('gruDescripcion')
            ->add('gruTipBateria1')
            ->add('gruTipBateria2')
            ->add('gruTipBateria3')
            ->add('gruTipBateria4')
            ->add('gruTipBateria5')
            ->add('gruSecuencia')
            ->add('gruEstado')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Grupos::class,
        ]);
    }
}
