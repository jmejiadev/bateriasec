<?php

namespace App\Form;

use App\Entity\Periodos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PeriodosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('perMes')
            ->add('perAnio')
            ->add('perEstado');
        if($options['data']->getId() == null) {
            $builder
                ->add('perFecInicio', DateType::class, array(
                    'format' => 'yyyy-MM-dd',
                    'widget' => 'single_text',
                    'data' => new \DateTime('now')
                ))
                ->add('perFecFin', DateType::class, array(
                    'format' => 'yyyy-MM-dd',
                    'widget' => 'single_text',
                    'data' => new \DateTime('now')
                ));
        }else{
            $builder
                ->add('perFecInicio', DateType::class, array(
                    'format' => 'yyyy-MM-dd',
                    'widget' => 'single_text',
                ))
                ->add('perFecFin', DateType::class, array(
                    'format' => 'yyyy-MM-dd',
                    'widget' => 'single_text',
                ));
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Periodos::class,
        ]);
    }
}
