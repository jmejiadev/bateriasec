<?php

namespace App\Controller;

use App\Entity\Bodegas;
use App\Form\BodegasType;
use App\Repository\BodegasRepository;
use App\Repository\SeccionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bodegas")
 */
class BodegasController extends AbstractController
{
    /**
     * @Route("/racks-secciones", name="bodegas_racks_secciones", methods={"GET"})
     */
    public function racSecciones(BodegasRepository $bodegasRepository, SeccionRepository $seccionRepository): Response
    {
        //if($_GET['rack'] == 'rack1'){
            $rack = $seccionRepository->getSeccionesByRack($_GET['rack']);
        //}
            /*$rack2 = $seccionRepository->getSeccionesByRack(2);
            $rack3 = $seccionRepository->getSeccionesByRack(3);
            $rack4 = $seccionRepository->getSeccionesByRack(4);
            $rack5 = $seccionRepository->getSeccionesByRack(5);
            $rack6 = $seccionRepository->getSeccionesByRack(6);
            $rack7 = $seccionRepository->getSeccionesByRack(7);
            $rack8 = $seccionRepository->getSeccionesByRack(8);
            $rack9 = $seccionRepository->getSeccionesByRack(9);
            $rack10 = $seccionRepository->getSeccionesByRack(10);
            $rack11 = $seccionRepository->getSeccionesByRack(11);*/
            //$response = [];
            //array_push($response, $rack);

            return new Response(json_encode($rack),200, array('Content-Type' => 'application/json'));
    }

    /** CAMBIA DE ESTADO AL REGISTRO
     * @Route("/cambia-estado-registro/{id}/{estado}", name="bodegas_cambia_estado", methods={"GET"})
     */
    public function cambiaEstado($id, $estado, BodegasRepository $bodegasRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $registro = $em->getRepository(Bodegas::class)->find($id);
        $registro->setBodEstado($estado);
        $em->persist($registro);
        $em->flush();
        return $this->redirectToRoute('bodegas_index');
    }

    /**
     * @Route("/plano", name="bodegas_plano", methods={"GET"})
     */
    public function plano(BodegasRepository $bodegasRepository, SeccionRepository $seccionRepository): Response
    {
        //dd($seccionRepository->getDisponibilidad());
        return $this->render('bodegas/plano.html.twig', [
            'modelos' => $seccionRepository->getModelosBySecciones(),
            'disponibles' => $seccionRepository->getDisponibilidad()
        ]);
    }

    /**
     * @Route("/", name="bodegas_index", methods={"GET"})
     */
    public function index(BodegasRepository $bodegasRepository): Response
    {
        return $this->render('bodegas/index.html.twig', [
            'bodegas' => $bodegasRepository->getAll(),
        ]);
    }

    /**
     * @Route("/new", name="bodegas_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager, BodegasRepository $bodegasRepository): Response
    {
        $bodega = new Bodegas();
        $form = $this->createForm(BodegasType::class, $bodega);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($bodega);
            $entityManager->flush();

            return $this->redirectToRoute('bodegas_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('bodegas/new.html.twig', [
            'bodega' => $bodega,
            'bodegas' => $bodegasRepository->getAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bodegas_show", methods={"GET"})
     */
    public function show(Bodegas $bodega): Response
    {
        return $this->render('bodegas/show.html.twig', [
            'bodega' => $bodega,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="bodegas_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Bodegas $bodega, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(BodegasType::class, $bodega);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('bodegas_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('bodegas/edit.html.twig', [
            'bodega' => $bodega,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bodegas_delete", methods={"POST"})
     */
    public function delete(Request $request, Bodegas $bodega, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bodega->getId(), $request->request->get('_token'))) {
            $entityManager->remove($bodega);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bodegas_index', [], Response::HTTP_SEE_OTHER);
    }
}
