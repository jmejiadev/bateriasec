<?php

namespace App\Controller;

use App\Entity\Palets;
use App\Entity\PaletsBaterias;
use App\Entity\Recepcion;
use App\Form\PaletsType;
use App\Repository\BodegasRepository;
use App\Repository\CodigosQrRepository;
use App\Repository\DistribucionRepository;
use App\Repository\ModelosRepository;
use App\Repository\PaletsBateriasRepository;
use App\Repository\PaletsRepository;
use App\Repository\ParametrosRepository;
use App\Repository\RecepcionRepository;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/palets")
 */
class PaletsController extends AbstractController
{
    /**
     * @Route("/palets-genera-qr/{palId}", name="palets_genera_qr", methods={"GET"})
     */
    public function generaQr($palId, PaletsRepository $paletsRepository, ParametrosRepository $parametrosRepository): Response
    {
        //$palId = $palId;
        $em = $this->getDoctrine()->getManager();
        include('lib/phpqrcode/qrlib.php');
        $tempDir = "codigos_qr_pallet/";
        $pallet = $paletsRepository->getDetalle($palId);
        //dd($pallet);
        //$codGarantias = [];
        //dd($datos);

            $codeContents = "ID: | " . $pallet[0]['id'] . " |";
            $codeContents .= "\n" . $pallet[0]['palDescripcion'] . " |\n";
            $codeContents .= 'RANGO: | ' . $pallet[0]['min'] . "-" . $pallet[0]['max'] . " |";
            $codeContents .="\n".'CANTIDAD: | ' .  $pallet[0]['cantidad'] . " |\n";

        $codeContents = $_ENV['endpoint_baterias'] . "palets/app/" . $palId;

        if (!file_exists($tempDir)) {
            mkdir($tempDir, 0755, true);
        }
            try {
                \QRcode::png($codeContents, $tempDir . "pallet_" . $palId . '.png', QR_ECLEVEL_L, 2.4358974358974358, 1, true);
                $path = $tempDir . "pallet_" . $palId . '.png';
            }catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
            $palet = $paletsRepository->find($palId);
            $palet->setPalPathQr($path);
            $em->persist($palet);
        $em->flush();


        //return $this->redirectToRoute('palets_index');
        return $this->render('codigos_qr/tirilla_pallet.html.twig', [
            'path' => $path,
            'pallet' => $pallet[0],
            'codigo' => $parametrosRepository->findBy(['par_codigo'=>"COD_PALLET"])[0],
            'revision' => $parametrosRepository->findBy(['par_codigo'=>"REVISION"])[0],
        ]);
    }

    /**
     * @Route("/palets-imprime-etiqueta", name="palets_imprime_etiqueta", methods={"POST"})
     */
    public function imprimeEtiqueta(PaletsRepository $paletsRepository, UsersRepository $usersRepository, RecepcionRepository $recepcionRepository,
                                    PaletsBateriasRepository $paletsBateriasRepository, CodigosQrRepository $codigosQrRepository): Response
    {
        $html = $_POST['html'];
        $head = '<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page {
            margin: 0;
            padding: 0;
        }
        html, body, img {
            margin: 0;
            min-height: 80%;
            max-height: 80%;
            padding: 0;
        }
        .margenes {
            margin: 0;
            padding: 0;
            background-color: #0e8a37;

        }
        table, tr, td {
            border-spacing:0; /* Removes the cell spacing via CSS */
            border-collapse: collapse;  
            margin: 0px;
            padding: 0px;
            cellspacing: 0px;
            cellpadding: 0px;
        }
</style>
</head>
<body>';

        $html = $head . $html . "</body>";
        //dd($html);
        //IOMPRIME ETIQUETA PALLET

        $options = new Options();
        $options->set('isRemoteEnabled',TRUE);

        $dompdf = new Dompdf($options);
        $contxt = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed'=> TRUE
            ]
        ]);
        $dompdf->setHttpContext($contxt);
        $paper_size = array(0,0,283.5,283.5);
        $dompdf->set_paper($paper_size);
        //$html = $html;

        // Load HTML to Dompdf $dompdf->loadHtml($html); // (Optional) Setup the paper size and orientation 'portrait' or 'portrait' $dompdf->setPaper('A4', 'portrait'); // Render the HTML as PDF $dompdf->render(); // Output the generated PDF to Browser (inline view) $dompdf->stream("mypdf.pdf", [ "Attachment" => false ]); } }
        $dompdf->load_html($html);
        $dompdf->render();

        $output = $dompdf->output();

        if (!file_exists("tirillas_pallet/")) {
            mkdir("tirillas_pallet/", 0755, true);
        }
        $pdf = file_put_contents("./tirillas_pallet/impresion.pdf", $output);

        return new Response(json_encode("OK"),200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/palets-lote-modelos", name="palets_lote_modelos", methods={"GET"})
     */
    public function modelosByLote(PaletsRepository $paletsRepository, UsersRepository $usersRepository, RecepcionRepository $recepcionRepository,
                                    PaletsBateriasRepository $paletsBateriasRepository, CodigosQrRepository $codigosQrRepository): Response
    {
        $lote = $_GET['lote'];
        //dd($lote);
        //$baterias = $usersRepository->getCodigosAll();
        $modelos = $codigosQrRepository->getModelosByLote(['GENERADO'], $lote);
        //$registros = $recepcionRepository->getRegistrosByEstado();
        //$palets = $paletsBateriasRepository->getBateriasByLote($lote);
        //$resultado = array_diff($baterias, $registros);
        //$resultado = array_diff($baterias, $palets);
       // $baterias =  $paletsRepository->getBateriasNoRegistradasByLote($resultado, $lote);

        return new Response(json_encode($modelos),200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/palets-baterias", name="palets_baterias_lote", methods={"GET"})
     */
    public function bateriasByLote(PaletsRepository $paletsRepository, UsersRepository $usersRepository, RecepcionRepository $recepcionRepository,
                                    PaletsBateriasRepository $paletsBateriasRepository, CodigosQrRepository $codigosQrRepository): Response
    {
        $lote = $_GET['lote'];
        $modelo = $_GET['modelo'];
        //dd($lote);
        //$baterias = $usersRepository->getCodigosAll();
        $baterias = $codigosQrRepository->getCodigosByEstado(['GENERADO'], $lote, $modelo);
        //$registros = $recepcionRepository->getRegistrosByEstado();
        //$palets = $paletsBateriasRepository->getBateriasByLote($lote);
        //$resultado = array_diff($baterias, $registros);
        //$resultado = array_diff($baterias, $palets);
       // $baterias =  $paletsRepository->getBateriasNoRegistradasByLote($resultado, $lote);

        return new Response(json_encode($baterias),200, array('Content-Type' => 'application/json'));
    }
    /**
     * @Route("/recibir/{id}", name="palets_recibir", methods={"GET"})
     */
    public function recibir(Palets $palet, PaletsRepository $paletsRepository, EntityManagerInterface $entityManager,
                            UsersRepository $usersRepository, BodegasRepository $bodegasRepository,
                            PaletsBateriasRepository $paletsBateriasRepository, CodigosQrRepository $codigosQrRepository): Response
    {
        $codigos =  $paletsBateriasRepository->getBateriasByPalet($palet->getId());
        $datos = $usersRepository->getBateriasGeneradas($codigos);
        foreach ($datos as $data){
            $registro = new Recepcion();
            $registro->setCodigoGar($data['codigo_gar']);
            $registro->setBodegas($bodegasRepository->find(1));
            $registro->setRecFecha(new \DateTime( "now", new \DateTimeZone( 'America/Guayaquil' ) ));
            //$registro->setIdBateria($data['id_bateria']);
            //$registro->setCodCrm($data['codCrm']);
            //$registro->setCodigoInicio($data['codigo_inicio']);
            //$registro->setCodigoLP($data['codigoLP']);
            //$registro->setCodNeural($data['codNeural']);
            //$registro->setEstadoBater($data['estado_bater']);
            $registro->setFechaGar($data['fecha_gar']->format('Y-m-d'));
            //$registro->setIdGarantia($data['id_garantia']);
            //$registro->setMarcaTimeGar($data['marca_time_gar']);
            $registro->setModelo($data['modelo']);
            //$registro->setNombProdNeural($data['nombProdNeural']);
            //$registro->setNombProducto($data['nombProducto']);
            $registro->setNumLote($data['id_lote']);
            //$registro->setPath('');
            //$registro->setTipoBater($data['tipo_bater']);
            $registro->setPalets($palet);
            $entityManager->persist($registro);

            $codigoQr = $codigosQrRepository->findBy(['codigo_gar' => $data['codigo_gar']])[0];
            $codigoQr->setCodEstado("RECIBIDO");
            $entityManager->persist($codigoQr);
        }
        $palet->setPalEstado('RECIBIDO');
        $entityManager->persist($palet);
        $entityManager->flush();
        return $this->render('palets/index.html.twig', [
            'palets' => $paletsRepository->getAll(),
        ]);
    }

    /**
     * @Route("/devolver-distribuido/{id}", name="palets_devolver_distribuido", methods={"GET"})
     */
    public function devolverDistribuido(Palets $palet, PaletsRepository $paletsRepository, EntityManagerInterface $entityManager,
                            UsersRepository $usersRepository, CodigosQrRepository $codigosQrRepository,
                            PaletsBateriasRepository $paletsBateriasRepository, RecepcionRepository $recepcionRepository,
                            DistribucionRepository $distribucionRepository): Response
    {
        $batDistribuidas =  $distribucionRepository->findBy(['palets'=>$palet->getId()]);
        //dd($baterias);
        //$datos = $usersRepository->getBateriasNoRegistradas($codigos);
        foreach ($batDistribuidas as $data){
            $codigoQr = $codigosQrRepository->findBy(['codigo_gar'=> $data->getCodigoGar()])[0];
            $codigoQr->setCodEstado("RECIBIDO");
            $entityManager->persist($codigoQr);
            $entityManager->remove($data);
        }
        $palet->setPalEstado('RECIBIDO');
        $entityManager->persist($palet);
        $entityManager->flush();

        return $this->redirectToRoute('distribucion_palets_index', [], Response::HTTP_SEE_OTHER);

    }

    /**
     * @Route("/devolver-recibido/{id}", name="palets_devolver_recibido", methods={"GET"})
     */
    public function devolverRecibido(Palets $palet, PaletsRepository $paletsRepository, EntityManagerInterface $entityManager,
                            UsersRepository $usersRepository, CodigosQrRepository $codigosQrRepository,
                            PaletsBateriasRepository $paletsBateriasRepository, RecepcionRepository $recepcionRepository): Response
    {
        $batRecibidas =  $recepcionRepository->findBy(['palets'=>$palet->getId()]);
        //dd($baterias);
        //$datos = $usersRepository->getBateriasNoRegistradas($codigos);
        foreach ($batRecibidas as $data){
            $codigoQr = $codigosQrRepository->findBy(['codigo_gar' => $data->getCodigoGar()])[0];
            $codigoQr->setCodEstado("EN PALLET");
            $entityManager->persist($codigoQr);
            $entityManager->remove($data);
        }
        $palet->setPalEstado('AGRUPADO');
        $entityManager->persist($palet);
        $entityManager->flush();

        return $this->render('palets/index.html.twig', [
            'palets' => $paletsRepository->getAll(),
        ]);
    }

    /**
     * @Route("/eliminar/{id}", name="palets_eliminar", methods={"GET"})
     */
    public function eliminar(Palets $palet, PaletsRepository $paletsRepository, EntityManagerInterface $entityManager,
                            UsersRepository $usersRepository, BodegasRepository $bodegasRepository,
                            PaletsBateriasRepository $paletsBateriasRepository, CodigosQrRepository $codigosQrRepository): Response
    {
        $baterias =  $paletsBateriasRepository->findBy(['palets'=>$palet->getId()]);
        //dd($baterias);
        //$datos = $usersRepository->getBateriasNoRegistradas($codigos);
        foreach ($baterias as $data){
            $entityManager->remove($data);
            $codigosQrRepository->findBy(['codigo_gar'=>$data->getCodigoGar()])[0]->setCodEstado("GENERADO");
        }
        $entityManager->flush();
        $entityManager->remove($palet);
        //$entityManager->persist($palet);
        $entityManager->flush();
        return $this->render('palets/index.html.twig', [
            'palets' => $paletsRepository->getAll(),
        ]);
    }

    /**
     * @Route("/", name="palets_index", methods={"GET"})
     */
    public function index(PaletsRepository $paletsRepository): Response
    {
        return $this->render('palets/index.html.twig', [
            'palets' => $paletsRepository->getAll(),
        ]);
    }

    /**
     * @Route("/new", name="palets_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager, UsersRepository $usersRepository,
                        RecepcionRepository $recepcionRepository, PaletsRepository $paletsRepository,
                        PaletsBateriasRepository $paletsBateriasRepository, CodigosQrRepository $codigosQrRepository, ModelosRepository $modelosRepository): Response
    {
        $baterias = $codigosQrRepository->getCodigosByEstado(["GENERADO"]);

        $palet = new Palets();
        $palet->setPalFecCreacion(new \DateTime('now'));
        $form = $this->createForm(PaletsType::class, $palet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $arreglo = $request->request->all();
            $registros = json_decode($arreglo['baterias']);
            foreach ($registros as $bateria){
                $pb = new PaletsBaterias();
                $pb->setCodigoGar($bateria->codigo_gar);
                $pb->setIdLote($bateria->num_lote);
                $pb->setPalets($palet);
                $entityManager->persist($pb);

                $codigosQrRepository->findBy(['codigo_gar'=>$bateria->codigo_gar])[0]->setCodEstado("EN PALLET");
            }
            $entityManager->persist($palet);
            $entityManager->flush();

            return $this->redirectToRoute('palets_index', [], Response::HTTP_SEE_OTHER);
        }
        //dd($paletsRepository->getBateriasNoRegistradasLote());
        return $this->render('palets/new.html.twig', [
            'palet' => $palet,
            'lotes' => $paletsRepository->getBateriasNoRegistradasLote(),
            'modelos' => $modelosRepository->getAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="palets_show", methods={"GET"})
     */
    public function show(Palets $palet, PaletsRepository $paletsRepository): Response
    {
        $baterias = $paletsRepository->getBateriasByPallet($palet->getId());
        return $this->render('palets/show.html.twig', [
            'palet' => $palet,
            'baterias' => $baterias,
        ]);
    }
    /**
     * @Route("/app/{id}", name="palets_show_app", methods={"GET"})
     */
    public function showApp(Palets $palet, PaletsRepository $paletsRepository): Response
    {
        $baterias = $paletsRepository->getBateriasByPallet($palet->getId());
        return $this->render('palets/show_app.html.twig', [
            'palet' => $palet,
            'baterias' => $baterias,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="palets_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Palets $palet, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PaletsType::class, $palet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('palets_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('palets/edit.html.twig', [
            'palet' => $palet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="palets_delete", methods={"POST"})
     */
    public function delete(Request $request, Palets $palet, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$palet->getId(), $request->request->get('_token'))) {
            $entityManager->remove($palet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('palets_index', [], Response::HTTP_SEE_OTHER);
    }
}
