<?php

namespace App\Controller;

use App\Entity\Despachos;
use App\Entity\DetalleFac;
use App\Entity\Facturas;
use App\Entity\FeClientes;
use App\Repository\CodigosQrRepository;
use App\Repository\DespachosRepository;
use App\Repository\DetalleFacRepository;
use App\Repository\DistribucionRepository;
use App\Repository\FacturasRepository;
use App\Repository\UserRepository;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use OpenApi\Annotations as OA;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ApiController extends AbstractFOSRestController
{

    /**
     * getClients
    /**
     * @Rest\Get("/api/clientes")
     * @OA\Response(
     *     response=400,
     *     description="operación interrumpida - estructura inválidos",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="estructura inválida"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=401,
     *     description="no autorizado",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="no autorizado"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=200,
     *     description="Operación Completada",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="OK",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="petición procesada correctamente"
     *    					)
     *    				),
     *    ),
     * )
     *
     * @OA\Tag(name="Clientes")
     * @Security(name="Bearer")
     * @param Request $request
     * @return mixed
     */
    public function getClientes(Request $request) :Response
    {
        $data = json_decode($request->getContent(), true);
        $arreglo = [];
        $arreglo['status'] = 'Petición procesada correctamente';
        //$arreglo['ord_num_entrega'] = $data['ord_num_entrega'];
        $response = $arreglo;
        $response = json_encode($response);
        return new Response($response, 200, ['Content-Type' => '"application/json"']);
    }

    /**
     * valida usuario
    /**
     * @Rest\Post("/api/login_check")
     * @OA\Response(
     *     response=400,
     *     description="operación interrumpida - estructura inválidos",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="estructura inválida"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=401,
     *     description="no autorizado",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="no autorizado"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=200,
     *     description="Operación Completada",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="OK",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="petición procesada correctamente"
     *    					)
     *    				),
     *    ),
     * )
     *
     * @OA\Tag(name="Login_Check")
     * @Security(name="Bearer")
     * @param Request $request
     * @return mixed
     */
    public function loginAction(Request $request, UserPasswordEncoderInterface $encoder, UsersRepository $usersRepository)
    {
        $arreglo = json_decode($request->getContent(), true);
        //dd($request->request->all());
        $response = [];
        //$arreglo = $request->request->all();
        $_username = $arreglo['_username'];
        $_password = $arreglo['_password'];

        $user = $usersRepository->findBy(['username' => $_username]);
        // Check if the user exists !
        if(!$user){
            $response['status'] = 'NOK';
            $response['mensaje'] = 'Usuario no existe';
            $response = json_encode($response);
            return new Response(
                $response,
                Response::HTTP_OK,
                array('Content-type' => 'application/json')
            );
        }

        if(!$encoder->isPasswordValid($user[0], $_password)) {
            $response['status'] = 'NOK';
            $response['mensaje'] = 'Usuario o contraseña no válida';
            $response = json_encode($response);
            return new Response(
                $response,
                Response::HTTP_OK,
                array('Content-type' => 'application/json')
            );
        }

        $response['status'] = 'OK';
        $response['mensaje'] = 'Bienvenido '. $user[0]->getUsername();
        $response['roles'] = $user[0]->getRoles()[0];
        $response['username'] = $user[0]->getUsername();
        $response['email'] = $user[0]->getEmail();
        $response = json_encode($response);
        return new Response($response,
            Response::HTTP_OK,
            array('Content-type' => 'application/json')
        );
    }

    /**
     * sincroniza  facturas x fecha
    /**
     * @Rest\Get("/api/sincroniza_facturas")
     * @OA\Response(
     *     response=400,
     *     description="operación interrumpida - estructura inválidos",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="estructura inválida"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=401,
     *     description="no autorizado",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="no autorizado"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=200,
     *     description="Operación Completada",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="OK",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="petición procesada correctamente"
     *    					)
     *    				),
     *    ),
     * )
     *
     * @OA\Tag(name="Sincroniza_Facturas")
     * @param Request $request
     * @return mixed
     */
    public function sincronizaFacturas(Request $request, FacturasRepository $facturasRepository, DetalleFacRepository $detalleFacRepository,
        EntityManagerInterface $em)
    {
        //dd($request->query->all()['fecDesde']);
        if(isset($_GET['fecDesde'])){
            $fecDesde = $_GET['fecDesde'];
            $fecHasta = $_GET['fecHasta'];
        }else{
            $fecDesde = date("Y-m-d");
            $fecHasta = date("Y-m-d");
        }
        //dd($fecHasta);
        //GENERA TOKEN DYNAMICS
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $_ENV['endpoint_login'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => 'scope='.$_ENV['scope'].'&grant_type='.$_ENV['grant_type'].'&client_id='.$_ENV['client_id'].'&client_secret='.$_ENV['client_secret'],
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Cookie: esctx=PAQABBwEAAAApTwJmzXqdR4BN2miheQMYnLVok9_eob3dsh-ya8sWYo-TPJ4_WQPTg8RHOPeXE1xyTKAuAgFA0yAFjE6DtKjW3VW9AJQhbz9pp2iJbpdEQZBJcUhnBbRxhRQ99Z1D6g9LHxAkMSyXt_LkiGtc7Q5MhnahL3oQOiYxl0PAfHnA2n2jZUBamXtp4jQ3hgXSP_AgAA; fpc=ArRaMJfyF9RKoAfhcgewuc5aLjAUAQAAAC5oaN4OAAAA; stsservicecookie=estsfd; x-ms-gateway-slice=estsfd'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if($err){
            $responseEnd['status'] = 'FAILED';
            $responseEnd['mensaje'] = 'Token de dynamics no pudo ser generado';
            $responseEnd['detalle'] = json_encode($err);
            $response = json_encode($responseEnd);
            return new Response($response,
                Response::HTTP_INTERNAL_SERVER_ERROR,
                array('Content-type' => 'application/json')
            );
        }
        $token = json_decode($response)->access_token;
        //dd($token);
        //CONSUME SERVICIO TRAE FACTURAS
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $_ENV['endpoint_facturas'].'?%24filter=invoiceDate%20ge%20'.$fecDesde.'%20and%20invoiceDate%20le%20'.$fecHasta.'&%24expand=salesInvoiceLines(%24expand%3Ditem)',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $token
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if($err){
            $responseEnd['status'] = 'FAILED';
            $responseEnd['mensaje'] = 'Error al consultar Facturas';
            $responseEnd['detalle'] = json_encode($err);
            $response = json_encode($responseEnd);
            return new Response($response,
                Response::HTTP_INTERNAL_SERVER_ERROR,
                array('Content-type' => 'application/json')
            );
        }
        $facturasFinal = json_decode($response)->value;
        $facturas = json_decode($response)->value;
        //dd($facturas);
        foreach ($facturas as $key=>$factura){
            $salesInvoiceLines = $factura->salesInvoiceLines;
            foreach ($salesInvoiceLines as $key2=>$sales) {
                //var_dump("AQUIII->SALES->". json_encode($sales->item->type));
                $item = $sales->item;

                //var_dump($pos);
                if($item == null){
                    unset($facturasFinal[$key]->salesInvoiceLines[$key2]);
                }else{
                    $pos = strpos($item->generalProductPostingGroupCode, "BATERIA");
                    if($item->type != "Inventory" OR $pos === false){
                        unset($facturasFinal[$key]->salesInvoiceLines[$key2]);
                    }
                }
            }
            if(count($facturasFinal[$key]->salesInvoiceLines) < 1){
                unset($facturasFinal[$key]);
            }

        }

        foreach ($facturasFinal as $key=>$factura){
            $existe = $facturasRepository->findBy(['idFac' => $factura->id]);
            if(!$existe){
                $newFactura = new Facturas();
                $newFactura->setTotalAmountIncludingTax($factura->totalAmountIncludingTax);
                $newFactura->setPhoneNumber($factura->phoneNumber);
                $newFactura->setNumFac($factura->number);
                $newFactura->setIdFac($factura->id);
                $newFactura->setFacFecha(new \DateTime($factura->invoiceDate));
                $newFactura->setFacFecDespacho(null);
                $newFactura->setFacEstado("REGISTRADA");
                $newFactura->setEmail($factura->email);
                $newFactura->setCustomerNumber($factura->customerNumber);
                $newFactura->setCustomerName($factura->customerName);
                $em->persist($newFactura);
                //GUARDA EL DETALLE
                foreach($factura->salesInvoiceLines as $detalle){
                    //dd($detalle);
                    //$pos = strpos($detalle->item->generalProductPostingGroupCode, "BATERIA");
                    //var_dump($pos);
                    //if($detalle->item->type != "Inventory" OR $pos === false) {
                        $newDetalle = new DetalleFac();
                        $newDetalle->setUnitPrice($detalle->unitPrice);
                        $newDetalle->setQuantity($detalle->quantity);
                        $newDetalle->setItemId($detalle->itemId);
                        $newDetalle->setDescription2($detalle->description2);
                        $newDetalle->setRegEstado("REGISTRADA");
                        $newDetalle->setDescription($detalle->description);
                        $newDetalle->setFacturas($newFactura);
                        $em->persist($newDetalle);
                    //}
                }
            }
        }

        $em->flush();;
        //dd($facturasFinal);
        $responseEnd['status'] = 'OK';
        $responseEnd['mensaje'] = 'OK';
        $responseEnd['facturas'] = json_encode($facturasFinal);
        $responseEnd['fecDesde'] = $fecDesde;
        //$responseEnd['fecHasta'] = $fecHasta;
        $response = json_encode($responseEnd);
        return new Response($response,
            Response::HTTP_OK,
            array('Content-type' => 'application/json')
        );
    }

    /**
    * trae facturas x fecha y nombre
    /**
     * @Rest\Get("/api/index_facturas")
     * @OA\Response(
     *     response=400,
     *     description="operación interrumpida - estructura inválidos",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="estructura inválida"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=401,
     *     description="no autorizado",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="no autorizado"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=200,
     *     description="Operación Completada",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="OK",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="petición procesada correctamente"
     *    					)
     *    				),
     *    ),
     * )
     *
     * @OA\Tag(name="Index_Facturas")
     * @Security(name="Bearer")
     * @param Request $request
     * @return mixed
     */
    public function indexFacturas(Request $request, FacturasRepository $facturasRepository )
    {
        //dd($request->query->all()['fecDesde']);
        if(!isset($request->query->all()['fecDesde'])){
            $fecDesde = $_GET['fecDesde'];
            $fecHasta = $_GET['fecHasta'];
        }else{
            $fecDesde = $request->query->all()['fecDesde'];
            $fecHasta = $request->query->all()['fecHasta'];
        }
        $buscar = $request->query->all()['buscar'];

        $facturas = $facturasRepository->getByFechasApi($fecDesde, $fecHasta, $buscar);

        //dd(json_encode($facturas));
        $responseEnd['status'] = 'OK';
        $responseEnd['mensaje'] = 'OK';
//        $responseEnd['facturas'] = str_replace("]", "}",str_replace("[", "{", json_encode($facturas)));
        $responseEnd['facturas'] = json_encode($facturas);
        $responseEnd['fecDesde'] = $fecDesde;
        //$responseEnd['fecHasta'] = $fecHasta;
        $response = json_encode($responseEnd);
        return new Response($response,
            Response::HTTP_OK,
            array('Content-type' => 'application/json')
        );
    }

    /**
    * trae detalle de factura
    /**
     * @Rest\Get("/api/detalle_factura")
     * @OA\Response(
     *     response=400,
     *     description="operación interrumpida - estructura inválidos",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="estructura inválida"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=401,
     *     description="no autorizado",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="no autorizado"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=200,
     *     description="Operación Completada",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="OK",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="petición procesada correctamente"
     *    					)
     *    				),
     *    ),
     * )
     *
     * @OA\Tag(name="Detalle_Factura")
     * @Security(name="Bearer")
     * @param Request $request
     * @return mixed
     */
    public function detalleFactura(Request $request, DetalleFacRepository $detalleFacRepository, FacturasRepository $facturasRepository)
    {
        $idFac = $_GET['idFac'];
        //$factura = $facturasRepository->find($idFac);
        $detallesFinal = $detalleFacRepository->getByFactura($idFac);

        //dd($detallesFinal);
        $responseEnd['status'] = 'OK';
        $responseEnd['mensaje'] = 'OK';
        $responseEnd['detalles'] = json_encode($detallesFinal);
        //$responseEnd['factura'] = json_encode($factura);
        //$responseEnd['fecDesde'] = $fecDesde;
        $responseEnd['idFac'] = $idFac;
        //$responseEnd['fecHasta'] = $fecHasta;
        $response = json_encode($responseEnd);
        return new Response($response,
            Response::HTTP_OK,
            array('Content-type' => 'application/json')
        );
    }

    /**
     * Valida Qr
    /**
     * @Rest\Get("/api/validaqr")
     * @OA\Response(
     *     response=400,
     *     description="operación interrumpida - estructura inválidos",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="estructura inválida"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=401,
     *     description="no autorizado",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="no autorizado"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=200,
     *     description="Operación Completada",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="OK",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="petición procesada correctamente"
     *    					)
     *    				),
     *    ),
     * )
     *
     * @OA\Tag(name="validaqr")
     * @Security(name="Bearer")
     * @param Request $request
     * @return mixed
     */
    public function validaQr(Request $request, UsersRepository $usersRepository, CodigosQrRepository $codigosQrRepository)
    {
        $serial = $_GET['serial'];

        $existe = $codigosQrRepository->validaQr($serial);

        //dd($existe);
        if($existe){
            $response='OK';
        }else{
            $response = 'FAIL';
        }
        //$response = json_encode($response);
        return new Response($response,
            Response::HTTP_OK,
            array('Content-type' => 'text/text')
        );
    }

    /**
     * Guarda asigna qr a facturas
    /**
     * @Rest\Post("/api/guardaqrs")
     * @OA\Response(
     *     response=400,
     *     description="operación interrumpida - estructura inválidos",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="estructura inválida"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=401,
     *     description="no autorizado",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="FAILED",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="no autorizado"
     *    					)
     *    				),
     *    ),
     * )
     * @OA\Response(
     *     response=200,
     *     description="Operación Completada",
     *     @OA\JsonContent(
     *       @OA\Property(property="status",
     *    				type="object",
     *    					@OA\Property(property="status",
     *    						type="string",
     *    						example="OK",
     *    					),
     *    					@OA\Property(property="message",
     *    						type="string",
     *    						example="petición procesada correctamente"
     *    					)
     *    				),
     *    ),
     * )
     *
     * @OA\Tag(name="guardaQrs")
     * @Security(name="Bearer")
     * @param Request $request
     * @return mixed
     */
    public function guardaQr(DistribucionRepository $distribucionRepository, FacturasRepository $facturasRepository,
                             UsersRepository $usersRepository, DespachosRepository $despachosRepository): Response
    {
        /*return new Response("papas",
            Response::HTTP_OK,
            array('Content-type' => 'application/json')
        );*/
        $request = json_decode(file_get_contents('php://input'), true);

        $em = $this->getDoctrine()->getManager();
        $exiFactura =  $facturasRepository->findBy(['codFac'=>$request['codfac']]);
        if(!$exiFactura){
            $facErp = $usersRepository->getFacturaErp($request['codfac'])[0];
            $factura = new Facturas();
            $factura->setFacValor($facErp['baseimpfac']);
            $factura->setFacFecha($facErp['fechafac']);
            $factura->setFacEstado('PARCIAL');
            $factura->setFacComentarios($facErp['comentfac']);
            $factura->setCodFac($facErp['codfac']);
            $factura->setCliTelefono($facErp['telefono1cli']);
            $factura->setCliNombre($facErp['nombrecli']);
            $factura->setCliMail($facErp['mailcli']);
            $factura->setCliCodigo($facErp['codcli']);
            $factura->setFacFecDespacho(new \DateTime('now'));
            $em->persist($factura);
            $em->flush();
        }else{
            $exiFactura[0]->setFacFecDespacho(new \DateTime('now'));
        }

        $factura =  $facturasRepository->findBy(['codFac'=>$request['codfac']])[0];
        $registros = $request['seriales'];
        foreach ($registros as $registro){
            $distribucion = $distribucionRepository->findBy(["codigo_gar"=>$registro])[0];
            $despacho = new Despachos();
            $despacho->setUsers( $this->getUser());
            $despacho->setDistribucion($distribucion);
            $despacho->setCodGarantia($distribucion->getCodigoGar());
            $despacho->setDetModelo($distribucion->getModelo());
            $despacho->setDetDescripcion($request['descprodetfac']);
            $despacho->setCodDetFac($request['coddetfac']);
            $despacho->setFacturas($factura);
            $em->persist($despacho);
            $distribucion->setDisEstado('DESPACHADO');
            $distribucion->setDisFecDespacho(new \DateTime('now'));
            $em->persist($distribucion);

            //GUARDA DETALLE
            $detfac = new DetalleFac();
            $detfac->setPreciounidetfac("");
            $detfac->setDesprodfac($request['descprodetfac']);
            $detfac->setDescprodetfac($request['descprodetfac']);
            $detfac->setCoddetfac($request['coddetfac']);
            $detfac->setCodbarprod("");
            $detfac->setClasepro($request['clasepro']);
            $detfac->setCandetfac($request['cantdetfac']);
            $detfac->setFacturas($factura);
            $detfac->setTotal(0);
            $em->persist($detfac);

        }
        $em->flush();
        //CAMBIA DE ESTADO A LA FACTURA
        $detFacErp = $usersRepository->getDetallesFactura($request['codfac']);
        //dd($detFacErp);
        $bandera1 = true;
        $bandera2 = false;
        foreach ($detFacErp as $detalle){
            $existe =  $despachosRepository->findBy(['codDetFac'=>$detalle['coddetfac']]);
            if(!$existe){
                $bandera1 = false;
            }else{
                $bandera2 = true;
            }
        }
        //dd($bandera1);
        if ($bandera2){
            if($bandera1){
                $factura->setFacEstado('COMPLETADA');
            }else{
                $factura->setFacEstado('PARCIAL');
            }
        }
        $em->persist($factura);
        $em->flush();

        $response['status'] = 'OK';
        $response['mensaje'] = 'Guardado correctamente';
        $response = json_encode($response);
        return new Response($response,
            Response::HTTP_OK,
            array('Content-type' => 'application/json')
        );
    }

}