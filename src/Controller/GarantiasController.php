<?php

namespace App\Controller;

use App\Entity\Garantias;
use App\Form\GarantiasType;
use App\Repository\BateriasRepository;
use App\Repository\GarantiasRepository;
use App\Repository\GruposRepository;
use App\Repository\PeriodosRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/garantias")
 */
class GarantiasController extends AbstractController
{
    /**
     * @Route("/generar-gatantias", name="app_garantias_generar_guardar", methods={"POST"})
     */
    public function generarGuardar(GarantiasRepository $garantiasRepository, BateriasRepository $bateriasRepository, PeriodosRepository $periodosRepository,
        GruposRepository $gruposRepository, EntityManagerInterface $em): Response
    {
        $id_bat = trim($_POST["batId"]);
        $id_lot = trim($_POST["lotId"]);
        $cantidad = trim($_POST["cantidad"]);

        $dia = date('j'); //sin ceros
        $mes = date('n'); //sin ceros
        $year = date('Y');//4 digitos
        $hoy = new \DateTime('now');

            //id periodo
        $periodo = $periodosRepository->getPeriodoNow();

        $response = [];
        if(count($periodo) < 1){
            $response['estado'] = "FAILED";
            $response['mensaje'] = "No existe el periodo actual";
            return new Response(json_encode($response), 500, ['Content-Type' => '"application/json"']);
        }
            //dd($periodo);
            //codigo mes
            $calc_mes = ($periodo[0]['perMes']*2)+102;
            $cod_mes = substr($calc_mes,-2);

            //codigo año
            $calc_year = ($periodo[0]['perAnio']*2)+2;
            $cod_year = substr($calc_year,-2);

            //codigo inicial bateria
            //$codigo_bater = generar_codigo_bateria($id_bat);
        $bateria = $bateriasRepository->find($id_bat);
        //dd($bateria);
        if(!$bateria){
            $response['estado'] = "FAILED";
            $response['mensaje'] = "No existe la Bateria con el ID seleccionado, no se puede generar las garantias";
            return new Response(json_encode($response), 500, ['Content-Type' => '"application/json"']);
        }
        $grupo = $gruposRepository->findBy(['gruCodigo'=>$bateria->getBatCodGrupo()])[0];
        if(!$grupo){
            $response['estado'] = "FAILED";
            $response['mensaje'] = "No existe un Grupo de bateria para el modelo seleccionado, no se puede generar las garantias";
            return new Response(json_encode($response), 500, ['Content-Type' => '"application/json"']);
        }

            //echo "<div class='alert alert-success' role='alert'><img src='img/ok.png'> $cantidad garantías generadas....!!";
            //echo "<div class='alert alert-success' role='information'> Escoja un Nuevo Modelo....!!";

        //secuencial
        $secuencial = $grupo->getGruSecuencia();
        $capacid_columna = ceil($cantidad/3);
        $limite= $secuencial+$cantidad;
        //dd($limite);
        $orden=0;
            //$body = "";

        $marca_time = new \DateTime('now');

            //$sql_in_gar = "UPDATE lote SET saldo_lote = saldo_lote - '$cantidad' WHERE id_lote = '$id_lot';";
            //echo $sql_in_ger;
            //$resp_in_gar = mysql_query($sql_in_gar);

        //EXTRAE CÒDIGO de Grupo
        $nuevas_garantias = [];
        $cdgGrp = $grupo->getGruCodigo();
        for($i=$secuencial; $i<$limite; $i++){
            $calc_secuencial = $i+1000000;
            $cod_secuencial = substr($calc_secuencial,-6);
            if($cdgGrp == 'N'){
                $garantia = $bateria->getBatCodInicio(). $cod_mes.$cod_year.$cod_secuencial;
            }else{
                $garantia = $cdgGrp.$bateria->getBatCodInicio().$cod_mes.$cod_year.$cod_secuencial;
            }

            $codGaran = $bateria->getBatCodInicio().$cod_mes.$cod_year.$cod_secuencial;
            $orden++;
            //$sql_filtro_gar = "SELECT id_garantia FROM Garantia WHERE codigo_gar='$garantia';";
            //$resp_filto_gar = mysql_query($sql_filtro_gar);
            $tot_filtro_gar = $garantiasRepository->getExiste($garantia);
            //llenar la columna 2
            if(count($tot_filtro_gar) > 0){

            }else{
                $garantia_new = new Garantias();
                $garantia_new->setPerId($periodo[0]["id"]);
                $garantia_new->setLotId($id_lot);
                $garantia_new->setGruCodigo($cdgGrp);
                $garantia_new->setGarMarTime($marca_time);
                $garantia_new->setGarFecha($hoy);
                $garantia_new->setGarCodSimple("");
                $garantia_new->setGarCodigo($codGaran);
                $garantia_new->setBarId($id_bat);
                $garantia_new->setGarCodQr(0);
                $em->persist($garantia_new);
                $em->flush();
                $nuevas_garantias[] = $garantia_new->getId();
                //$nuevas_garantias[]['perId'] = $garantia_new->getPerId();
                //$nuevas_garantias[]['lotId'] = $garantia_new->getPerId();
            }
        }


            if($orden == $cantidad){
                $limite= substr(($limite+1000000),-6);
                $grupo->setGruSecuencia($limite);
                $em->persist($grupo);
                $em->flush();
            }else{
            }
            $garantias = $garantiasRepository->getByIds($nuevas_garantias);
            return new Response(json_encode($garantias), 200, ['Content-Type' => '"application/json"']);
    }

    /**
     * @Route("/generar", name="app_garantias_generar", methods={"GET"})
     */
    public function generar(GarantiasRepository $garantiasRepository, BateriasRepository $bateriasRepository, PeriodosRepository $periodosRepository,
        GruposRepository $gruposRepository): Response
    {
        $baterias = $bateriasRepository->getByEstado(1);
        return $this->render('garantias/generar.html.twig', [
            'baterias' => $baterias,
            'garantias' => $garantiasRepository->findAll(),
            'periodos' => $periodosRepository->getPeriodoNow(),
            'grupos' => $gruposRepository->getAll(),
        ]);
    }

    /**
     * @Route("/", name="app_garantias_index", methods={"GET"})
     */
    public function index(GarantiasRepository $garantiasRepository): Response
    {
        return $this->render('garantias/index.html.twig', [
            'garantias' => $garantiasRepository->getAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_garantias_new", methods={"GET", "POST"})
     */
    public function new(Request $request, GarantiasRepository $garantiasRepository): Response
    {
        $garantia = new Garantias();
        $form = $this->createForm(GarantiasType::class, $garantia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $garantiasRepository->add($garantia);
            return $this->redirectToRoute('app_garantias_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('garantias/new.html.twig', [
            'garantia' => $garantia,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_garantias_show", methods={"GET"})
     */
    public function show(Garantias $garantia): Response
    {
        return $this->render('garantias/show.html.twig', [
            'garantia' => $garantia,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_garantias_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Garantias $garantia, GarantiasRepository $garantiasRepository): Response
    {
        $form = $this->createForm(GarantiasType::class, $garantia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $garantiasRepository->add($garantia);
            return $this->redirectToRoute('app_garantias_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('garantias/edit.html.twig', [
            'garantia' => $garantia,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_garantias_delete", methods={"POST"})
     */
    public function delete(Request $request, Garantias $garantia, GarantiasRepository $garantiasRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$garantia->getId(), $request->request->get('_token'))) {
            $garantiasRepository->remove($garantia);
        }

        return $this->redirectToRoute('app_garantias_index', [], Response::HTTP_SEE_OTHER);
    }
}
