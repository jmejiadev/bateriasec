<?php

namespace App\Controller;

use App\Entity\Despachos;
use App\Entity\DetalleFac;
use App\Entity\Distribucion;
use App\Entity\Facturas;
use App\Form\DistribucionType;
use App\Repository\BateriasRepository;
use App\Repository\CodigosQrRepository;
use App\Repository\DespachosRepository;
use App\Repository\DetalleFacRepository;
use App\Repository\DistribucionRepository;
use App\Repository\FacturasRepository;
use App\Repository\GarantiasRepository;
use App\Repository\PaletsBateriasRepository;
use App\Repository\PaletsRepository;
use App\Repository\RecepcionRepository;
use App\Repository\SeccionRepository;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/distribucion")
 */
class DistribucionController extends AbstractController
{
    /**
     * Guarda registros de reprocesos
     *
     * @Route("/guarda-reprocesos", name="reproceso_guarda_registros", methods={"POST","GET"})
     */
    public function guardaReproceso(GarantiasRepository $garantiasRepository, BateriasRepository $bateriasRepository, EntityManagerInterface $entityManager,
                    DistribucionRepository $distribucionRepository, RecepcionRepository $recepcionRepository, CodigosQrRepository $codigosQrRepository,
                    PaletsBateriasRepository $paletsBateriasRepository)
    {

        $datos = $_POST['data'];
        $batId = $_POST['batId'];
        $bateria = $bateriasRepository->find($batId);
        //dd($datos);
        foreach ($datos as $data){
            $garantia =  $garantiasRepository->findBy(['garCodigo'=>$data['codigo_gar']])[0];
            $garantia->setBarId($batId);
            $entityManager->persist($garantia);

            $codQr = $codigosQrRepository->findBy(['codigo_gar'=>$data['codigo_gar']])[0];
            $codQr->setModelo($bateria->getBatNomProdNeural());
            $codQr->setNomProdNeural($bateria->getBatNomProdNeural());
            $codQr->setCodEstado("REPROCESADO");
            $entityManager->persist($codQr);

            $distribucion = $distribucionRepository->findBy(['codigo_gar'=>$data['codigo_gar']])[0];
            $entityManager->remove($distribucion);

            $recepcion = $recepcionRepository->findBy(['codigo_gar'=>$data['codigo_gar']])[0];
            $entityManager->remove($recepcion);

            $pallet = $paletsBateriasRepository->findBy(['codigo_gar'=>$data['codigo_gar']])[0];
            $entityManager->remove($pallet);

        }

        $entityManager->flush();
        return new Response(json_encode('ok'),200, array('Content-Type' => 'application/json'));
    }

    /** index baterias para reprocesos
     * @Route("/index-baterias-reprocesos", name="reproceso_baterias_index", methods={"GET","POST"})
     */
    public function indexReprocesosBaterias(DistribucionRepository $distribucionRepository, BateriasRepository $bateriasRepository): Response{
        return $this->render('distribucion/index_reproceso.html.twig', [
            'baterias' => $distribucionRepository->getBateriasDistribuidas(),
            'modelos' => $bateriasRepository->getByEstado(1)
        ]);
    }

    /**
     * @Route("/distribucion/remueve/facturas/{facId}/{regId}/{fecDesde}/{fecHasta}", name="distribucion_remove_facturas", methods={"GET", "POST"})
     */
    public function disRemoveFacturas($facId, $regId, $fecDesde, $fecHasta, DistribucionRepository $distribucionRepository,
                                      CodigosQrRepository $codigosQrRepository, DespachosRepository $despachosRepository, FacturasRepository $facturasRepository,
                                        DetalleFacRepository $detalleFacRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        //$datFacura = $usersRepository->getDetalleFactura($codDetFactura);
        $registros = $despachosRepository->getDespachosByCodDetFac($regId);
        //dd($registros);
        foreach ($registros as $registro){
            $des = $despachosRepository->find($registro['id']);
            $dis = $distribucionRepository->find($des->getDistribucion()->getId());
            $cod = $codigosQrRepository->findBy(['codigo_gar'=>$dis->getCodigoGar()])[0];
            $cod->setCodEstado('DISTRIBUIDO');
            $em->persist($cod);

            $dis->setDisEstado('INGRESADO');
            $dis->setDisFecDespacho(null);
            $em->persist($dis);
            $em->remove($des);
        }
        $em->flush();

        /*$registros = $usersRepository->getDetallesFactura($codfac);

        foreach ($registros as $key=>$registro){
            $existe = $despachosRepository->findBy(['codDetFac'=>$registro['coddetfac']]);
            if($existe){
                $registros[$key]['detFacEstado'] = "ASIGNADO";
            }
        }*/

        //$factura = $facturasRepository->findBy(['detalleFac'=> $regId])[0];

        //Elimina el registro en detalle de la factura
        $detalle = $detalleFacRepository->find($regId);
        if(!$detalle){

        }else{
            $detalle->setRegEstado('REGISTRADA');
            $em->persist($detalle);
            $em->flush();
        }

        $detEstado = $detalleFacRepository->findBy(['facturas'=>$facId, 'regEstado'=>['PARCIAL', 'COMPLETADO']]);
        if(count($detEstado)<1){
            $detalle->getFacturas()->setFacEstado('REGISTRADA');
        }else{
            $detalle->getFacturas()->setFacEstado('PARCIAL');
        }
        $detalle->getFacturas()->setFacFecDespacho(null);
        $em->persist($detalle);
        $em->flush();

        return $this->redirectToRoute('facturas_detalle',['facId'=>$facId, 'fecDesde'=>$fecDesde, 'fecHasta'=>$fecHasta]);

    }

    /**
     * @Route("/guarda-baterias-asignadas", name="distribucion_guarda_baterias_asignadas", methods={"POST"})
     */
    public function guardaBateriasAsignadas(DistribucionRepository $distribucionRepository, FacturasRepository $facturasRepository,
                                            CodigosQrRepository $codigosQrRepository, DespachosRepository $despachosRepository, DetalleFacRepository $detalleFacRepository): Response
    {
        //dd($_POST);
        $em = $this->getDoctrine()->getManager();
        $factura =  $facturasRepository->find($_POST['idFac']);
        $detalle = $detalleFacRepository->find($_POST['idDet']);
        $registros = json_decode($_POST['data']);
        //dd($registros);
        foreach ($registros as $registro){
            $distribucion = $distribucionRepository->find($registro->id);
            $distribucion->setDisRucFactura($factura->getCustomerNumber());
            $distribucion->setDisStatus(1);
            $em->persist($distribucion);

            $despacho = new Despachos();
            $despacho->setUsers( $this->getUser());
            $despacho->setDetalleFac($detalle);
            $despacho->setDistribucion($distribucion);
            $despacho->setCodGarantia($distribucion->getCodigoGar());
            $despacho->setNumber($factura->getNumFac());
            $despacho->setDescription($detalle->getDescription());
            $despacho->setDescription2($detalle->getDescription2());
            $despacho->setActGarantia(false);
            $em->persist($despacho);
            $distribucion->setDisEstado('DESPACHADO');
            $distribucion->setDisFecDespacho(new \DateTime('now'));
            $em->persist($distribucion);
            $codigoQr = $codigosQrRepository->findBy(['codigo_gar'=>$distribucion->getCodigoGar()])[0];
            $codigoQr->setCodEstado('DESPACHADO');
            $em->persist($codigoQr);
        }
        if(count($registros) < ($detalle->getQuantity() - $_POST['canDespachada'])){
            $detalle->setRegEstado("PARCIAL");
        }else{
            $detalle->setRegEstado("COMPLETADO");
        }
        $em->flush();
        //CAMBIA DE ESTADO A LA FACTURA
        $numDetalle = $detalleFacRepository->getByEstado($_POST['idFac']);
        //dd($detFacErp);

        //dd($bandera1);
        if (count($numDetalle)<1){
                $factura->setFacEstado('COMPLETADA');
                $factura->setFacFecDespacho(new \DateTime('now'));
        }else{
            $factura->setFacEstado('PARCIAL');
        }
        $em->persist($factura);
        $em->flush();

        return $this->redirectToRoute('facturas_detalle', ['facId'=>$_POST['idFac'], 'fecDesde'=> $_POST['fecDesde'], 'fecHasta'=>$_POST['fecHasta']], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/distribucion-agrega-facturas/{regId}/{fecDesde}/{fecHasta}", name="distribucion_add_facturas", methods={"GET"})
     */
    public function disAddFacturas($regId, $fecDesde, $fecHasta, DistribucionRepository $distribucionRepository,
                                   DetalleFacRepository $detalleFacRepository, DespachosRepository $despachosRepository): Response
    {
        $detalle = $detalleFacRepository->find($regId);
        $registros = $distribucionRepository->getBateriasByModelo($detalle->getDescription());
        $canDespachadas = $despachosRepository->findBy(['detalleFac' =>$regId]);
        return $this->render('distribucion/asigna_baterias.html.twig', [
            'registros' => $registros,
            'detalle' => $detalle,
            'fecDesde' => $fecDesde,
            'fecHasta' => $fecHasta,
            'canDespachada' => count($canDespachadas)
        ]);
    }

    /**
     * @Route("/distribucion-palets-index", name="distribucion_palets_index", methods={"GET"})
     */
    public function indexDistribucion(PaletsRepository $paletsRepository): Response
    {
        return $this->render('distribucion/index_palets.html.twig', [
            'palets' => $paletsRepository->getByEstado(['RECIBIDO', 'DISTRIBUIDO']),
        ]);
    }

    /**
     * Consulta detalle x seccion
     *
     * @Route("/detalle-distribucion", name="distribucion_consulta_detalle", methods={"POST","GET"})
     */
    public function detalleDistribucion(RecepcionRepository $recepcionRepository, DistribucionRepository $distribucionRepository, EntityManagerInterface $entityManager)
    {
        //$datos = $_POST['data'];
        $secId = $_POST['secId'];
        //dd($secId);
        $resultado = $distribucionRepository->getDistribucionBySeccion($secId);
        //dd($resultado);

        return new Response(json_encode($resultado),200, array('Content-Type' => 'application/json'));
    }

    /**
     * Mover a fifo
     *
     * @Route("/distribucion-mover-fifo", name="distribucion_mover_fifo", methods={"POST","GET"})
     */
    public function distribucionMoverFifo( DistribucionRepository $distribucionRepository, EntityManagerInterface $entityManager, SeccionRepository $seccionRepository)
    {
        $registros = $_POST['data'];
        foreach ($registros as $registro){
            $distribucion = $distribucionRepository->findBy(['codigo_gar'=> $registro['codigo_gar']])[0];
            $distribucion->setDisSecOrigen($distribucion->getSecciones()->getId());
            $distribucion->setSecciones($seccionRepository->findby(['racks'=>12])[0]);
            $entityManager->persist($distribucion);
        }
        $entityManager->flush();
        return new Response(json_encode("OK"),200, array('Content-Type' => 'application/json'));
    }

    /**
     * Guarda registros de distribucion
     *
     * @Route("/guarda-distribucion", name="distribucion_guarda_registros", methods={"POST","GET"})
     */
    public function guardaDistribucion(RecepcionRepository $recepcionRepository, SeccionRepository $seccionRepository, EntityManagerInterface $entityManager,
                                        PaletsRepository $paletsRepository, CodigosQrRepository $codigosQrRepository)
    {
        if(isset($_POST['data'])){
            $datos = $_POST['data'];
        }
        $palet = null;
        if(isset($_POST['palet'])){
            $palet = $paletsRepository->find($_POST['palet']);
            $datos = $recepcionRepository->getRegistrosByPalet($_POST['palet']);
        }

        $secId = $_POST['secId'];
        //dd($datos);
        foreach ($datos as $data){
            $registro = new Distribucion();
            $registro->setPalets($palet);
            $registro->setCodigoGar($data['codigo_gar']);
            $registro->setModelo($data['modelo']);
            $registro->setDisStatus(0);
            if(isset($_POST['palet'])) {
                $registro->setRecepcion($recepcionRepository->find($data['id']));
            }else{
                $registro->setRecepcion($recepcionRepository->find($data['recId']));
            }
            $registro->setDisFecDistribucion(new \DateTime( "now", new \DateTimeZone( 'America/Guayaquil' ) ));
            $registro->setDisEstado("INGRESADO");
            $registro->setDisNumLote($data['num_lote']);
            $registro->setDisFecDespacho(null);
            $registro->setSecciones($seccionRepository->find($secId));

            $entityManager->persist($registro);
            $codigoQr = $codigosQrRepository->findBy(['codigo_gar'=> $data['codigo_gar']])[0];
            $codigoQr->setCodEstado("DISTRIBUIDO");
            $entityManager->persist($codigoQr);
        }
        if(isset($_POST['palet'])) {
            $palet->setPalEstado("DISTRIBUIDO");
            $entityManager->persist($palet);
        }

        $entityManager->flush();
        return new Response(json_encode('ok'),200, array('Content-Type' => 'application/json'));
    }

    /** index baterias distribucion
     * @Route("/index-baterias-distribucion", name="distribucion_baterias_index", methods={"GET","POST"})
     */
    public function indexDistribucionBaterias(DistribucionRepository $distribucionRepository): Response{
        return $this->render('distribucion/index_distribucion.html.twig', [
            'baterias' => $distribucionRepository->getBateriasNoDistribuidas(),
        ]);
    }

    /**
     * @Route("/", name="distribucion_index", methods={"GET"})
     */
    public function index(DistribucionRepository $distribucionRepository): Response
    {
        return $this->render('distribucion/index.html.twig', [
            'distribucions' => $distribucionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="distribucion_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $distribucion = new Distribucion();
        $distribucion->setDisStatus(0);
        $form = $this->createForm(DistribucionType::class, $distribucion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($distribucion);
            $entityManager->flush();

            return $this->redirectToRoute('distribucion_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('distribucion/new.html.twig', [
            'distribucion' => $distribucion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="distribucion_show", methods={"GET"})
     */
    public function show(Distribucion $distribucion): Response
    {
        return $this->render('distribucion/show.html.twig', [
            'distribucion' => $distribucion,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="distribucion_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Distribucion $distribucion, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(DistribucionType::class, $distribucion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('distribucion_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('distribucion/edit.html.twig', [
            'distribucion' => $distribucion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="distribucion_delete", methods={"POST"})
     */
    public function delete(Request $request, Distribucion $distribucion, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$distribucion->getId(), $request->request->get('_token'))) {
            $entityManager->remove($distribucion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('distribucion_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/guarda-baterias-asignadas/app", name="distribucion_guarda_baterias_asignadas_app", methods={"POST"})
     */
    public function guardaBateriasAsignadasApp(DistribucionRepository $distribucionRepository, FacturasRepository $facturasRepository,
            CodigosQrRepository $codigosQrRepository, UsersRepository $usersRepository, DetalleFacRepository $detalleFacRepository): Response
    {
        //dd($_POST);
        $em = $this->getDoctrine()->getManager();
        $factura =  $facturasRepository->find($_POST['idFac']);
        $detalle = $detalleFacRepository->find($_POST['idDet']);
        $registros = json_decode($_POST['data']);

        foreach ($registros as $registro){
            $distribucion = $distribucionRepository->findBy(['codigo_gar'=>$registro])[0];
            $distribucion->setDisRucFactura($factura->getCustomerNumber());
            $distribucion->setDisStatus(1);
            $em->persist($distribucion);

            $despacho = new Despachos();
            $despacho->setUsers( $usersRepository->find(1));
            $despacho->setDetalleFac($detalle);
            $despacho->setDistribucion($distribucion);
            $despacho->setCodGarantia($distribucion->getCodigoGar());
            $despacho->setNumber($factura->getNumFac());
            $despacho->setDescription($detalle->getDescription());
            $despacho->setDescription2($detalle->getDescription2());
            $despacho->setActGarantia(false);
            $em->persist($despacho);

            $distribucion->setDisEstado('DESPACHADO');
            $distribucion->setDisFecDespacho(new \DateTime('now'));
            $em->persist($distribucion);

            $codigoQr = $codigosQrRepository->findBy(['codigo_gar'=>$distribucion->getCodigoGar()])[0];
            $codigoQr->setCodEstado('DESPACHADO');
            $em->persist($codigoQr);
        }
        if(count($registros) < ($detalle->getQuantity() - $_POST['canDespachada'])){
            $detalle->setRegEstado("PARCIAL");
        }else{
            $detalle->setRegEstado("COMPLETADO");
        }
        $em->flush();
        //CAMBIA DE ESTADO A LA FACTURA
        $numDetalle = $detalleFacRepository->getByEstado($_POST['idFac']);
        //dd($detFacErp);

        //dd($bandera1);
        if (count($numDetalle)<1){
            $factura->setFacEstado('COMPLETADA');
            $factura->setFacFecDespacho(new \DateTime('now'));
        }else{
            $factura->setFacEstado('PARCIAL');
        }
        $em->persist($factura);
        $em->flush();

        return new Response(json_encode($registros),200, array('Content-Type' => 'application/json'));
    }
}
