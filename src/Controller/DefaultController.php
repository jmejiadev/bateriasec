<?php

namespace App\Controller;

use App\Entity\CodigosQr;
use App\Entity\Recepcion;
use App\Entity\Status;
use App\Repository\BodegasRepository;
use App\Repository\CodigosQrRepository;
use App\Repository\DespachosRepository;
use App\Repository\DetalleFacRepository;
use App\Repository\DistribucionRepository;
use App\Repository\FacturasRepository;
use App\Repository\GarantiasRepository;
use App\Repository\RecepcionRepository;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use http\Env;
use OpenApi\Examples\OpenapiSpec\Repository;
use PHPExcel;
use PHPExcel_Cell_DataType;
use PHPExcel_IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Reader\Xls\Color\BIFF8;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpParser\Builder;
use SecIT\SimpleExcelExport\Excel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;


class DefaultController extends AbstractController
{
    /** WebView_Scan
     * @Route("/webview_scan/{detId}", name="default_webview_scan", methods={"GET"})
     */
    public function webViewScan($detId, DetalleFacRepository $detalleFacRepository, DistribucionRepository $distribucionRepository,
        DespachosRepository $despachosRepository
    ): Response{
        date_default_timezone_set('America/Guayaquil');
        $detalle = $detalleFacRepository->find($detId);
        $distribucion = $distribucionRepository->getBateriasByModelo($detalle->getDescription());
        $ubicacion = $distribucionRepository->getUbicacionesByModelo($detalle->getDescription());
        $canDespachadas = $despachosRepository->findBy(['detalleFac' =>$detId]);
        return $this->render('erp_facturas/webview_scan.html.twig', [
            'detalle'=> $detalle,
            'distribucion'=> $distribucion,
            'ubicacion'=> $ubicacion,
            'canDespachada' => count($canDespachadas)
        ]);
    }

    /** WebView_Scan_Recepcion
     * @Route("/webview/scan/recepcion", name="default_webview_scan_recepcion", methods={"GET"})
     */
    public function webViewScanRecepcion(CodigosQrRepository $codigosQrRepository ): Response{
        date_default_timezone_set('America/Guayaquil');
        //$codQrGenerados = $codigosQrRepository->getCodQrGenerados();
        //dd($codQrGenerados);
        return $this->render('codigos_qr/webview_scan_recepcion.html.twig', [
            //'codQrGenerados'=> $codQrGenerados,
        ]);
    }

    /** valida si existe qr en recepcion
     * @Route("/webview_scan_recepcion_valida_qr/", name="default_webview_scan_recepcion_valida_qr", methods={"GET"})
     */
    public function validaExisteQr(CodigosQrRepository $codigosQrRepository ): Response{
        date_default_timezone_set('America/Guayaquil');
        //$codQrGenerados = $codigosQrRepository->getCodQrGenerados();
        //dd($codQrGenerados);
        return new Response(json_encode($response), 200);
    }

    /** Open thor
     * @Route("/open-thor", name="default_open_thor", methods={"GET"})
     */
    public function openThor(): Response
    {
        $fthor = $_ENV['path_thor'];
        shell_exec($fthor);
        // Return the excel file as an attachment
        return new Response(json_encode("ok"),200, array('Content-Type' => 'text/plain'));
    }

    /** Genera CSV
     * @Route("/exportar-csv", name="default_exportar_csv", methods={"POST"})
     */
    public function exportarCsv(): Response
    {

        $data = json_decode($_POST['data']);
        $modelo = $_POST['file'];
        $path = $_ENV['path_files_qr'];
        $file = $modelo . ".txt";
        $txt = fopen($path.$file, "w") or die("Unable to open file!");
        foreach ($data as $row ){
            if($row->modelo == $modelo){
                fwrite($txt, $row->codigo . "\n");
            }
        }
        fclose($txt);
        $txt = file_get_contents($path.$file);
        $response =  array(
            'op' => 'ok',
            //'file' => "data:text/plain;base64," . base64_encode($txt)
        );


        // Return the excel file as an attachment
        return new Response(json_encode($response),200, array('Content-Type' => 'text/plain'));
    }

    /**
     * Genera Comprobante en PDF para impresion
     *
     * @Route("/imprime-qr", name="default_imprime_qr", methods={"POST","GET"})
     */
    public function tirillaAction(CodigosQrRepository $codigosQrRepository)
    {
        //dd($_POST['data']);
        $em = $this->getDoctrine()->getManager();

        /*foreach ($_POST['data'] as $data){
            $codigo = $codigosQrRepository->findBy(['codigo_gar' => $data['codigo']]);
            $codigo[0]->setStaImpresion(1);
            $em->persist($codigo[0]);
        }*/
        //dd($_POST['data']);

        $options = new Options();
        $options->setChroot(dirname($this->getParameter('kernel.project_dir').'/public'));
        $options->set('isRemoteEnabled',TRUE);

        $dompdf = new Dompdf($options);
        $contxt = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed'=> TRUE
            ]
        ]);
        $dompdf->setHttpContext($contxt);
        $paper_size = array(0,0,71,71);
        $dompdf->set_paper($paper_size);
        $html = $this->renderView('codigos_qr/tirilla.html.twig',array(
            'codigos' => $_POST['data'],
        ));

        // Load HTML to Dompdf $dompdf->loadHtml($html); // (Optional) Setup the paper size and orientation 'portrait' or 'portrait' $dompdf->setPaper('A4', 'portrait'); // Render the HTML as PDF $dompdf->render(); // Output the generated PDF to Browser (inline view) $dompdf->stream("mypdf.pdf", [ "Attachment" => false ]); } }
        $dompdf->load_html($html);
        $dompdf->render();

        $output = $dompdf->output();

        $em->flush();
        $pdf = file_put_contents("./tirillas/impresion.pdf", $output);
        //$dompdf->stream("./tirillas/impresion.pdf", array("Attachment" => false));
        return new Response(json_encode('ok'),200, array('Content-Type' => 'application/json'));
    }

    /** genera QR
     * @Route("/genera-qr", name="default_genera_qr", methods={"GET","POST"})
     */
    public function generaQr(UsersRepository $usersRepository, GarantiasRepository $garantiasRepository): Response{
        $em = $this->getDoctrine()->getManager();
        include('lib/phpqrcode/qrlib.php');
        $tempDir = "codigos_qr/";
        $datos = $_POST['data'];
        $fecDesde = $_POST['fecDesde'];
        $fecHasta = $_POST['fecHasta'];
        $codGarantias = [];
        //dd($datos);
        foreach ($datos as $data ) {
            //$codeContents = "\n" . 'Marca   : Ecuador ' . "\n";
            $url = $_ENV['endpoint_garantias'] . "garantia_qr/" . base64_encode(str_replace(" ","%20",$data['nombProdNeural'])) . "/" . base64_encode($data['codigo_gar']);
            $codeContents = $url . "\n";
            //$codeContents .= "\n" .'Modelo: ' . $data['nombProdNeural'] . "\n";
            //$codeContents .= 'Lote     : 1' . "\n";
            //$codeContents .= 'Serie    : ' . $data['codigo_gar'] . "\n";
            //$codeContents .= 'Fecha    : ' . $data['fecha_gar']."\n";
            try {
                \QRcode::png($codeContents, $tempDir . $data['codigo_gar'] . '.png', 'H', 2.4358974358974358, 1, true);
                $paths[] = $tempDir . $data['codigo_gar'] . '.png';
                $codGarantias[] = $data['codigo_gar'];
            }catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
            $codigo = new CodigosQr();
            $codigo->setCodigoGar($data['codigo_gar']);
            $codigo->setPathQr($tempDir . $data['codigo_gar'] . '.png');
            $codigo->setStaEstado(1);
            $codigo->setStaImpresion(1);
            $codigo->setFechaGar($garantiasRepository->findBy(['garCodigo'=>$data['codigo_gar']])[0]->getGarFecha());
            $codigo->setModelo($data['nombProdNeural']);
            $codigo->setNomProdNeural($data['nombProdNeural']);
            $codigo->setTipoBater($data['tipo_bater']);
            $codigo->setNumLote($data['id_lote']);
            $codigo->setFecCreacion(new \DateTime('now'));
            $codigo->setCodEstado('GENERADO');
            $codigo->setCodigoGrupo($data['codigoGrupo']);
            $codigo->setCodigoGrupoGarantia($data['codigoGrupo'] . substr($data['codigo_gar'], -6, 6));
            $codigo->setUrl($url);
            $em->persist($codigo);

            //ACTUALIZA ESTADO DE LA GARANTIA A CODIGO GENERADO 1
            $garantiasRepository->findBy(['garCodigo'=>$data['codigo_gar']])[0]->setGarCodQr(1);
        }
        $em->flush();

        $baterias = $usersRepository->getBateriasGeneradas($codGarantias);
        //dd($baterias);

        //return $this->redirectToRoute('default_erp_baterias_index');
        return new Response(json_encode($baterias),200, array('Content-Type' => 'application/json'));
    }

    /** index erp_baterias
     * @Route("/index-baterias/{fecDesde}/{fecHasta}", name="default_erp_baterias_index", methods={"GET","POST"})
     */
    public function indexErpBaterias($fecDesde, $fecHasta, UsersRepository $usersRepository, CodigosQrRepository $codigosQrRepository): Response{
        date_default_timezone_set('America/Guayaquil');
        $existe = false;
        //dd($this->getParameter('kernel.project_dir').'/public');
        if($fecDesde == '-'){
            $d = date('d');
            $m = date('m');
            $y = date('Y');
            //$d = cal_days_in_month(CAL_GREGORIAN,$m,$y);
            $fecDesde = $y . "-" . $m . "-" . $d;
            $fecHasta = $y . "-" . $m . "-" . $d;
            //$codSiImp = $codigosQrRepository->getCodigosByStaImpresion(true, $fecDesde, $fecHasta);
            //if($codSiImp){ $existe = true; }
        }else{
            $codSiImp = $codigosQrRepository->getCodigosByStaImpresion(true, $fecDesde, $fecHasta);
            //$fecDesde = date('Y-m-d',$fecDesde);
            //$fecHasta = date('Y-m-d',$fecHasta);
        }

        //if($existe){
            $baterias = [];
        //}else{
            //$baterias = $usersRepository->getBateriasAll($codSiImp, null, $fecDesde, $fecHasta);
        //}
        $baterias = $usersRepository->getBateriasAll($fecDesde, $fecHasta,0);
        //dd($baterias);
        return $this->render('erp_baterias/index.html.twig', [
            'fecDesde'=> $fecDesde,
            'fecHasta'=> $fecHasta,
            'baterias' => $baterias,
        ]);
    }

    /** index erp_baterias reimpresión
     * @Route("/index-baterias/reimpresion/{fecDesde}/{fecHasta}", name="default_erp_baterias_reimpresion_index", methods={"GET","POST"})
     */
    public function indexErpBateriasReimpresion($fecDesde, $fecHasta, UsersRepository $usersRepository, CodigosQrRepository $codigosQrRepository): Response{
        $existe = false;
        if($fecDesde == '-'){
            $d = date('d');
            $m = date('m');
            $y = date('Y');
            $fecDesde = $y . "-" . $m . "-" . $d;
            $fecHasta = $y . "-" . $m . "-" . $d;

        }
        //$codGarantias = $codigosQrRepository->getCodigosByStaImpresion(true, $fecDesde, $fecHasta);
        //dd($codGarantias);
        $baterias = $usersRepository->getBateriasAll($fecDesde, $fecHasta,1);
        return $this->render('erp_baterias/reimpresion-index.html.twig', [
            'fecDesde'=> $fecDesde,
            'fecHasta'=> $fecHasta,
            'baterias' => $baterias,
        ]);
    }


    /** index erp_facturas
     * @Route("/index-facturas/{fecDesde}/{fecHasta}", name="default_erp_facturas_index", methods={"GET"})
     */
    public function indexErpFacturas($fecDesde, $fecHasta, FacturasRepository $facturasRepository): Response{
        //dd(json_encode($usersRepository->getBateriasAll()));
        //dd($fecDesde);
        if($fecDesde == '-'){
            $fecDesde = date('Y-m-d');
            $fecHasta = date('Y-m-d');
        }else{

        }
        $facturas = $facturasRepository->getByFechas($fecDesde, $fecHasta);
        //$apiController = new ApiController();
        /*$facturas = $apiController->indexFacturas( new Request([
            "fecDesde" => $fecDesde,
            "fecHasta" => $fecHasta,
            ]), $facturasRepository);*/
        //dd($facturas);
        //$facturas = json_decode(json_decode($facturas->getContent())->facturas);

        //dd($facturas);
        return $this->render('erp_facturas/index.html.twig', [
            'facturas' => $facturas,
            'fecDesde' => $fecDesde,
            'fecHasta' => $fecHasta
        ]);
    }

    /** detalle erp_facturas
     * @Route("/detalle-factura/{facId}/{fecDesde}/{fecHasta}", name="facturas_detalle", methods={"GET"})
     */
    public function detalleFactura($facId, $fecDesde, $fecHasta, FacturasRepository $facturasRepository, DetalleFacRepository $detalleFacRepository): Response
    {
        //$apiController = new ApiController();
        //$facturas = $apiController->detalleFactura( new Request([
        $detalle = $detalleFacRepository->getByFactura($facId);
        //$registros = json_decode(json_decode($facturas->getContent())->detalles);
        //$factura = json_decode(json_decode($facturas->getContent())->factura);

        return $this->render('erp_facturas/detalles.html.twig', [
            'detalle' => $detalle,
            'facId'=> $facId,
            'fecDesde' => $fecDesde,
            'fecHasta' => $fecHasta,
        ]);
    }

    /** Genera XLSX
     * @Route("/exportar-xls", name="default_exportar_xls", methods={"POST"})
     */
    public function exportarXls(CodigosQrRepository $codigosQrRepository): Response
    {
        $user = $this->getUser();
        $spreadsheet = new Spreadsheet();
        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $data = json_decode($_POST['data']);
        //dd($data);
        $colNames = json_decode($_POST['colNames']);
        $titulo = $_POST['titulo'];
        if(isset($_POST['reporte'])) {
            $reporte = $_POST['reporte'];
        }else{
            $reporte = "";
        }

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($titulo);


        //$sheet->setCellValue('A3', 'Fecha Reporte: '. date('Y-m-d'));

        $line = 1;
        $col = "A";
        for($i=0;$i<count($colNames);$i++){
            $sheet->setCellValue($col . $line, $colNames[$i]);
            $sheet->getColumnDimension($col)->setAutoSize(true);
            $col++;
        }
        /*$rowArray = $colNames;
        $columnArray = array_chunk($rowArray, 1);
        $spreadsheet->getActiveSheet()
            ->fromArray(
                $columnArray,   // The data to set
                NULL,           // Array values with this value will not be set
                'A1'            // Top left coordinate of the worksheet range where
            //  we want to set these values (default is A1)
            );*/


        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        //$sheet->getStyle('A1:'. $col . $line)->applyFromArray($styleArray)->getFont()->setBold( true );

        $line++;
        $col = "A";

        foreach ($data as $row){
            $row = (array)$row;
            unset($row['dataindex']);
            $i=0;
            $codigoQr = $codigosQrRepository->findBy(['codigo_gar'=>$row['codigo_gar']])[0];
            foreach($row as $column){
                //print_r($row);
                if($i<count($colNames)-4){
                    $sheet->setCellValueExplicit($col . $line, $column, DataType::TYPE_STRING);
                    $col++;
                }
                if($i==count($colNames)-3){
                    $sheet->setCellValue($col . $line, $row['codigoGrupo'] . substr($row['codigo_gar'], -6, 6));
                    $col++;
                }
                if($i==count($colNames)-2){
                    $sheet->setCellValue($col . $line, $row['codigoGrupo'] . $row['codigo_gar']);
                    $col++;
                }
                if($i==count($colNames)-1){
                    $sheet->setCellValue($col . $line,  substr($row['codigo_gar'], 0, 6));
                    $col++;
                }
                if($i==count($colNames)){
                    $sheet->setCellValue($col . $line, $codigoQr->getUrl());
                    $col++;
                }
                $i++;
            }
            $colC = $col;
            $col = "A";
            $line++;
        }
        $line--;
        //$sheet->getStyle('A5:'. $colC . $line)->applyFromArray($styleArray)->getFont()->setBold( false );
        // Create your Office 2007 Excel (XLSX Format)
        $spreadsheet->setActiveSheetIndex(0);
        $filePath = 'C:\\DOCUMENTO_LASER\\' . $_POST['file'] . '.xls';

        header('Content-Type: application/vnd.ms-excel');
        header('Cache-Control: max-age=0');



        try {
            $writer = new Xls($spreadsheet);
            //$writer->save('php://output');
            $writer->save($filePath);
        } catch(Exception $e) {
            exit($e->getMessage());
        }
        $writer = new Xls($spreadsheet);
        ob_start();
        $writer->save("php://output");
        $xlsData = ob_get_contents();
        ob_end_clean();
        $response =  array(
            'op' => 'ok',
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($xlsData)
        );

        // Return the excel file as an attachment
        return new Response(json_encode($response),200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route({
     *     "en": "/",
     *     "es": "/"
     * }, name="home")
     */
    public function indexAction(UsersRepository $usersRepository)
    {
        date_default_timezone_set('America/Guayaquil');
        $user = $this->getUser();
        if (is_object($user)) {
            if(in_array("ADMINISTRADOR",$user->getRoles())) {
                return $this->redirectToRoute('default_erp_baterias_index', ['fecDesde'=>'-', 'fecHasta'=>'-']);
            }else{
                if(in_array("DISTRIBUIDOR",$user->getRoles())) {
                    return $this->redirectToRoute('app_despachos_index');
                }else{
                    if(in_array("RECEPCION",$user->getRoles())) {
                        return $this->redirectToRoute('recepcion_baterias_index');
                    }else {
                        if(in_array("DISTRIBUCION",$user->getRoles())) {
                            return $this->redirectToRoute('distribucion_baterias_index');
                        }else {
                            if(in_array("DISTRIBUCION_DESPACHO",$user->getRoles())) {
                                return $this->redirectToRoute('default_erp_facturas_index', ['fecDesde' => '-', 'fecHasta' => '-']);
                            }else {
                                if(in_array("INGRESO_PRODUCTO",$user->getRoles())) {
                                    return $this->redirectToRoute('users_index');
                                }else {
                                    return $this->redirectToRoute('default_erp_baterias_index', ['fecDesde' => '-', 'fecHasta' => '-']);
                                }
                            }
                        }
                    }
                }
            }
        } else {

            //$baterias = $usersRepository->getBateriasAll();
            //dd($baterias);
            //$facturas = $usersRepository->getFacturasAll();
            //dd($facturas);
            return $this->redirectToRoute('fos_user_security_login', ['_locale' => 'es']);
        }
    }

    /** Cambia el idioma
     * @Route("/cambia-idioma", name="default_cambia_idioma", methods={"POST","GET"})
     */
    public function cambiaIdioma(Request $request)
    {
        $lang = $_POST['lang'];
        $route = $_POST['route'];
        $parameters = json_decode($_POST['parameters'],true);
        unset($parameters['_locale']);
        $parameters['_locale']=$lang;
        return $this->redirectToRoute($route,$parameters);
    }

}
