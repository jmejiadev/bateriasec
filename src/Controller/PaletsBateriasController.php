<?php

namespace App\Controller;

use App\Entity\PaletsBaterias;
use App\Form\PaletsBateriasType;
use App\Repository\PaletsBateriasRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/palets/baterias")
 */
class PaletsBateriasController extends AbstractController
{
    /**
     * @Route("/", name="palets_baterias_index", methods={"GET"})
     */
    public function index(PaletsBateriasRepository $paletsBateriasRepository): Response
    {
        return $this->render('palets_baterias/index.html.twig', [
            'palets_baterias' => $paletsBateriasRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="palets_baterias_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $paletsBateria = new PaletsBaterias();
        $form = $this->createForm(PaletsBateriasType::class, $paletsBateria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($paletsBateria);
            $entityManager->flush();

            return $this->redirectToRoute('palets_baterias_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('palets_baterias/new.html.twig', [
            'palets_bateria' => $paletsBateria,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="palets_baterias_show", methods={"GET"})
     */
    public function show(PaletsBaterias $paletsBateria): Response
    {
        return $this->render('palets_baterias/show.html.twig', [
            'palets_bateria' => $paletsBateria,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="palets_baterias_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, PaletsBaterias $paletsBateria, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PaletsBateriasType::class, $paletsBateria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('palets_baterias_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('palets_baterias/edit.html.twig', [
            'palets_bateria' => $paletsBateria,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="palets_baterias_delete", methods={"POST"})
     */
    public function delete(Request $request, PaletsBaterias $paletsBateria, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$paletsBateria->getId(), $request->request->get('_token'))) {
            $entityManager->remove($paletsBateria);
            $entityManager->flush();
        }

        return $this->redirectToRoute('palets_baterias_index', [], Response::HTTP_SEE_OTHER);
    }
}
