<?php

namespace App\Controller;

use App\Entity\Periodos;
use App\Form\PeriodosType;
use App\Repository\PeriodosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/periodos")
 */
class PeriodosController extends AbstractController
{
    /** CAMBIA DE ESTADO AL REGISTRO
     * @Route("/cambia-estado-registro/{id}/{estado}", name="app_periodos_cambia_estado", methods={"GET"})
     */
    public function cambiaEstado($id, $estado, PeriodosRepository $periodosRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $registro = $em->getRepository(Periodos::class)->find($id);
        $registro->setPerEstado($estado);
        $em->persist($registro);
        $em->flush();
        return $this->redirectToRoute('app_periodos_index');
    }

    /**
     * @Route("/", name="app_periodos_index", methods={"GET"})
     */
    public function index(PeriodosRepository $periodosRepository): Response
    {
        return $this->render('periodos/index.html.twig', [
            'periodos' => $periodosRepository->getAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_periodos_new", methods={"GET", "POST"})
     */
    public function new(Request $request, PeriodosRepository $periodosRepository): Response
    {
        $periodo = new Periodos();
        $form = $this->createForm(PeriodosType::class, $periodo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $periodosRepository->add($periodo);
            return $this->redirectToRoute('app_periodos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('periodos/new.html.twig', [
            'periodo' => $periodo,
            'periodos' => $periodosRepository->getAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_periodos_show", methods={"GET"})
     */
    public function show(Periodos $periodo): Response
    {
        return $this->render('periodos/show.html.twig', [
            'periodo' => $periodo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_periodos_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Periodos $periodo, PeriodosRepository $periodosRepository): Response
    {
        $form = $this->createForm(PeriodosType::class, $periodo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $periodosRepository->add($periodo);
            return $this->redirectToRoute('app_periodos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('periodos/edit.html.twig', [
            'periodo' => $periodo,
            'periodos' => $periodosRepository->getEdit($periodo->getId()),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_periodos_delete", methods={"POST"})
     */
    public function delete(Request $request, Periodos $periodo, PeriodosRepository $periodosRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$periodo->getId(), $request->request->get('_token'))) {
            $periodosRepository->remove($periodo);
        }

        return $this->redirectToRoute('app_periodos_index', [], Response::HTTP_SEE_OTHER);
    }
}
