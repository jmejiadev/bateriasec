<?php

namespace App\Controller;

use App\Entity\Recepcion;
use App\Form\RecepcionType;
use App\Repository\BodegasRepository;
use App\Repository\CodigosQrRepository;
use App\Repository\PaletsBateriasRepository;
use App\Repository\RecepcionRepository;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/recepcion")
 */
class RecepcionController extends AbstractController
{

    /**
     * Registra reccción de baterias pnc
     *
     * @Route("/recepcion-baterias-pnc", name="recepcion_registro_baterias_pnc", methods={"POST","GET"})
     */
    public function recepcionPncAction(BodegasRepository $bodegasRepository, EntityManagerInterface $entityManager, CodigosQrRepository $codigosQrRepository)
    {
        $datos = $_POST['data'];
        $estado = $_POST['estado'];
        foreach ($datos as $data){
            $codigoQr = $codigosQrRepository->findBy(['codigo_gar' => $data['codigo_gar']])[0];
            $codigoQr->setCodEstado($estado);
            $entityManager->persist($codigoQr);
        }
        $entityManager->flush();
        return new Response(json_encode('ok'),200, array('Content-Type' => 'application/json'));
    }

    /**
     * Registra reccción de baterias
     *
     * @Route("/recepcion-baterias", name="recepcion_registro_baterias", methods={"POST","GET"})
     */
    public function recepcionAction(BodegasRepository $bodegasRepository, EntityManagerInterface $entityManager, CodigosQrRepository $codigosQrRepository)
    {
        $datos = $_POST['data'];
        foreach ($datos as $data){
            $registro = new Recepcion();
            $registro->setCodigoGar($data['codigo_gar']);
            $registro->setBodegas($bodegasRepository->find(1));
            $registro->setRecFecha(new \DateTime( "now", new \DateTimeZone( 'America/Guayaquil' ) ));
            //$registro->setIdBateria($data['id_bateria']);
            //$registro->setCodCrm($data['codCrm']);
            //$registro->setCodigoInicio($data['codigo_inicio']);
            //$registro->setCodigoLP($data['codigoLP']);
            //$registro->setCodNeural($data['codNeural']);
            //$registro->setEstadoBater($data['estado_bater']);
            $registro->setFechaGar($data['fecha_gar']);
            //$registro->setIdGarantia($data['id_garantia']);
            //$registro->setMarcaTimeGar($data['marca_time_gar']);
            $registro->setModelo($data['modelo']);
            //$registro->setNombProdNeural($data['nombProdNeural']);
            //$registro->setNombProducto($data['nombProducto']);
            $registro->setNumLote($data['num_lote']);
            //$registro->setPath('');
            //$registro->setTipoBater($data['tipo_bater']);
            $entityManager->persist($registro);

            $codigoQr = $codigosQrRepository->findBy(['codigo_gar' => $data['codigo_gar']])[0];
            $codigoQr->setCodEstado("RECIBIDO");
            $entityManager->persist($codigoQr);
        }
        $entityManager->flush();
        return new Response(json_encode('ok'),200, array('Content-Type' => 'application/json'));
    }

    /** index erp_baterias recepción
     * @Route("/index-baterias-recepcion/{fecha}", name="recepcion_baterias_index", methods={"GET","POST"})
     */
    public function indexRecepcionBaterias($fecha, RecepcionRepository $recepcionRepository, UsersRepository $usersRepository,
                                           PaletsBateriasRepository $paletsBateriasRepository, CodigosQrRepository $codigosQrRepository): Response{
        date_default_timezone_set('America/Guayaquil');
        $existe = false;
        //dd($this->getParameter('kernel.project_dir').'/public');
        if($fecha == '-'){
            $d = date('d');
            $m = date('m');
            $y = date('Y');
            //$d = cal_days_in_month(CAL_GREGORIAN,$m,$y);
            $fecha = $y . "-" . $m . "-" . $d;
        }
        $baterias = $codigosQrRepository->getCodigosByEstadoFecha(['GENERADO'], $fecha);
        //dd($baterias);
        return $this->render('recepcion/index_recepcion.html.twig', [
            'baterias' => $baterias,
            'fecha' => $fecha,
            'origen' => 'producidas'
        ]);
    }

    /** index erp_baterias recepción
     * @Route("/index-baterias-recepcion-reprocesadas/{fecha}", name="recepcion_baterias_index_reprocesadas", methods={"GET","POST"})
     */
    public function indexRecepcionBateriasReprocesadas($fecha, RecepcionRepository $recepcionRepository, UsersRepository $usersRepository,
                                           PaletsBateriasRepository $paletsBateriasRepository, CodigosQrRepository $codigosQrRepository): Response{
        date_default_timezone_set('America/Guayaquil');
        $existe = false;
        //dd($this->getParameter('kernel.project_dir').'/public');
        if($fecha == '-'){
            $d = date('d');
            $m = date('m');
            $y = date('Y');
            //$d = cal_days_in_month(CAL_GREGORIAN,$m,$y);
            $fecha = $y . "-" . $m . "-" . $d;
        }
        $baterias = $codigosQrRepository->getCodigosByEstado(['REPROCESADO'], $fecha);

        return $this->render('recepcion/index_recepcion.html.twig', [
            'baterias' => $baterias,
            'fecha'=> $fecha,
            'origen' => 'reprocesadas'
        ]);
    }

    /** index erp_baterias recepción mover a PNC
     * @Route("/index-baterias-pnc", name="recepcion_baterias_index_pnc", methods={"GET"})
     */
    public function indexRecepcionBateriasPnc(CodigosQrRepository $codigosQrRepository): Response{
        $baterias = $codigosQrRepository->getCodigosByEstado(['GENERADO']);

        return $this->render('recepcion/index_recepcion_pnc.html.twig', [
            'baterias' => $baterias,
        ]);
    }

    /** Reporte erp_baterias  PNC
     * @Route("/reporte-baterias-pnc", name="reporte_baterias_pnc", methods={"GET"})
     */
    public function reporteRecepcionBateriasPnc(CodigosQrRepository $codigosQrRepository): Response{
        $baterias = $codigosQrRepository->getCodigosByEstado(['MOVIDA A PNC']);

        return $this->render('recepcion/reporte_recepcion_pnc.html.twig', [
            'baterias' => $baterias,
        ]);
    }

    /**
     * @Route("/", name="recepcion_index", methods={"GET"})
     */
    public function index(RecepcionRepository $recepcionRepository): Response
    {
        return $this->render('recepcion/index.html.twig', [
            'recepcions' => $recepcionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="recepcion_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $recepcion = new Recepcion();
        $form = $this->createForm(RecepcionType::class, $recepcion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($recepcion);
            $entityManager->flush();

            return $this->redirectToRoute('recepcion_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('recepcion/new.html.twig', [
            'recepcion' => $recepcion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="recepcion_show", methods={"GET"})
     */
    public function show(Recepcion $recepcion): Response
    {
        return $this->render('recepcion/show.html.twig', [
            'recepcion' => $recepcion,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="recepcion_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Recepcion $recepcion, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RecepcionType::class, $recepcion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('recepcion_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('recepcion/edit.html.twig', [
            'recepcion' => $recepcion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="recepcion_delete", methods={"POST"})
     */
    public function delete(Request $request, Recepcion $recepcion, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$recepcion->getId(), $request->request->get('_token'))) {
            $entityManager->remove($recepcion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('recepcion_index', [], Response::HTTP_SEE_OTHER);
    }
}
