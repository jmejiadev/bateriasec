<?php

namespace App\Controller;

use App\Entity\CodigosQr;
use App\Entity\Despachos;
use App\Form\DespachosType;
use App\Repository\CodigosQrRepository;
use App\Repository\DespachosRepository;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/despachos")
 */
class DespachosController extends AbstractController
{
    /**
     * @Route("/", name="app_despachos_index", methods={"GET"})
     */
    public function index(DespachosRepository $despachosRepository, UsersRepository $usersRepository): Response
    {
        $user = $this->getUser();
        $bandera = $usersRepository->getConGarantias()[0];
        if(in_array("DISTRIBUIDOR", $user->getRoles())){
            $bandera['query_configuracion'] = 1;
            $garantias = $despachosRepository->getBateriasByDespacho($user->getDistribuidores(), $bandera);
        }else{
            $garantias = $despachosRepository->getBateriasByDespacho( null, $bandera);
        }
        return $this->render('despachos/index.html.twig', [
            'garantias' => $garantias,
        ]);
    }

    /**
     * @Route("/valida-codigo-corto", name="despachos_valida_codigo", methods={"POST"})
     */
    public function valCodigo(CodigosQrRepository $codigosQrRepository, UsersRepository $usersRepository): Response
    {
        //dd($_POST);
        $codigo_gar = $_POST['codigo_gar'];

        $existe = $codigosQrRepository->findBy(['codigo_grupo_garantia'=> $codigo_gar, 'actGarantia'=>0]);
        if($existe){
            $response['status'] = "OK";
            $response['menssage'] = 'OK';
            $response['url'] = $existe[0]->getUrl();
        }else{
            $response['status'] = "NOK";
            $response['menssage'] = 'Código no existe o ya se encuentra activo';
            $response['url'] = '';
        }
        //dd($bandera);
        //dd($despachosRepository->getBateriasByDespacho(null, $bandera));
        return new Response(json_encode($response),200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/puntos-de-venta/{token}", name="app_despachos_puntos_de_venta", methods={"GET"})
     */
    public function punVenta(DespachosRepository $despachosRepository, UsersRepository $usersRepository): Response
    {
        $bandera = $usersRepository->getConGarantias()[0];
        //dd($bandera);
        //dd($despachosRepository->getBateriasByDespacho(null, $bandera));
        return $this->render('despachos/puntos_de_venta.html.twig', [
            //'garantias' => $despachosRepository->getBateriasByDespacho(null, $bandera),
        ]);
    }

    /**
     * @Route("/new", name="app_despachos_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $despacho = new Despachos();
        $form = $this->createForm(DespachosType::class, $despacho);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($despacho);
            $entityManager->flush();

            return $this->redirectToRoute('app_despachos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('despachos/new.html.twig', [
            'despacho' => $despacho,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_despachos_show", methods={"GET"})
     */
    public function show(Despachos $despacho): Response
    {
        return $this->render('despachos/show.html.twig', [
            'despacho' => $despacho,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_despachos_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Despachos $despacho, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(DespachosType::class, $despacho);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_despachos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('despachos/edit.html.twig', [
            'despacho' => $despacho,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_despachos_delete", methods={"POST"})
     */
    public function delete(Request $request, Despachos $despacho, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$despacho->getId(), $request->request->get('_token'))) {
            $entityManager->remove($despacho);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_despachos_index', [], Response::HTTP_SEE_OTHER);
    }
}
