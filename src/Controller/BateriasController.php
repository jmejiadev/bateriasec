<?php

namespace App\Controller;

use App\Entity\Baterias;
use App\Form\BateriasType;
use App\Repository\BateriasRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/baterias")
 */
class BateriasController extends AbstractController
{

    /** CAMBIA DE ESTADO AL REGISTRO
     * @Route("/cambia-estado-registro/{id}/{estado}", name="app_baterias_cambia_estado", methods={"GET"})
     */
    public function cambiaEstado($id, $estado, BateriasRepository $bateriasRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $registro = $em->getRepository(Baterias::class)->find($id);
        $registro->setBatEstado($estado);
        $em->persist($registro);
        $em->flush();
        return $this->redirectToRoute('app_baterias_index');
    }
    /**
     * @Route("/", name="app_baterias_index", methods={"GET"})
     */
    public function index(BateriasRepository $bateriasRepository): Response
    {
        return $this->render('baterias/index.html.twig', [
            'baterias' => $bateriasRepository->getAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_baterias_new", methods={"GET", "POST"})
     */
    public function new(Request $request, BateriasRepository $bateriasRepository): Response
    {
        $bateria = new Baterias();
        $form = $this->createForm(BateriasType::class, $bateria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bateriasRepository->add($bateria);
            return $this->redirectToRoute('app_baterias_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('baterias/new.html.twig', [
            'bateria' => $bateria,
            'baterias' => $bateriasRepository->getAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_baterias_show", methods={"GET"})
     */
    public function show(Baterias $bateria): Response
    {
        return $this->render('baterias/show.html.twig', [
            'bateria' => $bateria,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_baterias_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Baterias $bateria, BateriasRepository $bateriasRepository): Response
    {
        $form = $this->createForm(BateriasType::class, $bateria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bateriasRepository->add($bateria);
            return $this->redirectToRoute('app_baterias_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('baterias/edit.html.twig', [
            'bateria' => $bateria,
            'baterias' => $bateriasRepository->getEdit($bateria->getId()),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_baterias_delete", methods={"POST"})
     */
    public function delete(Request $request, Baterias $bateria, BateriasRepository $bateriasRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bateria->getId(), $request->request->get('_token'))) {
            $bateriasRepository->remove($bateria);
        }

        return $this->redirectToRoute('app_baterias_index', [], Response::HTTP_SEE_OTHER);
    }
}
