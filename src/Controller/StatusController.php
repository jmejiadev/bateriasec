<?php

namespace App\Controller;

use App\Entity\Status;
use App\Form\StatusType;
use App\Repository\StatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/status")
 */
class StatusController extends AbstractController
{
    /** CAMBIA DE ESTADO AL REGISTRO
     * @Route("/cambia-estado-registro/{id}/{estado}", name="status_cambia_estado", methods={"GET"})
     */
    public function cambiaEstado($id, $estado, StatusRepository $statusRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $registro = $em->getRepository(Status::class)->find($id);
        $registro->setStaEstado($estado);
        $em->persist($registro);
        $em->flush();
        return $this->render('status/index.html.twig', [
            'statusAll' => $statusRepository->getAll(),
        ]);
    }
    /**
     * @Route("/", name="status_index", methods={"GET"})
     */
    public function index(StatusRepository $statusRepository): Response
    {
        return $this->render('status/index.html.twig', [
            'statusAll' => $statusRepository->getAll(),
        ]);
    }

    /**
     * @Route("/new", name="status_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager, StatusRepository $statusRepository): Response
    {
        $status = new Status();
        $form = $this->createForm(StatusType::class, $status);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($status);
            $entityManager->flush();

            return $this->redirectToRoute('status_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('status/new.html.twig', [
            'statusAll' => $statusRepository->getAll(),
            'status' => $status,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="status_show", methods={"GET"})
     */
    public function show(Status $status): Response
    {
        return $this->render('status/show.html.twig', [
            'status' => $status,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="status_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Status $status, EntityManagerInterface $entityManager, StatusRepository $statusRepository): Response
    {
        $form = $this->createForm(StatusType::class, $status);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('status_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('status/edit.html.twig', [
            'statusAll' => $statusRepository->getEdit($status->getId()),
            'status' => $status,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="status_delete", methods={"POST"})
     */
    public function delete(Request $request, Status $status, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$status->getId(), $request->request->get('_token'))) {
            $entityManager->remove($status);
            $entityManager->flush();
        }

        return $this->redirectToRoute('status_index', [], Response::HTTP_SEE_OTHER);
    }
}
