<?php

namespace App\Controller;

use App\Entity\Racks;
use App\Form\RacksType;
use App\Repository\RacksRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/racks")
 */
class RacksController extends AbstractController
{
    /** CAMBIA DE ESTADO AL REGISTRO
     * @Route("/cambia-estado-registro/{id}/{estado}", name="racks_cambia_estado", methods={"GET"})
     */
    public function cambiaEstado($id, $estado, RacksRepository $racksRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $registro = $em->getRepository(Racks::class)->find($id);
        $registro->setRacEstado($estado);
        $em->persist($registro);
        $em->flush();
        return $this->redirectToRoute('racks_index');

    }

    /**
     * @Route("/", name="racks_index", methods={"GET"})
     */
    public function index(RacksRepository $racksRepository): Response
    {
        return $this->render('racks/index.html.twig', [
            'racks' => $racksRepository->getAll(),
        ]);
    }

    /**
     * @Route("/new", name="racks_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $rack = new Racks();
        $form = $this->createForm(RacksType::class, $rack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($rack);
            $entityManager->flush();

            return $this->redirectToRoute('racks_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('racks/new.html.twig', [
            'rack' => $rack,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="racks_show", methods={"GET"})
     */
    public function show(Racks $rack): Response
    {
        return $this->render('racks/show.html.twig', [
            'rack' => $rack,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="racks_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Racks $rack, EntityManagerInterface $entityManager, RacksRepository $racksRepository): Response
    {
        $form = $this->createForm(RacksType::class, $rack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('racks_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('racks/edit.html.twig', [
            'rack' => $rack,
            'racks' => $racksRepository->getEdit($rack->getId()),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="racks_delete", methods={"POST"})
     */
    public function delete(Request $request, Racks $rack, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$rack->getId(), $request->request->get('_token'))) {
            $entityManager->remove($rack);
            $entityManager->flush();
        }

        return $this->redirectToRoute('racks_index', [], Response::HTTP_SEE_OTHER);
    }
}
