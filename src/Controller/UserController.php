<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\UsersType;
use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/users")
 */
class UserController extends AbstractController
{
    /** VALIDA SI EXISTE UN USUARIO POR USERNAME o EMAIL
     * @Route("/user-existe", name="users_existe", methods={"POST"})
     */
    public function userExiste(Request $request, UsersRepository $usersRepository): Response
    {
        //dd($_POST);
        $username = $_POST['username'];
        $user = $usersRepository->findBy(array('username'=>$username));
        if(!$user){
            $user = $usersRepository->findBy(array('email'=>$username));
            if(!$user){
                $response = false;
            }else{
                $response = true;
            }
        }else{
            $response = true;
        }
        return new Response(json_encode($response), 200, ['Content-Type' => '"application/json"']);
    }

    /** CAMBIA EL ESTADO DEL MENU OPEN-CLOSE
     * @Route("/cambia_estado_menu", name="users_cambia_estado_menu", methods={"POST"})
     */
    public function cambiaEstadoMenu(Request $request, UsersRepository $usersRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $usuLog = $this->getUser();
        $user = $usersRepository->find($usuLog->getId());
        $user->setMenu($_POST['estado']);
        $em->persist($user);
        $em->flush();

        return new Response('',200);
    }

    /** CAMBIA DE ESTADO AL REGISTRO
     * @Route("/cambia-estado-registro/{id}/{estado}", name="users_cambia_estado", methods={"GET"})
     */
    public function cambiaEstado($id, $estado, UsersRepository $usersRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $registro = $em->getRepository(Users::class)->find($id);
        $registro->setEnabled($estado);
        $em->persist($registro);
        $em->flush();
        return $this->redirectToRoute('users_index');
    }

    /**
     * @Route("/", name="users_index", methods={"GET"})
     */
    public function index(UsersRepository $usersRepository): Response
    {
        $usuLog = $this->getUser();
        if ($usuLog->getRoles()[0] == "SUPER_ADMINISTRADOR") {
            return $this->render('users/index.html.twig', [
                'users' => $usersRepository->getAllUser(),
            ]);
        }else{
            if ($usuLog->getRoles()[0] == "ADMINISTRADOR") {
                return $this->render('users/index.html.twig', [
                    'users' => $usersRepository->getAllUser(),
                ]);
            }else{
                return $this->render('users/index.html.twig', [
                    'users' => $usersRepository->getAllUser(),
                ]);
            }
        }

    }

     /**
     * @Route("/new", name="users_new", methods={"GET","POST"})
     */
    public function new(Request $request, UsersRepository $usersRepository, UserPasswordEncoderInterface $encoder): Response
    {
        $usuLog = $this->getUser();
        $user = new Users();
        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);
        $arreglo = $request->request->all();
        //dd($arreglo);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $arreglo = $request->request->all();
            foreach ($arreglo['perfil'] as $perfil){
                $user->addRole($perfil);
            }
            $encoded = $encoder->encodePassword($user, $arreglo['users']['password']['first']);
            $user->setPassword($encoded);
            $user->setLastLogin(new \DateTime('now'));

            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('users_index');
        }
        return $this->render('users/new.html.twig', array(
            'usuarios' => $usersRepository->getAllUser(),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}", name="users_show", methods={"GET"})
     */
    public function show(Users $user): Response
    {
        return $this->render('users/show.html.twig', [
            'users' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="users_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Users $user,UsersRepository $usersRepository): Response
    {
        $usuLog = $this->getUser();

        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            foreach ($user->getRoles() as $role){
                $user->removeRole($role);
            }
            $arreglo = $request->request->all();
            foreach ($arreglo['perfil'] as $perfil){
                $user->addRole($perfil);
            }
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('users_index');
        }

        return $this->render('users/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'usuarios' => $usersRepository->getEditUser($user->getId()),
        ]);
    }

    /**
     * @Route("/{id}", name="users_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Users $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('users_index');
    }
}
