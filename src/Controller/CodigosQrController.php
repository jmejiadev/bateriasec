<?php

namespace App\Controller;

use App\Entity\CodigosQr;
use App\Form\CodigosQrType;
use App\Repository\CodigosQrRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/codigos/qr")
 */
class CodigosQrController extends AbstractController
{
    /**
     * @Route("/update-estado-recepcion-pnc/{codGar}", name="codigos_update_recepcion_pnc", methods={"GET"})
     */
    public function updatePnc($codGar, CodigosQrRepository $codigosQrRepository, EntityManagerInterface $em): Response
    {
        $codigo = $codigosQrRepository->getCodQrGenerados($codGar);
        if($codigo){
            $codigo = $codigosQrRepository->find($codigo[0]['id']);
            $codigo->setCodEstado('MOVIDA A PNC');
            $em->persist($codigo);
            $em->flush();
            return new Response("OK", 200);
        }else{
            return new Response("NOK", 200);
        }
    }

    /**
     * @Route("/update-estado-recepcion/{codGar}", name="codigos_update_recepcion", methods={"GET"})
     */
    public function update($codGar, CodigosQrRepository $codigosQrRepository, EntityManagerInterface $em): Response
    {
        $codigo = $codigosQrRepository->getCodQrGenerados($codGar);
        if($codigo){
            $codigo = $codigosQrRepository->find($codigo[0]['id']);
            $codigo->setCodEstado('RECIBIDO');
            $em->persist($codigo);
            $em->flush();
            return new Response("OK", 200);
        }else{
            return new Response("NOK", 200);
        }
    }

    /**
     * @Route("/", name="codigos_qr_index", methods={"GET"})
     */
    public function index(CodigosQrRepository $codigosQrRepository): Response
    {
        return $this->render('codigos_qr/index.html.twig', [
            'codigos_qrs' => $codigosQrRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="codigos_qr_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $codigosQr = new CodigosQr();
        $form = $this->createForm(CodigosQrType::class, $codigosQr);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($codigosQr);
            $entityManager->flush();

            return $this->redirectToRoute('codigos_qr_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('codigos_qr/new.html.twig', [
            'codigos_qr' => $codigosQr,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="codigos_qr_show", methods={"GET"})
     */
    public function show(CodigosQr $codigosQr): Response
    {
        return $this->render('codigos_qr/show.html.twig', [
            'codigos_qr' => $codigosQr,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="codigos_qr_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, CodigosQr $codigosQr, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CodigosQrType::class, $codigosQr);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('codigos_qr_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('codigos_qr/edit.html.twig', [
            'codigos_qr' => $codigosQr,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="codigos_qr_delete", methods={"POST"})
     */
    public function delete(Request $request, CodigosQr $codigosQr, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$codigosQr->getId(), $request->request->get('_token'))) {
            $entityManager->remove($codigosQr);
            $entityManager->flush();
        }

        return $this->redirectToRoute('codigos_qr_index', [], Response::HTTP_SEE_OTHER);
    }
}
