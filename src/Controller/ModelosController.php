<?php

namespace App\Controller;

use App\Entity\Modelos;
use App\Form\ModelosType;
use App\Repository\ModelosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/modelos")
 */
class ModelosController extends AbstractController
{
    /**
     * @Route("/", name="app_modelos_index", methods={"GET"})
     */
    public function index(ModelosRepository $modelosRepository): Response
    {
        return $this->render('modelos/index.html.twig', [
            'modelos' => $modelosRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_modelos_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ModelosRepository $modelosRepository): Response
    {
        $modelo = new Modelos();
        $form = $this->createForm(ModelosType::class, $modelo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $modelosRepository->add($modelo);
            return $this->redirectToRoute('app_modelos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('modelos/new.html.twig', [
            'modelo' => $modelo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_modelos_show", methods={"GET"})
     */
    public function show(Modelos $modelo): Response
    {
        return $this->render('modelos/show.html.twig', [
            'modelo' => $modelo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_modelos_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Modelos $modelo, ModelosRepository $modelosRepository): Response
    {
        $form = $this->createForm(ModelosType::class, $modelo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $modelosRepository->add($modelo);
            return $this->redirectToRoute('app_modelos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('modelos/edit.html.twig', [
            'modelo' => $modelo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_modelos_delete", methods={"POST"})
     */
    public function delete(Request $request, Modelos $modelo, ModelosRepository $modelosRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$modelo->getId(), $request->request->get('_token'))) {
            $modelosRepository->remove($modelo);
        }

        return $this->redirectToRoute('app_modelos_index', [], Response::HTTP_SEE_OTHER);
    }
}
