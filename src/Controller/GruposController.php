<?php

namespace App\Controller;

use App\Entity\Grupos;
use App\Form\GruposType;
use App\Repository\GruposRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/grupos")
 */
class GruposController extends AbstractController
{
    /** CAMBIA DE ESTADO AL REGISTRO
     * @Route("/cambia-estado-registro/{id}/{estado}", name="app_grupos_cambia_estado", methods={"GET"})
     */
    public function cambiaEstado($id, $estado, GruposRepository $gruposRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $registro = $em->getRepository(Grupos::class)->find($id);
        $registro->setGruEstado($estado);
        $em->persist($registro);
        $em->flush();
        return $this->redirectToRoute('app_grupos_index');
    }

    /**
     * @Route("/", name="app_grupos_index", methods={"GET"})
     */
    public function index(GruposRepository $gruposRepository): Response
    {
        return $this->render('grupos/index.html.twig', [
            'grupos' => $gruposRepository->getAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_grupos_new", methods={"GET", "POST"})
     */
    public function new(Request $request, GruposRepository $gruposRepository): Response
    {
        $grupo = new Grupos();
        $form = $this->createForm(GruposType::class, $grupo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $gruposRepository->add($grupo);
            return $this->redirectToRoute('app_grupos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('grupos/new.html.twig', [
            'grupo' => $grupo,
            'grupos' => $gruposRepository->getAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_grupos_show", methods={"GET"})
     */
    public function show(Grupos $grupo): Response
    {
        return $this->render('grupos/show.html.twig', [
            'grupo' => $grupo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_grupos_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Grupos $grupo, GruposRepository $gruposRepository): Response
    {
        $form = $this->createForm(GruposType::class, $grupo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $gruposRepository->add($grupo);
            return $this->redirectToRoute('app_grupos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('grupos/edit.html.twig', [
            'grupo' => $grupo,
            'grupos' => $gruposRepository->getEdit($grupo->getId()),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_grupos_delete", methods={"POST"})
     */
    public function delete(Request $request, Grupos $grupo, GruposRepository $gruposRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$grupo->getId(), $request->request->get('_token'))) {
            $gruposRepository->remove($grupo);
        }

        return $this->redirectToRoute('app_grupos_index', [], Response::HTTP_SEE_OTHER);
    }
}
