<?php

namespace App\Controller;

use App\Entity\Lotes;
use App\Entity\Racks;
use App\Form\LotesType;
use App\Repository\LotesRepository;
use App\Repository\RacksRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lotes")
 */
class LotesController extends AbstractController
{

    /** Trae lotes por bateria ID
     * @Route("/get-by-bateria", name="app_lotes_get_by_bateria", methods={"POST"})
     */
    public function getByBateria(LotesRepository $lotesRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $lotes = $lotesRepository->getByBateria($_POST['batId']);
        if(!$lotes){
            $lotes = $lotesRepository->getByGenerico();
        }
        return new Response(json_encode($lotes, true),200, ['Content-Type' => '"application/json"']);
    }

    /** CAMBIA DE ESTADO AL REGISTRO
     * @Route("/cambia-estado-registro/{id}/{estado}", name="app_lotes_cambia_estado", methods={"GET"})
     */
    public function cambiaEstado($id, $estado, LotesRepository $lotesRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $registro = $em->getRepository(Lotes::class)->find($id);
        $registro->setLotEstado($estado);
        $em->persist($registro);
        $em->flush();
        return $this->redirectToRoute('app_lotes_index');
    }

    /**
     * @Route("/", name="app_lotes_index", methods={"GET"})
     */
    public function index(LotesRepository $lotesRepository): Response
    {
        return $this->render('lotes/index.html.twig', [
            'lotes' => $lotesRepository->getAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_lotes_new", methods={"GET", "POST"})
     */
    public function new(Request $request, LotesRepository $lotesRepository): Response
    {
        $lote = new Lotes();
        $form = $this->createForm(LotesType::class, $lote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lotesRepository->add($lote);
            return $this->redirectToRoute('app_lotes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('lotes/new.html.twig', [
            'lote' => $lote,
            'lotes' => $lotesRepository->getAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_lotes_show", methods={"GET"})
     */
    public function show(Lotes $lote): Response
    {
        return $this->render('lotes/show.html.twig', [
            'lote' => $lote,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_lotes_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Lotes $lote, LotesRepository $lotesRepository): Response
    {
        $form = $this->createForm(LotesType::class, $lote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lotesRepository->add($lote);
            return $this->redirectToRoute('app_lotes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('lotes/edit.html.twig', [
            'lote' => $lote,
            'lotes' => $lotesRepository->getEdit($lote->getId()),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_lotes_delete", methods={"POST"})
     */
    public function delete(Request $request, Lotes $lote, LotesRepository $lotesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$lote->getId(), $request->request->get('_token'))) {
            $lotesRepository->remove($lote);
        }

        return $this->redirectToRoute('app_lotes_index', [], Response::HTTP_SEE_OTHER);
    }
}
