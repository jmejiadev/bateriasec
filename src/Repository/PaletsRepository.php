<?php

namespace App\Repository;

use App\Entity\Palets;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Palets|null find($id, $lockMode = null, $lockVersion = null)
 * @method Palets|null findOneBy(array $criteria, array $orderBy = null)
 * @method Palets[]    findAll()
 * @method Palets[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaletsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Palets::class);
    }

    public function getBateriasNoRegistradasLote(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT l
            FROM App:CodigosQr qr, App:Lotes l
            WHERE qr.num_lote = l.id
            AND qr.codEstado = 'GENERADO'
            GROUP BY l.id
            ORDER BY l.id");
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }
    public function getDetalle($palId): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT p.id, p.palDescripcion, p.palFecCreacion, COUNT(pb.id) as cantidad, MIN(pb.codigo_gar) as min, MAX(pb.codigo_gar) as max
            FROM App:PaletsBaterias pb
            JOIN pb.palets p
            GROUP BY p.id
            ORDER BY p.palEstado');
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getBateriasByPallet($palId): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT cqr
            FROM App:PaletsBaterias pb, App:Palets p, App:CodigosQr cqr
            WHERE pb.palets = p.id
            AND cqr.codigo_gar = pb.codigo_gar
            AND p.id = :palId
            ORDER BY cqr.codigo_gar');
        $consulta->setParameter('palId', $palId);
        return $consulta->getArrayResult();
    }
    public function getAll(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT p.id, p.palDescripcion, p.palEstado, p.palFecCreacion, COUNT(pb.id) as cantidad
            FROM App:Palets p
            JOIN p.paletsbaterias pb
            GROUP BY p.id
            ORDER BY p.palFecCreacion DESC');
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getByEstado($estados): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT p.id, p.palDescripcion, p.palEstado, p.palFecCreacion, COUNT(pb.id) as cantidad
            FROM App:Palets p
            JOIN p.paletsbaterias pb
            WHERE p.palEstado in (:estados)
            GROUP BY p.id
            ORDER BY p.palFecCreacion DESC');
        $consulta->setParameter('estados', $estados);
        return $consulta->getArrayResult();
    }
}
