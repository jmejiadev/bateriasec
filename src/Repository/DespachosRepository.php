<?php

namespace App\Repository;

use App\Entity\Despachos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Despachos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Despachos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Despachos[]    findAll()
 * @method Despachos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DespachosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Despachos::class);
    }

    public function getDespachosByCodDetFac($coddetfac): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT d, dis
            FROM App:Despachos d
            JOIN d.distribucion dis
            WHERE d.detalleFac = :coddetfac 
            ");
        $consulta->setParameter('coddetfac', $coddetfac);
        return $consulta->getArrayResult();
    }

    public function getBateriasByDespacho($docDistribuidor, $bandera): array
    {
        if($bandera['query_configuracion'] == 1) {
            if ($docDistribuidor) {
                $condicion = " AND f.customerNumber LIKE '%" . $docDistribuidor . "%'";
            } else {
                $condicion = "";
            }
            $em = $this->getEntityManager();
            $consulta = $em->createQuery("SELECT d.number, cqr.nom_prod_neural, cqr.codigo_gar, cqr.fecha_gar, cqr.pathQr, cqr.codigo_grupo_garantia,
                cqr.url,  IfElse(d.actGarantia = 0, 'Activar Garantia','Garantia Activa') as actGarantia
                FROM App:CodigosQr cqr, App:Despachos d
                JOIN d.detalleFac df
                JOIN df.facturas f
                WHERE cqr.codigo_gar = d.codGarantia " .
                $condicion
            );
        }else{
            $em = $this->getEntityManager();
            $consulta = $em->createQuery("SELECT cqr.nom_prod_neural, cqr.codigo_gar, cqr.fecha_gar, cqr.pathQr, cqr.codigo_grupo_garantia,
                cqr.url,  IfElse(cqr.actGarantia = 0, 'Activar Garantia','Garantia Activa') as actGarantia
                FROM App:CodigosQr cqr, App:Garantias g
                WHERE cqr.codigo_gar = g.garCodigo
                AND g.garCodQr = 1
                AND cqr.codEstado != 'MOVIDA A PNC' "
            );
        }
        //$consulta->setParameter('docDistribuidor', $docDistribuidor);
        return $consulta->getArrayResult();
    }

}
