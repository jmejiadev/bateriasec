<?php

namespace App\Repository;

use App\Entity\Seccion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Seccion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Seccion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Seccion[]    findAll()
 * @method Seccion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeccionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Seccion::class);
    }

    public function getAll(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT s.id, s.secCodigo, s.secNombre, s.secEstado, r.id as racId, r.racNumero, b.id as bodId, b.bodNombre 
            FROM App:Seccion s
            LEFT JOIN s.racks r
            LEFT JOIN r.bodegas b
            ORDER BY s.id');
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getModelosBySecciones(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT r.racNumero, r.racDescripcion, d.modelo, COUNT(d.id) as cantidad
            FROM App:Distribucion d
            JOIN d.secciones s
            JOIN s.racks r
            JOIN r.bodegas b
            WHERE d.disEstado = 'INGRESADO' 
            GROUP BY r.racNumero, d.modelo
            ORDER BY r.id");
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getDisponibilidad(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT r.racNumero, r.racDescripcion, COUNT(s.id) as cantidad
            FROM App:Racks r
            LEFT JOIN r.secciones s
            LEFT JOIN s.distribucion d 
            JOIN r.bodegas b 
            WHERE d.id IS NULL
            GROUP BY r.racNumero
            ORDER BY r.id");
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getSeccionesByRack($numRack): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT s.id, s.secCodigo, s.secNombre, s.secEstado, r.racNumero, r.racNumFilas, r.racNumColumnas, r.racDescripcion,
            r.racEstado, b.bodNombre, b.bodDescripcion, b.bodEstado,
            (SELECT CONCAT('[', group_concat(DISTINCT JSON_OBJECT('modelo', d.modelo, 'cantidad', 
                (SELECT COUNT(d2.modelo)  
                    FROM App:Distribucion d2 
                    WHERE d2.secciones = s.id
                    AND d2.modelo = d.modelo
                    AND d2.disEstado = 'INGRESADO')
                )) , ']')  
                FROM App:Distribucion d 
                WHERE d.secciones = s.id 
                AND d.disEstado = 'INGRESADO'
                ) as detalle
            FROM App:Seccion s
            LEFT JOIN s.racks r
            LEFT JOIN r.bodegas b
            WHERE r.racNumero = :numRack
            ORDER BY s.id");
        $consulta->setParameter('numRack', $numRack);
//        dd($consulta);
        return $consulta->getArrayResult();
    }

    // /**
    //  * @return Seccion[] Returns an array of Seccion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Seccion
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
