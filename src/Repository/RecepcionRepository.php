<?php

namespace App\Repository;

use App\Entity\Recepcion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Recepcion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recepcion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recepcion[]    findAll()
 * @method Recepcion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecepcionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recepcion::class);
    }

    public function getRegistrosByEstado($estado='DESPACHADO'): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT r.codigo_gar
            FROM App:Recepcion r
            LEFT JOIN r.distribucion d
            WHERE (d.disEstado != 'DESPACHADO' OR d.disEstado is null)
            ORDER BY r.id");
       // $consulta->setParameter('estado', '%"' . $estado . '"%');
        return array_column($consulta->getScalarResult(), "codigo_gar");
    }

    public function getRegistrosByPalet($id_palet): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT r
            FROM App:Recepcion r
            WHERE r.palets = :id_palet
            ORDER BY r.id");
        $consulta->setParameter('id_palet', $id_palet);
        return $consulta->getArrayResult();
    }


}
