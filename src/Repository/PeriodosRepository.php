<?php

namespace App\Repository;

use App\Entity\Periodos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Periodos>
 *
 * @method Periodos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Periodos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Periodos[]    findAll()
 * @method Periodos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeriodosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Periodos::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Periodos $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Periodos $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getPeriodoNow(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT p
            FROM App:Periodos p 
            WHERE p.perFecInicio <= '" . date('Y-m-d') . "' 
            AND p.perFecFin >= '" . date('Y-m-d') . "'"
            );
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getAll(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT p
            FROM App:Periodos p
            ORDER BY p.perAnio DESC');
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getEdit($id): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT p
            FROM App:Periodos p
            WHERE p.id != :id
            ORDER BY p.id');
        $consulta->setParameter('id', $id);
        return $consulta->getArrayResult();
    }




    // /**
    //  * @return Periodos[] Returns an array of Periodos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Periodos
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
