<?php

namespace App\Repository;

use App\Entity\PaletsBaterias;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PaletsBaterias|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaletsBaterias|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaletsBaterias[]    findAll()
 * @method PaletsBaterias[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaletsBateriasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaletsBaterias::class);
    }

    public function getBateriasByPalet($palet = null): array
    {
        $em = $this->getEntityManager();
            $consulta = $em->createQuery('SELECT pb.codigo_gar
                FROM App:PaletsBaterias pb
                WHERE pb.palets = :palet
                ORDER BY pb.codigo_gar');
            $consulta->setParameter('palet', $palet);

        return array_column($consulta->getScalarResult(), "codigo_gar");
        //return $consulta->getArrayResult();
    }

    public function getBateriasByLote($lote = null): array
    {
        $em = $this->getEntityManager();
        if($lote) {
            $consulta = $em->createQuery("SELECT pb.codigo_gar
                FROM App:PaletsBaterias pb
                JOIN pb.palets p
                WHERE pb.id_lote =:lote
                AND p.palEstado != 'DISTRIBUIDO'
                ORDER BY pb.id");
            $consulta->setParameter('lote', $lote);
        }else{
            $consulta = $em->createQuery("SELECT pb.codigo_gar
                FROM App:PaletsBaterias pb
                JOIN pb.palets p
                WHERE p.palEstado != 'DISTRIBUIDO'
                ORDER BY pb.id");
        }

        return array_column($consulta->getScalarResult(), "codigo_gar");
        //return $consulta->getArrayResult();
    }



    // /**
    //  * @return PaletsBaterias[] Returns an array of PaletsBaterias objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaletsBaterias
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
