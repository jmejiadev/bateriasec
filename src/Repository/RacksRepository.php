<?php

namespace App\Repository;

use App\Entity\Racks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Racks|null find($id, $lockMode = null, $lockVersion = null)
 * @method Racks|null findOneBy(array $criteria, array $orderBy = null)
 * @method Racks[]    findAll()
 * @method Racks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RacksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Racks::class);
    }

    public function getAll(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT r.id, r.racNumero, r.racNumFilas, r.racNumColumnas, r.racDescripcion, r.racEstado, 
            b.id as bodId, b.bodNombre, b.bodDescripcion
            FROM App:Racks r
            LEFT JOIN r.bodegas b
            ORDER BY r.id');
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getEdit($id): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT r
            FROM App:Racks r
            WHERE r.id != :id
            ORDER BY r.id');
        $consulta->setParameter('id', $id);
        return $consulta->getArrayResult();
    }

    // /**
    //  * @return Racks[] Returns an array of Racks objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Racks
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
