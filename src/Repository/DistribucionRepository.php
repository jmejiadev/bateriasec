<?php

namespace App\Repository;

use App\Entity\Distribucion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Distribucion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Distribucion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Distribucion[]    findAll()
 * @method Distribucion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DistribucionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Distribucion::class);
    }

    public function getBateriasDistribuidas(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT q.id, q.num_lote, q.codigo_gar, q.modelo, q.fecha_gar, q.fecCreacion, d.id as disId
            FROM App:Distribucion d, App:CodigosQr q
            WHERE d.codigo_gar = q.codigo_gar
            AND d.disEstado = 'INGRESADO'
            ORDER BY d.codigo_gar");
        // $consulta->setParameter('estado', '%"' . $estado . '"%');
        return $consulta->getArrayResult();
    }

    public function getBateriasNoDistribuidas(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT q.id, q.num_lote, q.codigo_gar, q.modelo, q.fecha_gar, q.fecCreacion, r.id as recId
            FROM App:Recepcion r, App:CodigosQr q
            WHERE r.codigo_gar = q.codigo_gar
            AND r.palets is null
            AND q.codEstado = 'RECIBIDO'
            ORDER BY r.id");
        // $consulta->setParameter('estado', '%"' . $estado . '"%');
        return $consulta->getArrayResult();
    }

    public function getDistribucionBySeccion($secId): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT cqr
            FROM App:Distribucion d, App:CodigosQr cqr
            WHERE d.codigo_gar = cqr.codigo_gar
            AND d.secciones = :secId
            ORDER BY cqr.id");
        $consulta->setParameter('secId', $secId);
        return $consulta->getArrayResult();
    }

    public function getBateriasByModelo($modelo): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT d.id, d.disNumLote, d.codigo_gar, d.modelo, r.racDescripcion, s.secNombre, p.palDescripcion, p.id as palId
            FROM App:CodigosQr cqr, App:Distribucion d
            LEFT JOIN d.palets p
            JOIN d.secciones s
            JOIN s.racks r
            WHERE cqr.codigo_gar = d.codigo_gar
            AND d.disEstado = 'INGRESADO'
            AND cqr.nom_prod_neural = :modelo
            ORDER BY d.id DESC");
        $consulta->setParameter('modelo', $modelo);
        return $consulta->getArrayResult();
    }

    public function getUbicacionesByModelo($modelo): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT r.racDescripcion, s.secNombre, p.id as palId , count(d.id) as cantidad
            FROM App:CodigosQr cqr, App:Distribucion d
            LEFT JOIN d.palets p
            JOIN d.secciones s
            JOIN s.racks r
            WHERE cqr.codigo_gar = d.codigo_gar
            AND d.disEstado = 'INGRESADO'
            AND cqr.nom_prod_neural = :modelo
            GROUP BY r.racDescripcion, s.secNombre, p.id
            ORDER BY r.id DESC");
        $consulta->setParameter('modelo', $modelo);
        return $consulta->getArrayResult();
    }


}
