<?php

namespace App\Repository;

use App\Entity\Baterias;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Baterias>
 *
 * @method Baterias|null find($id, $lockMode = null, $lockVersion = null)
 * @method Baterias|null findOneBy(array $criteria, array $orderBy = null)
 * @method Baterias[]    findAll()
 * @method Baterias[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BateriasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Baterias::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Baterias $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Baterias $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getByEstado($estado): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT b
            FROM App:Baterias b
            WHERE b.batEstado = :estado
            ORDER BY b.batNomProdNeural');
        $consulta->setParameter('estado', $estado);
        return $consulta->getArrayResult();
    }

    public function getAll(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT b
            FROM App:Baterias b
            ORDER BY b.batEstado DESC');
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getEdit($id): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT b
            FROM App:Baterias b
            WHERE b.id != :id
            ORDER BY b.id');
        $consulta->setParameter('id', $id);
        return $consulta->getArrayResult();
    }

    // /**
    //  * @return Baterias[] Returns an array of Baterias objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Baterias
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
