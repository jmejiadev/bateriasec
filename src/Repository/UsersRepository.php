<?php
/**
 * Created by PhpStorm.
 * User: jmejia
 * Date: 2019-03-12
 * Time: 13:36
 */

namespace App\Repository;

use App\Entity\Users;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use http\Env;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UsersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Users::class);
    }

    public function getAllUser(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT u
            FROM App:Users u
            ORDER BY u.id');
//      $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }


    public function getEditUser($id): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT u
            FROM App:Users u
            Where u.id != :id
            ORDER BY u.id');
        $consulta->setParameter('id', $id);
        return $consulta->getArrayResult();
    }


    public function getUbicacionByProducto($modelo): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT CONCAT(r.racDescripcion, ' - ', s.secNombre, ' - Cantidad: ', count(d.modelo)) as ubicacion
            FROM App:Distribucion d 
            JOIN d.secciones s
            JOIN s.racks r
            WHERE d.modelo = :modelo
            GROUP BY r.racDescripcion, s.secNombre, d.modelo");
        $consulta->setParameter('modelo', $modelo);
        return $consulta->getArrayResult();
    }

    public function getDetallesFactura($codfac): array
    {

        $server = $_ENV['server'];
        $dbName = $_ENV['databaseName'];
        $uid = $_ENV['sql_user'];
        $pwd = $_ENV['sql_password'];

        $options = array("Database" => $dbName, "UID" => $uid, "PWD" => $pwd, "CharacterSet" => "UTF-8");
        $conn = sqlsrv_connect($server, $options);
        if ($conn === false) {
            $msg = "Error, no existe conexión a la BD SQL SERVER";
            die(dd($msg, sqlsrv_errors()));
        }

        $query = "Select df.coddetfac, df.codfac, df.codpro, p.nombrepro, p.despro, p.clasepro, df.cantdetfac, df.descprodetfac, 'NO ASIGNADO' as detFacEstado,
                    p.fampro, 'No Disponible' as ubicacion
                    from detallefac df, producto p 
                    where df.codpro = p.codpro 
                    and p.fampro = 'BATE'
                    and df.codfac = " . $codfac . "
                    Order By df.coddetfac DESC";
        $stmt = sqlsrv_query($conn, $query);

        $result = [];
        while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
            $result[] = $row;
        }
        //dd($result);
        sqlsrv_free_stmt($stmt);
        sqlsrv_close($conn);
        return $result;
    }

    public function getFacturaErp($codFactura): array
    {
        $server = $_ENV['server'];
        $dbName = $_ENV['databaseName'];
        $uid = $_ENV['sql_user'];
        $pwd = $_ENV['sql_password'];

        $options = array("Database" => $dbName, "UID" => $uid, "PWD" => $pwd, "CharacterSet" => "UTF-8");
        $conn = sqlsrv_connect($server, $options);
        if ($conn === false) {
            $msg = "Error, no existe conexión a la BD SQL SERVER";
            die(dd($msg, sqlsrv_errors()));
        }

        $query="Select f.codfac, f.codcli, c.nombrecli, c.telefono1cli, c.mailcli, f.comentfac, f.fechafac, f.baseimpfac, f.statusfac 
                    from factura f, cliente c
                    where c.codcli = f.codcli
                    and f.codfac = " . $codFactura;

        $stmt = sqlsrv_query($conn, $query);

        //$result = [];
        while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
            $result[] = $row;
        }
        //dd($result);
        sqlsrv_free_stmt($stmt);
        sqlsrv_close($conn);
        return $result;
    }


    public function getBateriasAll($fecDesde, $fecHasta, $estado): array
    {
        //dd($fecDesde);
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT b.id as id_bateria, l.id as id_lote, b.batCodCrm as codCrm, b.batCodGrupo as codigoGrupo, b.batCodNeural as codNeural,
                    g.garFecha as fecha_gar, b.batNomProducto as nombProducto, b.batNomProdNeural as nombProdNeural, b.batModelo as modelo, b.batCodLp as codigoLP,
                    b.batCodInicio as codigo_inicio, b.batTipo as tipo_bater, b.batEstado as estado_bater, g.id as id_garantia, g.garCodigo as codigo_gar,
                    g.garMarTime as marca_time_gar, g.garCodSimple as codigo_simple, 'VER QR' as imgQr
                    FROM App:Baterias b, App:Garantias g, App:lotes l 
                    WHERE g.barId = b.id AND g.lotId = l.id
                    AND g.garFecha BETWEEN '". $fecDesde . " 00:00:00' AND '" . $fecHasta . " 23:59:59'
                    AND g.garCodQr = :estado
                    ORDER BY g.garCodigo");
        $consulta->setParameter('estado', $estado);
        return $consulta->getArrayResult();
    }

    public function getBateriasGeneradas($codigos): array
    {
        //dd(implode("','", $codigos));
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT b.id as id_bateria, l.id as id_lote, b.batCodCrm as codCrm, b.batCodGrupo as codigoGrupo, b.batCodNeural as codNeural,
                    g.garFecha as fecha_gar, b.batNomProducto as nombProducto, b.batNomProdNeural as nombProdNeural, b.batModelo as modelo, b.batCodLp as codigoLP,
                    b.batCodInicio as codigo_inicio, b.batTipo as tipo_bater, b.batEstado as estado_bater, g.id as id_garantia, g.garCodigo as codigo_gar,
                    g.garMarTime as marca_time_gar, g.garCodSimple as codigo_simple, 'SIN QR' as imgQr
                    FROM App:Baterias b, App:Garantias g, App:lotes l 
                    WHERE g.barId = b.id AND g.lotId = l.id
                    AND g.garCodigo IN ('" . implode("','", $codigos) . "') 
                    ORDER BY g.garCodigo");
        return $consulta->getArrayResult();
    }

    public function getBateriasImpresas($codSi, $fecDesde, $fecHasta): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT qr.id, qr.num_lote, qr.codigo_gar, qr.modelo, qr.fecha_gar, 'VER QR' as imgQr, qr.nom_prod_neural,
            qr.tipo_bater , qr.codigo_grupo
            FROM App:CodigosQr qr
            WHERE qr.fecha_gar BETWEEN '". $fecDesde . " 00:00:00' AND '" . $fecHasta . " 23:59:59' 
            AND qr.staImpresion = 1 
            ORDER BY qr.codigo_gar DESC");
        return $consulta->getArrayResult();
    }

    public function getCodigosAll(): array
    {
        $server = $_ENV['server_mysql'];
        $dbName = $_ENV['databaseName_mysql'];
        $uid = $_ENV['sql_user_mysql'];
        $pwd = $_ENV['sql_password_mysql'];
        $dsn = "mysql:host=$server;dbname=$dbName;charset=UTF8";

        $options = [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION];
        try {
            $conn = new \PDO($dsn, $uid, $pwd, $options);
        } catch (PDOException $e) {
            $msg = "Error, no existe conexión a la BD MYSQL SERVER, BD_NAME:". $dbName;
            die(dd($msg, $e));
        }

        if($conn){
            $consulta = "SELECT g.codigo_gar FROM bateria b, garantia g, lote l WHERE g.id_bateria = b.id_bateria AND g.id_lote = l.id_lote 
                        AND  b.estado_bater = 1 ORDER BY l.id_lote, g.codigo_gar";
            $stmt = $conn->query($consulta);
            $resultado = $stmt->fetchAll(\PDO::FETCH_COLUMN);
            return $resultado;
        }else{
            $msg = "Error, no existe conexión a la BD MYSQL SERVER, BD_NAME:". $dbName;
            die(dd($msg, $conn->errorInfo()));
        }
    }

    public function getConGarantias(): array
    {
        $server = $_ENV['server_mysql'];
        $dbName = $_ENV['databaseName_mysql'];
        $uid = $_ENV['sql_user_mysql'];
        $pwd = $_ENV['sql_password_mysql'];
        $dsn = "mysql:host=$server;dbname=$dbName;charset=UTF8";

        $options = [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION];
        try {
            $conn = new \PDO($dsn, $uid, $pwd, $options);
        } catch (PDOException $e) {
            $msg = "Error, no existe conexión a la BD MYSQL SERVER, BD_NAME:". $dbName;
            die(dd($msg, $e));
        }

        if($conn){
            //$friendsArray = array("zac1987", "peter", "micellelimmeizheng1152013142");
            //$codigos = implode("','",$codigos);

            $consulta = "SELECT query_configuracion 
                        FROM tb_configuracion c 
                        WHERE c.id_configuracion = 1
                        ";
            $stmt = $conn->query($consulta);
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            $msg = "Error, no existe conexión a la BD MYSQL SERVER, BD_NAME:". $dbName;
            die(dd($msg, $conn->errorInfo()));
        }
    }
}
