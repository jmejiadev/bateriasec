<?php

namespace App\Repository;

use App\Entity\Lotes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Lotes>
 *
 * @method Lotes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lotes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lotes[]    findAll()
 * @method Lotes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LotesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lotes::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Lotes $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Lotes $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getAll(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT l
            FROM App:Lotes l
            ORDER BY l.lotEstado DESC');
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getByBateria($batId): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT l
             FROM App:Lotes l, App:Baterias b
             WHERE l.lotEstado = 1
             AND l.lotNumPlacas = b.batNumPlacas
             AND b.id = :batId
             ORDER BY l.lotNumPlacas');
        $consulta->setParameter('batId', $batId);
        return $consulta->getArrayResult();
    }

    public function getByGenerico(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT l
             FROM App:Lotes l
             WHERE l.lotCodigo = 'GENERICO'
             ");
        //$consulta->setParameter('batId', $batId);
        return $consulta->getArrayResult();
    }

    public function getEdit($id): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT l
            FROM App:Lotes l
            WHERE l.id != :id
            ORDER BY l.id');
        $consulta->setParameter('id', $id);
        return $consulta->getArrayResult();
    }

    // /**
    //  * @return Lotes[] Returns an array of Lotes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lotes
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
