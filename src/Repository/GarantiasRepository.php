<?php

namespace App\Repository;

use App\Entity\Garantias;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Garantias>
 *
 * @method Garantias|null find($id, $lockMode = null, $lockVersion = null)
 * @method Garantias|null findOneBy(array $criteria, array $orderBy = null)
 * @method Garantias[]    findAll()
 * @method Garantias[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GarantiasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Garantias::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Garantias $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Garantias $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getAll(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT g.id, g.garCodigo, g.gruCodigo, g.garFecha, l.lotCodigo, p.perAnio, p.perMes, b.batModelo, b.batCodInicio,
            b.batCodNeural
            FROM App:Garantias g, App:Lotes l, App:Periodos p, App:Baterias b
            WHERE g.lotId = l.id
            AND g.perId = p.id
            AND g.barId = b.id
            ORDER BY g.garFecha DESC');
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    public function getExiste($codGarantia): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT g
            FROM App:Garantias g
            WHERE g.garCodigo = :codGarantia
            ');
        $consulta->setParameter('codGarantia', $codGarantia);
        return $consulta->getArrayResult();
    }

    public function getByIds($garantias): array
        {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT g.id, g.garCodigo, g.gruCodigo, g.garFecha, l.lotCodigo, p.perAnio, p.perMes, b.batModelo, b.batCodInicio,
            b.batCodNeural
            FROM App:Garantias g, App:Lotes l, App:Periodos p, App:Baterias b
            WHERE g.lotId = l.id
            AND g.perId = p.id
            AND g.barId = b.id
            AND g.id in (:garantias)
            ');
        $consulta->setParameter('garantias', $garantias);
        return $consulta->getArrayResult();
    }


    // /**
    //  * @return Garantias[] Returns an array of Garantias objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Garantias
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
