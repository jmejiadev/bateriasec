<?php

namespace App\Repository;

use App\Entity\Facturas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use http\Env;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Facturas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Facturas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Facturas[]    findAll()
 * @method Facturas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacturasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Facturas::class);
    }

    public function getByFechas($fecDesde, $fecHasta):array
    {
        $em = $this->getEntityManager();

        $consulta = $em->createQuery("SELECT f, d
            FROM App:Facturas f
            JOIN f.detalleFac d
            WHERE f.facFecha BETWEEN '" . $fecDesde . "' and '" . $fecHasta . "'
            ORDER BY f.facEstado, f.facFecha");
        //$consulta->setParameter('secId', $secId);
        return $consulta->getArrayResult();
    }

    public function getByFechasApi($fecDesde, $fecHasta, $buscar):array
    {
        $em = $this->getEntityManager();
        if($buscar != "none"){
            $condicion = " AND f.numFac Like '%" . $buscar . "%'";
        }else{
            $condicion = "";
        }
        $consulta = $em->createQuery("SELECT f.id, f.numFac, f.customerNumber, f.customerName, f.totalAmountIncludingTax, f.facEstado
            FROM App:Facturas f
            WHERE f.facFecha BETWEEN '" . $fecDesde . "' and '" . $fecHasta . "'
            ". $condicion."
            ORDER BY f.facEstado, f.numFac");

        //$consulta->setParameter('secId', $secId);
        return $consulta->getArrayResult();
    }



}
