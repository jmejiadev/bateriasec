<?php

namespace App\Repository;

use App\Entity\DetalleFac;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DetalleFac>
 *
 * @method DetalleFac|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetalleFac|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetalleFac[]    findAll()
 * @method DetalleFac[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetalleFacRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetalleFac::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(DetalleFac $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(DetalleFac $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }


    public function getByFactura($facId): array
    {
        $em = $this->getEntityManager();

        $consulta = $em->createQuery("SELECT d, f
            FROM App:DetalleFac d
            JOIN d.facturas f
            WHERE f.id = :facId
            ORDER BY d.description");
        $consulta->setParameter('facId', $facId);
        return $consulta->getArrayResult();
    }

    public function getByEstado($facId): array
    {
        $em = $this->getEntityManager();

        $consulta = $em->createQuery("SELECT d, f
            FROM App:DetalleFac d
            JOIN d.facturas f
            WHERE f.id = :facId
            AND (d.regEstado = 'REGISTRADA' OR d.regEstado = 'PARCIAL')
            ORDER BY d.description");
        $consulta->setParameter('facId', $facId);
        return $consulta->getArrayResult();
    }

    // /**
    //  * @return DetalleFac[] Returns an array of DetalleFac objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DetalleFac
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
