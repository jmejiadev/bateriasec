<?php

namespace App\Repository;

use App\Entity\Modelos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Modelos>
 *
 * @method Modelos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Modelos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Modelos[]    findAll()
 * @method Modelos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModelosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Modelos::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Modelos $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Modelos $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getAll(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT m
            FROM App:Modelos m
            ORDER BY m.id');
//    $consulta->setParameter('roles', '%"' . $role . '"%');
        return $consulta->getArrayResult();
    }

    // /**
    //  * @return Modelos[] Returns an array of Modelos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Modelos
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
