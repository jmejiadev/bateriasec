<?php

namespace App\Repository;

use App\Entity\Bodegas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Bodegas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bodegas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bodegas[]    findAll()
 * @method Bodegas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BodegasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bodegas::class);
    }

    public function getAll(): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT b
            FROM App:Bodegas b
            ORDER BY b.id');
        //$consulta->setParameter('numRack', '%"' . $numRack . '"%');
        return $consulta->getArrayResult();
    }

    // /**
    //  * @return Bodegas[] Returns an array of Bodegas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bodegas
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
