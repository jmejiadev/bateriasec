<?php

namespace App\Repository;

use App\Entity\CodigosQr;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @method CodigosQr|null find($id, $lockMode = null, $lockVersion = null)
 * @method CodigosQr|null findOneBy(array $criteria, array $orderBy = null)
 * @method CodigosQr[]    findAll()
 * @method CodigosQr[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CodigosQrRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CodigosQr::class);
    }

    public function getCodigosByStaImpresion($estado, $fecDesde, $fecHasta): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT c.codigo_gar
            FROM App:CodigosQr c
            WHERE c.staImpresion = :estado
            AND c.fecha_gar BETWEEN '". $fecDesde . " 00:00:00' AND '" . $fecHasta . " 23:59:59'  
            AND c.staEstado = true
            ");
        $consulta->setParameter('estado', $estado);
        return array_column($consulta->getScalarResult(), "codigo_gar");
        //return $consulta->getArrayResult();
    }

    public function getModelosByLote($estado, $lote): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT c.modelo, c.nom_prod_neural
            FROM App:CodigosQr c
            WHERE c.codEstado IN (:estado)
            AND c.num_lote = :lote
            GROUP BY c.modelo
            ");
        $consulta->setParameter('estado', $estado);
        $consulta->setParameter('lote', $lote);
        return $consulta->getArrayResult();
    }

    public function getCodQrGenerados($codGarantia): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT c.id, c.codigo_gar, c.codigo_grupo_garantia
            FROM App:CodigosQr c, App:Garantias g
            WHERE g.garCodigo = c.codigo_gar
            AND c.codEstado = 'GENERADO'
            AND g.garCodQr = 1
            AND (c.codigo_gar = :codGarantia OR c.codigo_grupo_garantia LIKE '%" . $codGarantia . "%')
            ORDER BY c.codigo_gar");
        $consulta->setParameter('codGarantia', $codGarantia);
        return $consulta->getArrayResult();
    }

    public function getCodigosByEstado($estado, $lote = null, $modelo = null): array
    {
        $em = $this->getEntityManager();
        $condicion="";
        if($lote){
            $condicion = ' AND c.num_lote = ' . $lote;
        }
        if($modelo){
            $condicion .= " AND c.nom_prod_neural = '" . $modelo . "' ";
        }
        $consulta = $em->createQuery("SELECT c
            FROM App:CodigosQr c
            WHERE c.codEstado IN (:estado)
            " . $condicion .
            " ORDER BY c.codigo_gar");
        $consulta->setParameter('estado', $estado);
        return $consulta->getArrayResult();
    }

    public function getCodigosByEstadoFecha($estado, $fecha): array
    {
        //dd($fecha);
        $em = $this->getEntityManager();
        $condicion="";
        if($fecha){
            $condicion = " AND c.fecCreacion BETWEEN '" . $fecha . " 00:00:00' AND '" . $fecha . " 23:59:59'";
        }
        //dd($condicion);
        $consulta = $em->createQuery("SELECT c
            FROM App:CodigosQr c
            WHERE c.codEstado IN (:estado)
            " . $condicion .
            " ORDER BY c.codigo_gar");
        $consulta->setParameter('estado', $estado);
        return $consulta->getArrayResult();
    }
    public function getCodigosByCodigos($codigos): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT c
            FROM App:CodigosQr c
            Where c.codigo_gar IN (:codigos)
            ');
        $consulta->setParameter('codigos', $codigos);
        //return array_column($consulta->getScalarResult(), "codigo_gar");
        return $consulta->getArrayResult();
    }

    public function validaQr($serial): array
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery("SELECT d
            FROM App:Distribucion d
            Where d.codigo_gar = :serial
            AND  d.disEstado = 'INGRESADO'
            ");
        $consulta->setParameter('serial', $serial);
        //return array_column($consulta->getScalarResult(), "codigo_gar");
        return $consulta->getArrayResult();
    }


    // /**
    //  * @return CodigosQr[] Returns an array of CodigosQr objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CodigosQr
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
