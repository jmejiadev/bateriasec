{% extends 'base.html.twig' %}

{% block title %}{% trans %}Nuevo Grupo{% endtrans %}{% endblock %}
{% block contenido %}
<main>
    <div class="container-fluid p-5">
        <div id="window" class="row justify-content-center">
            <div class="col-6 ">
                <div class="row justify-content-center mt-3">
                    <div class="col-12 text-left" style="font-size: 24px">
                        <i class="fas fa-edit text-danger"></i><span class="text-danger"> Editar Grupo</span>
                    </div>
                </div>
                <hr>
                {{ form_start(form, {'attr': {'id': 'forNew','class':"needs-validation",'novalidate':''}}) }}
                <div class="row align-items-center justify-content-center">
                    <div class="text-left col-6 mb-3">
                        <label for="gruCodigo">{% trans %}Código{% endtrans %}</label>
                        {{ form_widget(form.gruCodigo,{'id':'gruCodigo','attr':{'class':'form-control form-control-sm'}}) }}
                        <div class="valid-feedback">
                            {% trans %}válido{% endtrans %}
                        </div>
                        <div id="lgruCodigo" class="invalid-feedback">
                            {% trans %}inválido!{% endtrans %}
                        </div>
                    </div>
                    <div class="text-left col-6 mb-3">
                        <label for="gruDescripcion">{% trans %}Descripción{% endtrans %}</label>
                        {{ form_widget(form.gruDescripcion,{'id':'gruDescripcion','attr':{'class':'form-control form-control-sm'}}) }}
                        <div class="valid-feedback">
                            {% trans %}válido{% endtrans %}
                        </div>
                        <div id="lgruDescripcion" class="invalid-feedback">
                            {% trans %}inválido!{% endtrans %}
                        </div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center">
                    <div class="text-left col-6 mb-3">
                        <label for="gruTipBateria1">{% trans %}Tipo Bateria 1{% endtrans %}</label>
                        {{ form_widget(form.gruTipBateria1,{'id':'gruTipBateria1','attr':{'class':'form-control form-control-sm'}}) }}
                        <div class="valid-feedback">
                            {% trans %}válido{% endtrans %}
                        </div>
                        <div id="lgruTipBateria1" class="invalid-feedback">
                            {% trans %}inválido!{% endtrans %}
                        </div>
                    </div>
                    <div class="text-left col-6 mb-3">
                        <label for="gruTipBateria2">{% trans %}Tipo Bateria 2{% endtrans %}</label>
                        {{ form_widget(form.gruTipBateria2,{'id':'gruTipBateria2','attr':{'class':'form-control form-control-sm'}}) }}
                        <div class="valid-feedback">
                            {% trans %}válido{% endtrans %}
                        </div>
                        <div id="lgruTipBateria2" class="invalid-feedback">
                            {% trans %}inválido!{% endtrans %}
                        </div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center">
                    <div class="text-left col-6 mb-3">
                        <label for="gruTipBateria3">{% trans %}Tipo Bateria 3{% endtrans %}</label>
                        {{ form_widget(form.gruTipBateria3,{'id':'gruTipBateria3','attr':{'class':'form-control form-control-sm'}}) }}
                        <div class="valid-feedback">
                            {% trans %}válido{% endtrans %}
                        </div>
                        <div id="lgruTipBateria3" class="invalid-feedback">
                            {% trans %}inválido!{% endtrans %}
                        </div>
                    </div>
                    <div class="text-left col-6 mb-3">
                        <label for="gruTipBateria4">{% trans %}Tipo Bateria 4{% endtrans %}</label>
                        {{ form_widget(form.gruTipBateria4,{'id':'gruTipBateria4','attr':{'class':'form-control form-control-sm'}}) }}
                        <div class="valid-feedback">
                            {% trans %}válido{% endtrans %}
                        </div>
                        <div id="lgruTipBateria4" class="invalid-feedback">
                            {% trans %}inválido!{% endtrans %}
                        </div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center">
                    <div class="text-left col-6 mb-3">
                        <label for="gruTipBateria5">{% trans %}Tipo Bateria 5{% endtrans %}</label>
                        {{ form_widget(form.gruTipBateria5,{'id':'gruTipBateria5','attr':{'class':'form-control form-control-sm'}}) }}
                        <div class="valid-feedback">
                            {% trans %}válido{% endtrans %}
                        </div>
                        <div id="gruTipBateria5" class="invalid-feedback">
                            {% trans %}inválido!{% endtrans %}
                        </div>
                    </div>
                    <div class="text-left col-6 mb-3">
                        <label for="gruSecuencia">{% trans %}Secuencia{% endtrans %}</label>
                        {{ form_widget(form.gruSecuencia,{'id':'gruSecuencia','attr':{'class':'form-control form-control-sm'}}) }}
                        <div class="valid-feedback">
                            {% trans %}válido{% endtrans %}
                        </div>
                        <div id="lgruSecuencia" class="invalid-feedback">
                            {% trans %}inválido!{% endtrans %}
                        </div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center">
                    <div class="text-left col-12 mb-3">
                        <label>{% trans %}Estado{% endtrans %}</label>
                        <div class="form-control custom-control">
                            <div class="custom-control custom-switch">
                                {{ form_widget(form.gruEstado,{'id':'gruEstado','attr':{'class':'custom-control-input'}}) }}
                                <label id="labEstado" class="custom-control-label" for="gruEstado">
                                    {% if grupo.gruEstado %}
                                        {% trans %}Registro Activo{% endtrans %}
                                    {% else %}
                                        {% trans %}Registro Inactivo{% endtrans %}
                                    {% endif %}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row align-items-center justify-content-center mt-2 mb-3">
                    <div class="col-12 smallText text-center">
                        <button type="submit" class="btn btn-danger">
                            <i class="far fa-save"></i> Guardar
                        </button>
                        <a href="{{ path('app_grupos_index') }}">
                            <button type="button" class="btn btn-warning">
                                <i class="fas fa-undo-alt"></i> Volver
                            </button>
                        </a>
                    </div>
                </div>
                {{ form_end(form)}}
            </div>
        </div>
    </div>
</main>
    <script>
        var grupos = JSON.parse('{{ grupos|json_encode|raw }}');
        $(document).ready(function(){
            $("#gruEstado").click(function(){
                if(this.checked == true){
                    $("#labEstado").addClass('text-primary');
                    $("#labEstado").empty();
                    $("#labEstado").append('{% trans %}Registro Activo{% endtrans %}');
                }else{
                    $("#labEstado").removeClass('text-primary');
                    $("#labEstado").empty();
                    $("#labEstado").append('{% trans %}Registro Inactivo{% endtrans %}');
                }
            });

            $("input:checkbox").click(function(){
                if(this.checked == true){
                    $("#l"+$(this).val()).addClass('text-primary');
                }else{
                    $("#l"+$(this).val()).removeClass('text-primary');
                }
            });

        });
        // VALIDACION
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                var forms = document.getElementsByClassName('needs-validation');
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }else{
                            grupos.forEach(function(element) {
                                if(element.gruCodigo == $("#gruCodigo").val()){
                                    $("#gruCodigo").val('');
                                    $("#lgruCodigo").empty();
                                    $("#lgruCodigo").append("{% trans %}Código ya registrado!{% endtrans %}");
                                    $("#gruCodigo").focus();
                                    event.preventDefault();
                                    event.stopPropagation();
                                }
                            });

                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
{% endblock %}
